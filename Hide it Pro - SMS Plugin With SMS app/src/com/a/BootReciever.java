package com.a;

import com.smartanuj.dbnew.PrefManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootReciever extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
    	
    	PrefManager prefs = PrefManager.getInstance(context);
    	if(prefs.useGoSMSHack() && !MonitorService.isRunning){
    		context.startService(new Intent(context,MonitorService.class));
    		AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    		Intent x= new Intent(context,MonitorService.class);
    		PendingIntent in = PendingIntent.getService(context, 0, x, 0);
    		Long interval = AlarmManager.INTERVAL_HOUR;
    		manager.setRepeating(AlarmManager.RTC, System.currentTimeMillis()+interval, interval,in);
    	}
    }
    

}
