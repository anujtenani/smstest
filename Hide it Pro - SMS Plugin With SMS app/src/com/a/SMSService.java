package com.a;

import java.util.ArrayList;

import com.smartanuj.sms.obj.BlackListObj;
import com.smartanuj.sms.obj.SMSIntent;
import com.smartanuj.sms.obj.SMSObj;
import com.smartanuj.sms.util.Utils;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.telephony.SmsManager;

public class SMSService extends Service{

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	public void sendSMS(SMSObj obj,BlackListObj bObj){
		SmsManager mgr = SmsManager.getDefault();
		
	if (obj.text.length() > 160) {
		ArrayList<String> parts = mgr.divideMessage(obj.text);
		int numParts = parts.size();
		ArrayList<PendingIntent> sentIntents = new ArrayList<PendingIntent>();
		ArrayList<PendingIntent> deliveryIntents = new ArrayList<PendingIntent>();

		for (int i = 0; i < numParts; i++) {
			sentIntents.add(SentBroadcast(obj));
			deliveryIntents.add(DeliveredBroadcast(obj));
		}
		mgr.sendMultipartTextMessage(bObj.phone, null, parts, sentIntents,
				deliveryIntents);
	} else {
		mgr.sendTextMessage(bObj.phone, null, obj.text, SentBroadcast(obj),
				DeliveredBroadcast(obj));
	}
	
			
	}
	private PendingIntent SentBroadcast(SMSObj obj) {
		Intent i = new Intent(SMSIntent.SMS_SENT);
		i.putExtras(Utils.converSMSToBundle(obj));
		return PendingIntent.getBroadcast(this, 111, i, 0);
	}

	private PendingIntent DeliveredBroadcast(SMSObj obj) {
		Intent i = new Intent(SMSIntent.SMS_DELIVERED);
		i.putExtras(Utils.converSMSToBundle(obj));
		return PendingIntent.getBroadcast(this, 111, new Intent(SMSIntent.SMS_DELIVERED), 0);
	}


}
