package com.a;

import com.smartanuj.dbnew.DbHelper;
import com.smartanuj.dbnew.PrimaryDb;
import com.smartanuj.sms.obj.SMSIntent;
import com.smartanuj.sms.obj.SMSObj;
import com.smartanuj.sms.util.Utils;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;

public class SMSDeliveryReciever extends BroadcastReceiver {

	PrimaryDb db;

	@Override
	public void onReceive(Context context, Intent intent) {
		SMSObj obj = Utils.converBundleToSMSObj(intent.getExtras());
		db = DbHelper.getPrimaryDb(context);		
		String action = intent.getAction();

		if (action.equals(SMSIntent.SMS_SENT)) {
			int resultCode = getResultCode();
			switch (resultCode) {
			case Activity.RESULT_OK:
				obj.type = PrimaryDb.TYPE_OUTGOING_SENT;
				break;
			case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
			case SmsManager.RESULT_ERROR_NO_SERVICE:
			case SmsManager.RESULT_ERROR_NULL_PDU:
			case SmsManager.RESULT_ERROR_RADIO_OFF:
				obj.type = PrimaryDb.TYPE_OUTGOING_FAILED;
				break;
			}
		} else if (action.equals(SMSIntent.SMS_DELIVERED)) {
			obj.type = PrimaryDb.TYPE_OUTGOING_DELIVERED;
		}
		db.updateSMSStatus(obj);
		return;
	}
}
