package com.a;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.smartanuj.dbnew.DbHelper;
import com.smartanuj.dbnew.MainDb;
import com.smartanuj.dbnew.PrimaryDb;
import com.smartanuj.sms.CompoundView;
import com.smartanuj.sms.obj.BlackListObj;
import com.smartanuj.sms.obj.SMSIntent;
import com.smartanuj.sms.obj.SMSObj;
import com.smartanuj.sms.util.NotificationUtil;
import com.smartanuj.sms.util.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class SMSReciever extends BroadcastReceiver {

	private static Map<String, String> RetrieveMessages(Intent intent) {
		Map<String, String> msg = null;
		SmsMessage[] msgs = null;
		Bundle bundle = intent.getExtras();

		if (bundle != null && bundle.containsKey("pdus")) {
			Object[] pdus = (Object[]) bundle.get("pdus");

			if (pdus != null) {
				int nbrOfpdus = pdus.length;
				msg = new HashMap<String, String>(nbrOfpdus);
				msgs = new SmsMessage[nbrOfpdus];

				// There can be multiple SMS from multiple senders, there can be
				// a maximum of nbrOfpdus different senders
				// However, send long SMS of same sender in one message
				for (int i = 0; i < nbrOfpdus; i++) {
					msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);

					String originatinAddress = msgs[i].getOriginatingAddress();
					if(originatinAddress != null){
					// Check if index with number exists
					if (!msg.containsKey(originatinAddress)) {
						// Index with number doesn't exist
						// Save string into associative array with sender number
						// as index
						msg.put(msgs[i].getOriginatingAddress(),
								msgs[i].getMessageBody());

					} else {
						// Number has been there, add content but consider that
						// msg.get(originatinAddress) already contains
						// sms:sndrNbr:previousparts of SMS,
						// so just add the part of the current PDU
						String previousparts = msg.get(originatinAddress);
						String msgString = previousparts
								+ msgs[i].getMessageBody();
						msg.put(originatinAddress, msgString);
						}
					}
				}
			}
		}
		return msg;
	}

	private SMSObj addToBlackList(BlackListObj bObj, String text) {
		SMSObj obj = new SMSObj();
		obj.tid = bObj.id;
		// obj.from = messages.getOriginatingAddress();
		obj.subject = "";
		obj.text = text;
		obj.timestamp = System.currentTimeMillis();
		obj.type = MainDb.TYPE_INCOMING;
		db.addToMessages(obj);
		return obj;

	}

	private void sendBroadcast(Context context, BlackListObj bObj, SMSObj obj) {
		Intent i = new Intent(SMSIntent.SMS_RECEIVED);
//		i.putExtras(Utils.getBundleFromSMS(obj));
		i.putExtras(Utils.converSMSToBundle(obj));
		i.putExtra("id", bObj.id);
	//	LocalBroadcastManager manager = LocalBroadcastManager.getInstance(context);
	//	manager.sendBroadcastSync(i);
		context.sendBroadcast(i);
//		context.sendBroadcast(i);

	}

//	MainDb db;
	PrimaryDb db;
	NotificationUtil nUtil;
	@Override
	public void onReceive(Context context, Intent intent) {
		
//			Log.i("Anuj","recieved");
//		  IntentFilter in = new IntentFilter();
//		    in.addAction("android.provider.Telephony.SMS_RECEIVED");
//		    in.setPriority(Integer.MAX_VALUE);
//		    SMSReciever rc = new com.a.SMSReciever();
//		    context.registerReceiver(this, in);

		nUtil = new NotificationUtil(context);
//		db = new MainDb(context);
		db = DbHelper.getPrimaryDb(context);
		Map<String, String> msgs = RetrieveMessages(intent);
		Iterator<String> keys = msgs.keySet().iterator();
//		Log.i("Anuj", "len:" + msgs.size());
		while (keys.hasNext()) {
			String phone = keys.next();
//			Log.i("Anuj", phone);
			BlackListObj bObj = db.isBlackList(phone);
//			Log.i("Anuj", bObj.name+":"+bObj.phone+":"+bObj.phoneFormatted);
			if (bObj.id > 0) {

				SMSObj obj = addToBlackList(bObj, msgs.get(phone));
				
				db.updateLastTimestamp(bObj.id, System.currentTimeMillis());
				if(bObj.showSMSnotification && bObj.id!=CompoundView.SHOWING_PERSON_ID){
//					Log.i("Anuj","bOBjId:"+bObj.id+":showingId:"+CompoundView.SHOWING_PERSON_ID);
					int unread = db.getTotalUnreadSMSCount();
					nUtil.createSMSNotification(unread);
				}	
//				if (bObj.showSMSnotification) {
//					int unread = db.getTotalUnreadSMSCount();
//					NotificationHelper.getNotificationInstance(context).createSMSNotification(unread);
//				}
				sendBroadcast(context, bObj, obj);
				setResult(123,null,null);
				//remove temp notification block
				this.abortBroadcast();
			}
		}

		/*
		 * 
		 * Bundle pudsBundle = intent.getExtras(); try{ Object[] pdus =
		 * (Object[]) pudsBundle.get("pdus");
		 * 
		 * final SmsMessage messages = SmsMessage.createFromPdu((byte[])
		 * pdus[0]); String phone = messages.getDisplayOriginatingAddress();
		 * 
		 * BlackListObj bObj = new BlackListObj(); if(phone.matches(".*[A-z]")){
		 * // Log.i("Anuj","Has alphabets");
		 * 
		 * }else{ // String newP = phone.replaceAll("[-+]", ""); //
		 * Log.i("Anuj",newP); bObj = db.isBlackList(phone); //
		 * Log.i("Anuj","Has numbers"); } Log.i("Anuj","ID:"+ bObj.id);
		 * if(bObj.id>0){
		 * 
		 * 
		 * //the number is blacklisted... so we just abort the broadcast // and
		 * add the message to the database SMSObj obj =
		 * addToBlackList(bObj,messages.getDisplayMessageBody());
		 * sendBroadcast(context,bObj,obj);
		 * 
		 * db.updateLastTimestamp(bObj.id,System.currentTimeMillis());
		 * 
		 * if(bObj.showSMSnotification){ int unread =
		 * db.getTotalUnreadSMSCount(); new
		 * NotificationUtil(context).createSMSNotification(unread); }
		 * 
		 * this.abortBroadcast();
		 * 
		 * }
		 * 
		 * 
		 * }catch(Exception e){e.printStackTrace();}
		 */

	}
}
