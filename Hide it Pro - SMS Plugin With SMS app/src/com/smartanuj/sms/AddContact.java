package com.smartanuj.sms;

import java.util.ArrayList;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Toast;

import com.smartanuj.dbnew.DbHelper;
import com.smartanuj.dbnew.PrimaryDb;
import com.smartanuj.sms.R;
import com.smartanuj.sms.obj.BlackListObj;
import com.smartanuj.sms.obj.CallObj;
import com.smartanuj.sms.obj.SMSObj;
import com.smartanuj.sms.util.SMSHelper;
import com.smartanuj.sms.util.Utils;
import com.smartanuj.telephony.CallLogHelper;
import com.contactpicker.compatibility.MyContactPicker;

public class AddContact extends ThemedActivity {

	private static final int CONTACT_PICKER = 11;
	private EditText editname, editphone;
	CheckBox hide_sms,show_sms_notif,show_call_notif,hide_log,block_incoming_calls,block_outgoing_calls;

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.add_contact);
		getIntentData();
		setupView();
	}

	public static final int MODE_ADD = 0;
	public static final int MODE_EDIT = 1;
	private int MODE;
	private BlackListObj bObj;

	public void getIntentData() {
		Bundle b = getIntent().getExtras();
		if (b != null) {
			Bundle obj = b.getBundle("bundle");

			if (obj != null) {
				MODE = MODE_EDIT;
			}
			if (MODE == MODE_EDIT) {
				bObj = Utils.convertToBlacklistObj(obj);

			}
		}
	}

	private void setupView() {

		if (MODE == MODE_EDIT) {
			setTitle(R.string.editing_contact);
		} else {
			setTitle(R.string.adding_contact);
		}

		Button addFromContacts = (Button) findViewById(R.id.button2);
		addFromContacts.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intentContact = new Intent(getApplicationContext(),
						MyContactPicker.class);
				startActivityForResult(intentContact, CONTACT_PICKER);
			}

		});
		editname = (EditText) findViewById(R.id.editText1);
		editphone = (EditText) findViewById(R.id.editText2);
		 hide_sms = (CheckBox) findViewById(R.id.checkBox1);
		 show_sms_notif = (CheckBox) findViewById(R.id.checkBox2);
		 block_incoming_calls = (CheckBox) findViewById(R.id.checkBox3);
		 block_outgoing_calls = (CheckBox) findViewById(R.id.checkBox4);
		 hide_log = (CheckBox) findViewById(R.id.checkBox5);
		 show_call_notif = (CheckBox) findViewById(R.id.checkBox6);

		hide_sms.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				if (isChecked) {
					show_sms_notif.setEnabled(true);
				} else {
					show_sms_notif.setChecked(false);
					show_sms_notif.setEnabled(false);
				}
			}

		});

		hide_log.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					show_call_notif.setEnabled(true);
				} else {
					show_call_notif.setChecked(false);
					show_call_notif.setEnabled(false);
				}
			}
		});
		hide_log.setChecked(false);

		/*
		 * if(Build.VERSION.SDK_INT > 16){
		 * show_call_notif.setVisibility(CheckBox.GONE);
		 * block_calls.setVisibility(CheckBox.GONE); }
		 */

		Button save = (Button) findViewById(R.id.button1);
		save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				saveContact();
			}

		});
		if (MODE == MODE_EDIT) {
			addFromContacts.setVisibility(View.GONE);
			editname.setText(bObj.name);
			editphone.setText(bObj.phone);
			hide_sms.setChecked(bObj.hide_sms);
			hide_log.setChecked(bObj.hide_call_log);
			show_sms_notif.setChecked(bObj.showSMSnotification);
			show_call_notif.setChecked(bObj.showCallnotification);
			block_incoming_calls.setChecked(bObj.block_incoming_call);
			block_outgoing_calls.setChecked(bObj.block_outgoing_call);
		}
		
		if(!hide_log.isChecked()){
			show_call_notif.setEnabled(false);
		}

	}
	private void saveContact(){
		if (editphone.getText().toString().length() > 0) {
			String phone = editphone.getText().toString().trim();
			BlackListObj obj = new BlackListObj();
			obj.name = editname.getText().toString();
			obj.phone = phone;
			obj.hide_sms = hide_sms.isChecked();
			obj.showSMSnotification = show_sms_notif.isChecked();
			obj.showCallnotification = show_call_notif.isChecked();
			obj.hide_call_log = hide_log.isChecked();
			obj.block_incoming_call = block_incoming_calls.isChecked();
			obj.block_outgoing_call = block_outgoing_calls.isChecked();
			if (MODE == MODE_EDIT) {
				obj.id = bObj.id;
			}
			PhoneNumberUtils.formatNumber(phone);
			obj.phoneFormatted = phone.replaceAll("[-+ ]", "");
			obj.phoneFormatted = PhoneNumberUtils.formatNumber(phone);
			// Log.i("Anuj",obj.phoneFormatted);
			
			// existing call log and sms too
			new SaveContact(this,obj).execute();
		} else {
			showToast(getString(android.R.string.emptyPhoneNumber));
		}
		
	}
	
	private class SaveContact extends AsyncTask<Void,Void,Void>{

		ProgressDialog pDialog;
		BlackListObj obj;
		public SaveContact(Context ctx,BlackListObj obj){
			pDialog = new ProgressDialog(ctx);
			pDialog.setMessage("Saving...");
			this.obj = obj;
		}
		
		@Override
		protected void onPreExecute(){
			pDialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... arg0) {
			int ret = DbHelper.getPrimaryDb(AddContact.this).addUserToBlacklist(obj);
	/*		
			if(contactPic != null){
				try{
				File f = new File(Environment.getExternalStorageDirectory(),"pic.png");
				f.createNewFile();
				FileOutputStream fout = new FileOutputStream(f);
				contactPic.compress(CompressFormat.PNG, 100, fout);				
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		*/	
			SMSHelper helper = new SMSHelper(getApplicationContext());
			ArrayList<SMSObj>newMessages = helper.getMessages(obj.phone);
			for(SMSObj msg:newMessages){
				msg.tid = ret;
			}

		//	Log.i("Anuj","newMessages:"+newMessages.size());
			ArrayList<SMSObj>msgInDatabase = DbHelper.getPrimaryDb(getApplicationContext()).getMessages(obj.id);
		//	Log.i("Anuj","inDatabase:"+msgInDatabase.size());

			ArrayList<SMSObj> messagesToAdd = helper.removeDuplicates(newMessages, msgInDatabase);
		//	Log.i("Anuj","toAdd:"+messagesToAdd.size());

			DbHelper.getPrimaryDb(getApplicationContext()).addToMessages(messagesToAdd);
			
			CallLogHelper callHelper = new CallLogHelper(getApplicationContext());
			ArrayList<CallObj>calls = callHelper.getCallLog(obj.phone);
			for(CallObj call:calls){
				call.uid = ret;
			}
			ArrayList<CallObj>inDatabase =  DbHelper.getPrimaryDb(getApplicationContext()).getCallLogs(ret);
			ArrayList<CallObj>newCallsToAdd = callHelper.removeDuplicates(calls,inDatabase);
			DbHelper.getPrimaryDb(getApplicationContext()).addToCalls(newCallsToAdd);
			

			return null;
		}
		@Override
		protected void onPostExecute(Void v){
			pDialog.dismiss();
			goToHome();
			
		}
		
	}
	

	/*
	 * private void hideExistingSms(String phone,int blacklistId){
	 * 
	 * } private void hideExistingCalls(String phone,int blacklistId){
	 * 
	 * } private void showHideExistingDialog(boolean hide_call,boolean
	 * hide_sms,String phone,int blacklistId){ final String ph = phone; final
	 * int bid = blacklistId; LinearLayout ll = new LinearLayout(this);
	 * ll.setOrientation(LinearLayout.VERTICAL); ll.setLayoutParams(new
	 * LinearLayout
	 * .LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,LinearLayout
	 * .LayoutParams.FILL_PARENT)); ll.setPadding(10,10,10,10);
	 * 
	 * final CheckBox hideCall = new CheckBox(this);
	 * hideCall.setText("Hide existing Call Logs"); hideCall.setChecked(false);
	 * 
	 * final CheckBox hideSms = new CheckBox(this);
	 * hideSms.setText("Hide existing SMS Messages"); hideSms.setChecked(false);
	 * if(hide_call){ ll.addView(hideCall); } if(hide_sms){ ll.addView(hideSms);
	 * } new AlertDialog.Builder(this) .setView(ll) .setMessage(
	 * "Do you wish to hide the already existing call logs and messages from this number"
	 * +
	 * "\n\nCaution:This action is irreversible , i.e. once hidden these calls/sms cannot be unhid"
	 * ) .setPositiveButton("Okay",new DialogInterface.OnClickListener(){
	 * 
	 * @Override public void onClick(DialogInterface dialog, int which) {
	 * if(hideCall.isChecked()){ hideExistingCalls(ph,bid); }
	 * if(hideSms.isChecked()){ hideExistingSms(ph,bid); } }
	 * 
	 * }) .setNegativeButton("Cancel",new DialogInterface.OnClickListener(){
	 * 
	 * @Override public void onClick(DialogInterface dialog, int which) {
	 * dialog.dismiss(); }
	 * 
	 * }) .create().show();
	 * 
	 * 
	 * }
	 */
//	Bitmap contactPic = null;
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case CONTACT_PICKER:
			if (resultCode == Activity.RESULT_OK) {
				Bundle b = data.getExtras();
				String name = b.getString("name");
				editname.setText("");
	/*			contactPic = (Bitmap) b.get("image");
				if(contactPic==null){
					showToast("Contact is null");
					Log.i("Anuj","Contact isnull");
				}
	*/			editphone.setText("");
				editname.setText(name);
				ArrayList<String> phoneNumbers = b.getStringArrayList("phone");
				if (phoneNumbers.size() == 0) {
					showToast(getString(R.string.no_phone_on_contact));
				} else if (phoneNumbers.size() == 1) {
					editphone.setText(phoneNumbers.get(0));
				} else {
					showPhonePickerDialog(phoneNumbers);
				}
			} else {
				// showToast("Cancelled");
			}
			break;
		}
	}

	private void showToast(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
	}

	int selected = 0;

	private void showPhonePickerDialog(final ArrayList<String> ph) {
		selected = 0;

		int size = ph.size();
		CharSequence[] items = new CharSequence[size];
		for (int i = 0; i < size; i++) {
			items[i] = ph.get(i);
		}

		new AlertDialog.Builder(this)
				.setSingleChoiceItems(items, selected,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								selected = which;
							}

						})
				.setPositiveButton(android.R.string.ok,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								editphone.setText(ph.get(selected));
							}
						})
				.setTitle(R.string.select_phone_number)
				.setNegativeButton(android.R.string.cancel,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {

							}

						}).create().show();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (MODE == MODE_EDIT) {
			getMenuInflater().inflate(R.menu.edit_contact, menu);
		}else{
			getMenuInflater().inflate(R.menu.add_contact, menu);
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			goToHome();
			break;
		case R.id.delete:
			showDeleteConfirmDialog();
			break;
		case R.id.save:
			saveContact();
			break;
		
		}

		return super.onOptionsItemSelected(item);
	}

	private void showDeleteConfirmDialog() {
		new AlertDialog.Builder(this)
				.setMessage(R.string.delete_confirm_dialog)
				.setPositiveButton(R.string.delete,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								PrimaryDb db = DbHelper.getPrimaryDb(getApplicationContext());
								//MainDb db = new MainDb(getApplicationContext());
								try {
									db.deleteUserFromBlacklist(bObj);
									db.deleteAllMessagesFromUser(bObj.id);
									db.deleteAllLogOf(bObj.id);
								} catch (Exception e) {
								}
								goToHome();

							}

						})
				.setNegativeButton(android.R.string.cancel,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {

							}

						}).create().show();

	}

	private void goToHome() {
		finish();
/*		Intent i = new Intent(getApplicationContext(), HiddenContacts.class);
		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(i);
*/
	}
	

}
