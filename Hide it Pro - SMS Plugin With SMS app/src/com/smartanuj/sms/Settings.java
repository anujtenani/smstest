package com.smartanuj.sms;

import java.io.File;
import java.util.ArrayList;

import com.a.MonitorService;
import com.smartanuj.dbnew.DbHelper;
import com.smartanuj.dbnew.PrefManager;
import com.smartanuj.dbnew.PrimaryDb;
import com.smartanuj.sms.util.Utils;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.text.format.DateUtils;
import android.view.Window;
import android.widget.Toast;

public class Settings extends PreferenceActivity{

	PrimaryDb db;
	PrefManager prefs;
	@Override
	public void onCreate(Bundle icicle){
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(icicle);
		addPreferencesFromResource(R.xml.settings);
		
		
		db = DbHelper.getPrimaryDb(this);
		prefs = PrefManager.getInstance(this);
		findPreference("restoreBackup").setOnPreferenceClickListener(new OnPreferenceClickListener(){

			@Override
			public boolean onPreferenceClick(Preference preference) {
				new GetBackups().execute();
				return false;
			}
			
		});
		findPreference("createBackup").setOnPreferenceClickListener(new OnPreferenceClickListener(){

			@Override
			public boolean onPreferenceClick(Preference arg0) {
				new CreateBackup().execute();
				return false;
			}
			
		});
		
		findPreference("helpUsTranslate").setOnPreferenceClickListener(new OnPreferenceClickListener(){

			@Override
			public boolean onPreferenceClick(Preference preference) {
				launchEmailIntent();
				return false;
			}
			
		});
		ListPreference themeChange = (ListPreference) findPreference("themeType");
		themeChange.setOnPreferenceChangeListener(new OnPreferenceChangeListener(){

			@Override
			public boolean onPreferenceChange(Preference preference,Object newValue) {
				prefs.setTheme((String)newValue);
				startActivity(new Intent(getApplicationContext(),HiddenContacts.class));
				finish();
				return false;
			}
			});
		
		findPreference("helpDocument").setOnPreferenceClickListener(new OnPreferenceClickListener(){

			@Override
			public boolean onPreferenceClick(Preference arg0) {
				startActivity(new Intent(getApplicationContext(),Help.class));
				return false;
			}
			
		});
		findPreference("goSMSHack").setOnPreferenceChangeListener(new OnPreferenceChangeListener(){

			@Override
			public boolean onPreferenceChange(Preference arg0, Object arg1) {
				
				boolean newValue = (Boolean) arg1;
				if(newValue){
					if(!MonitorService.isRunning){
						Context context = getApplicationContext();
			    		context.startService(new Intent(context,MonitorService.class));
			    		AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			    		Intent x= new Intent(context,MonitorService.class);
			    		PendingIntent in = PendingIntent.getService(context, 0, x, 0);
			    		Long interval = new Long(10000);
			    		manager.setRepeating(AlarmManager.RTC, System.currentTimeMillis()+interval, interval,in);
			    	}
				}else{
					stopService(new Intent(getApplicationContext(),MonitorService.class));
					AlarmManager manager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
		    		Intent x= new Intent(getApplicationContext(),MonitorService.class);
		    		PendingIntent in = PendingIntent.getService(getApplicationContext(), 0, x, 0);

					manager.cancel(in);
					
				}
				
				return true;
			}
			
		});
		findPreference("customNotification").setOnPreferenceChangeListener(new OnPreferenceChangeListener(){

			@Override
			public boolean onPreferenceChange(Preference arg0, Object arg1) {
			//	NotificationHelper.resetInstance();
				return true;
			}
			
		});
	}
	
	 public void launchEmailIntent(){
			Intent i = new Intent(Intent.ACTION_SEND);
			i.setType("plain/text");
			String[] email = {"translate@hideitpro.com"};
			i.putExtra(Intent.EXTRA_EMAIL, email);
			i.putExtra(Intent.EXTRA_SUBJECT,"Translate");
			String str = "";
			i.putExtra(Intent.EXTRA_TEXT, str);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(Intent.createChooser(i,"Send us a message"));
		}
	
	private class CreateBackup extends AsyncTask<Void,Void,Void>{

		
		
		@Override
		protected Void doInBackground(Void... arg0) {
			prefs.backupDatabase();
			return null;
		}
		
		@Override
		protected void onPostExecute(Void v){
			showToast(getResources().getString(R.string.database_backup_created));
		}
	}
	
	
	private class GetBackups extends AsyncTask<Void,Void,ArrayList<File>>{

		@Override
		protected ArrayList<File> doInBackground(Void... params) {
			return prefs.getDatabaseBackups();
		}
		@Override
		protected void onPostExecute(ArrayList<File> files){
			showBackups(files);
		}
		
		
	}
	
	public void showBackups(final ArrayList<File>files){
		
		ArrayList<CharSequence>items = new ArrayList<CharSequence>();
		for(File f:files){
//			items.add(Utils.betterTime(f.lastModified()));
			items.add(DateUtils.getRelativeTimeSpanString(Settings.this, f.lastModified(), false));
		}
		CharSequence[] array = new CharSequence[items.size()];
		new AlertDialog.Builder(this)
		.setItems(items.toArray(array), new OnClickListener(){

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				showConfirmDialog(files.get(arg1));
			}
			
		}).setTitle(R.string.restore_backup_from_title).
		create().show();
		
		
	}
	
	private void showConfirmDialog(final File f){
		StringBuilder str = new StringBuilder();
//		str.append(getString(R.string.restore_backup_from)).append(Utils.betterTime(f.lastModified())).append(" ?");
		str.append(getString(R.string.restore_backup_from)).append(DateUtils.getRelativeTimeSpanString(Settings.this, f.lastModified(), false)).append(" ?");
		
		new AlertDialog.Builder(this)
		.setMessage(str.toString())
		.setPositiveButton(android.R.string.ok, new OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {
				db.restoreDatabaseBackup(f);
				showToast(getString(R.string.backup_restored));
			}
			
			
		})
		.setNegativeButton(android.R.string.cancel, new OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
			
		})
		.create().show();
		
		
		
	}
	
	private void showToast(String text){
		Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
	}
}
