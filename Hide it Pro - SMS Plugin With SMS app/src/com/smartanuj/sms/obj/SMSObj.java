package com.smartanuj.sms.obj;

import android.text.format.DateUtils;

import com.smartanuj.sms.util.Utils;

public class SMSObj {
    
    /**message id and thread ids**/
    public int mid,tid;
    
    public Long timestamp;
    public String text,subject;
    public int type;
    
    /** if isSeen = 1 , than we have seen the message */
    public int isSeen;
    
    public String uri,otp;
    
    public String toString(){
    	return new StringBuilder().append("text:").append(text)
    	.append(":type:").append(type)
    	.append(":mid:").append(mid)
    	.append(":tid:").append(tid)
    	.append(":time:").append(DateUtils.getRelativeTimeSpanString(timestamp))
    	.toString();    	
    }
    
}
