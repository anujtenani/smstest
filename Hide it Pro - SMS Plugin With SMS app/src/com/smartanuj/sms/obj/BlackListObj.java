package com.smartanuj.sms.obj;

public class BlackListObj {

    public int id;
    public boolean hide_sms,showSMSnotification,hide_call_log;
    public boolean block_incoming_call,block_outgoing_call,showCallnotification;
    public long last_sms_recieved;
    public int unreadSMSCount;
    public int totalSMSCount;
    public int unreadCallCount;
    public int totalCallCount;
    public String name,phone,phoneFormatted;
}
