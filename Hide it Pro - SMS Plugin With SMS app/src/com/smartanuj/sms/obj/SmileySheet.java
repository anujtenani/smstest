package com.smartanuj.sms.obj;

import java.util.regex.Pattern;

import com.smartanuj.sms.R;

public class SmileySheet {

	public static final Pattern[] sheet = new Pattern[]{
		Pattern.compile(":[-|=]?\\)"), //happy :)
		Pattern.compile(":[-|=]?\\("), //sad :(
		Pattern.compile(":[-|=]?[D|d]"), //laughing :D
		Pattern.compile(";[-|=]?\\)"), //winking ;)
		Pattern.compile(";[-|=]?\\("), //cryinh ;(
		Pattern.compile(":'\\("), // crying
		Pattern.compile("[8|B][-|=]?\\)"), //cool 8)
		Pattern.compile(":[-|=]?[P|p]"), //toungue out :p
		Pattern.compile(":[-|=]?\\*"), //kissing :* 
		Pattern.compile("[O|0]:[-|=]?\\)") ,//angel O:)
		Pattern.compile("[o|O]\\.[o|O]"), // wtf o.O
		Pattern.compile(":[-|=]?[O|o]"), // surprised :O
		Pattern.compile("<3"),
	};
	public static final Integer[] icon = new Integer[]{
		
		R.drawable.emo_im_happy,
		R.drawable.emo_im_sad,
		R.drawable.emo_im_laughing,
		R.drawable.emo_im_winking,
		R.drawable.emo_im_crying,
		R.drawable.emo_im_crying,
		R.drawable.emo_im_cool,
		R.drawable.emo_im_tongue_sticking_out,
		R.drawable.emo_im_kissing,
		R.drawable.emo_im_angel,
		R.drawable.emo_im_wtf,
		R.drawable.emo_im_surprised,
		R.drawable.emo_im_heart,
/*
		R.drawable.emo_im_embarrassed,
		R.drawable.emo_im_foot_in_mouth,
		R.drawable.emo_im_lips_are_sealed,
		R.drawable.emo_im_money_mouth,
		R.drawable.emo_im_undecided,
		R.drawable.emo_im_yelling
		*/
		};
	
}
