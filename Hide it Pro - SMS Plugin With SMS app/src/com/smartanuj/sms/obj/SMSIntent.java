package com.smartanuj.sms.obj;

public class SMSIntent {
    
    // used to be set up as broadcast reciever intent for updating the activities ui
    public static final  String SMS_SENT = "com.smartanuj.sms_sent";
    public  static final String SMS_DELIVERED = "com.smartanuj.sms_delivered";
    public static final String SMS_RECEIVED = "sms_received";
    public static final String CALL_TYPE_INCOMING = "call_incoming";
    public static final String CALL_TYPE_OUTGOING = "call_outgoing";
    public static final String CALL_TYPE_MISSED = "call_missed";

    
    //used for determining where to go once the user comes to disguise screen
    // from notification
    public static final String GO_TO_CALL_LOGS = "call_logs";
    public static final String GO_TO_SMS= "sms_logs";

}
