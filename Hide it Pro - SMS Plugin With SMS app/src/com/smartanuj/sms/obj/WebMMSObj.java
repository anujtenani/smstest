package com.smartanuj.sms.obj;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import com.smartanuj.countingstreams.CountingOutputStream;
import com.smartanuj.dbnew.PrefManager;
import com.smartanuj.imageutils.MediaFile;
import com.smartanuj.imageutils.MyThumbUtil;


public class WebMMSObj {
	
	public static class ToUpload{
		public String path;
		public int orientation;
		public ToUpload(String path,int orientation){
			this.path = path;
			this.orientation = orientation;
		}
		
		public static Bundle createBundle(ToUpload up){
			Bundle b = new Bundle();
			b.putString(Intent.EXTRA_STREAM, up.path);
			b.putInt("orientation", up.orientation);
			return b;
		}
	
		
	}
	
	private static final int MAX_LENGTH = 1024;
	public interface ProgressListener{
		public void onPercentUpload(int percent);
		public void onResult(String result);
		public void onCancelled();
		public void onError();
	}
	
	private static final String URL = "http://h.imgt.in/upload";
	private static final int TIMEOUT = 1000;
	
	public int otp;
	public String toPhone;
	public String fromPhone;
	public ProgressListener pListener;
	public boolean doResize;
	private File file;
	private String MMSPicsDir;
	private MultipartEntity multipartEntity;
	private int orientation;
	public WebMMSObj(String MMSPicsDir,String toPhone,String fromPhone,File f,int orientation,boolean doResize,ProgressListener progressListener){
	//	Log.i("Anuj", "created webmms object");
		this.MMSPicsDir = MMSPicsDir;
		this.toPhone = toPhone;
		this.fromPhone = fromPhone;
		this.pListener = progressListener;
		this.doResize = doResize;
		this.file = f;
		this.orientation = orientation;
		
		 HttpParams params = new BasicHttpParams();
		    params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
		    HttpConnectionParams.setConnectionTimeout(params,TIMEOUT);
		    mHttpClient = new DefaultHttpClient(params);
		    listener = new CountingOutputStream.ProgressListener(){
				
				@Override
				public void bytesWritten(long bytesWritten) {
					pListener.onPercentUpload((int)(bytesWritten*100/contentLength));
				}
			};
			multipartEntity = new MultipartEntity(){
	       		@Override
	       	    public void writeTo(final OutputStream outstream) throws IOException {
	       	        super.writeTo(new CountingOutputStream(outstream,listener,30000));
	       	    }
	       	
	       };
	       try{
	       multipartEntity.addPart("to",new StringBody(toPhone));
	       multipartEntity.addPart("from",new StringBody(fromPhone));
	       }catch(Exception e){}
	       httppost = new HttpPost(URL);
	}
	
	HttpClient mHttpClient;
	CountingOutputStream.ProgressListener listener;
	HttpPost httppost;
	long contentLength;
	public void doSend(){
	   
//		Log.i("Anuj", "doing do send");

       
    
      new Thread(){
    	  public void run(){
    		  String toReturn = null;
    		  try{
    			  String mimetype = MediaFile.getFileType(file.getName()).mimeType;
    			  
    		   		 if(doResize){
    //		 			Log.i("Anuj", "resizgin");

    		   			 Bitmap bit = MyThumbUtil.scaleImage(file.getAbsolutePath()	, MAX_LENGTH,orientation);
    		   			 File compressed = new File(MMSPicsDir,file.getName());
    		   			 compressed.getParentFile().mkdirs();
    		   			 bit.compress(CompressFormat.JPEG, 85, new FileOutputStream(compressed));
    		   			 bit.recycle();
    		   			 if(compressed.exists()){
    		   		        multipartEntity.addPart("files", new FileBody(compressed,mimetype));
    		   			 }else{
     		   		        multipartEntity.addPart("files", new FileBody(file,mimetype));
    		   			 }
    		   		 }else{
 		   		        multipartEntity.addPart("files", new FileBody(file,mimetype));
    		   		 }
    		   		
    		      
    		      httppost.setEntity(multipartEntity);
    		      contentLength = multipartEntity.getContentLength();
    			 
  //      		  Log.i("Anuj", "executing request");
    				toReturn =  mHttpClient.execute(httppost,new ResponseHandler<String>(){

    					@Override
    					public String handleResponse(HttpResponse response)	throws ClientProtocolException, IOException {
    			            HttpEntity r_entity = response.getEntity();
    			            String returned =  EntityUtils.toString(r_entity);
    			            return returned;
    					}
    					
    				});
    				pListener.onResult(toReturn);
    				}catch(Exception e){
    					String msg = e.getMessage();
    					if(msg.contains("abort") || msg.contains("shutdown") || msg.contains("Socket closed")){
    						pListener.onCancelled();
    					}else{
    						pListener.onError();
    					}
    					e.printStackTrace();
    				}
    	  }
      }.start();
	}
	public void cancelUpload(){
		httppost.abort();
	}


}
