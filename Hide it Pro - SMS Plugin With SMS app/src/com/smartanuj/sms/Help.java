package com.smartanuj.sms;


import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.widget.LinearLayout;

public class Help extends ActionBarActivity {

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setTitle("Help Document");
		LinearLayout ll = new LinearLayout(this);
		ll.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		WebView w = new WebView(this);
		ll.addView(w);
		w.loadUrl("file:///android_asset/help.html");
		setContentView(ll);
	}
}
