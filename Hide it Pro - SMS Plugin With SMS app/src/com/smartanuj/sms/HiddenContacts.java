package com.smartanuj.sms;

import java.util.ArrayList;
import com.a.MonitorService;
import com.smartanuj.arrowview.ArrowView;
import com.smartanuj.arrowview.ArrowViewNew;
import com.smartanuj.dbnew.DbHelper;
import com.smartanuj.dbnew.MainDb;
import com.smartanuj.dbnew.PrefManager;
import com.smartanuj.dbnew.PrimaryDb;
import com.smartanuj.sms.R;
import com.smartanuj.sms.obj.BlackListObj;
import com.smartanuj.sms.obj.SMSIntent;
import com.smartanuj.sms.obj.WebMMSObj.ToUpload;
import com.smartanuj.sms.util.NotificationUtil;
import com.smartanuj.sms.util.Utils;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;

public class HiddenContacts extends ThemedActivity {

	
	public static boolean SHOWING_HIDDEN_CONTACTS = false;
	ArrayList<BlackListObj> blacklist = new ArrayList<BlackListObj>();
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.hidden_contacts);
		try{
		Bundle b = getIntent().getExtras();
		if(b!=null){
			String vaultLoc = b.getString("vaultLoc");
			if(vaultLoc!=null){
				prefs.setVaultLocation(vaultLoc);
			}
		}
		
		}catch(Exception e){}
			setTitle(R.string.contacts);

		try{
		checkForGoSMS();
			}catch(Exception e){
				}
		setupView();
	}
	


	private MyAdapter adapter;
	private PrimaryDb db;
	private ListView lv;
	private ProgressBar pBar;

	private void setupView() {
		adapter = new MyAdapter(this);
		pBar = (ProgressBar) findViewById(R.id.progressBar1);
		lv = (ListView) findViewById(R.id.listView1);
		
		TextView tv = new TextView(this);
		tv.setText(R.string.footer_help);
//		tv.setTextColor(Color.WHITE);
		tv.setLayoutParams(new ListView.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		tv.setGravity(Gravity.CENTER_HORIZONTAL);
		tv.setPadding(0, 30, 0, 0);
		
//		lv.addFooterView(tv, null, false);
//		lv.setFooterDividersEnabled(false);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,int position, long arg3) {
				BlackListObj bl = blacklist.get(position);
		/*		getInten()
				if(shareUri!=null){
					if(bl.hide_sms){
						Intent i = new Intent(getApplicationContext(),CompoundView.class);
						i.putExtra("bundle", Utils.convertToBundle(bl));
						i.putExtras(getIntent().getExtras());
						i.putExtra(Intent.EXTRA_STREAM,shareUri);
						startActivity(i);
						finish();
					}else{
						showToast("You have not enabled SMS for this contact");
					}
					
					return;
				}
				*/
				// Intent i = new
				// Intent(getApplicationContext(),SMSThread.class);
				if(bl.hide_sms||bl.hide_call_log){
					Intent i = new Intent(HiddenContacts.this,CompoundView.class);
					i.putExtra("bundle", Utils.convertToBundle(bl));
					Bundle all = getIntent().getExtras();
				if(all!=null){
					i.putExtras(all);
				}
				startActivity(i);
				}
			}
		});
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,int position, long arg3) {

				selectedPos = position;
				showDialog(DIALOG_OPTS);
//				showOptions();
				return false;
			}

		});

	//	new Populate(this).execute();
		
		db = DbHelper.getPrimaryDb(this);
		upgradeApp();
		prefs.performRoutineBackup();
        
  //      new MarketRating(this,5);
	}
/*
	private class Populate extends AsyncTask<Void,Integer,Void>{

		private static final int MAX = 5000;
		ProgressDialog pDialog;
		MainDb mdb;
		public Populate(Context ctx){
			pDialog = new ProgressDialog(ctx);
			pDialog.setIndeterminate(false);
			pDialog.setMax(MAX);
			pDialog.setProgress(0);
			mdb = new MainDb(ctx);
			try {
				mdb.createDataBase();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			ArrayList<SMSObj>objs = new ArrayList<SMSObj>();
			for(int i=0;i<MAX;i++){
		    	SMSObj sms = new SMSObj();
		    	sms.mid = 1;
		    	sms.subject = "";
		    	sms.text = "testing with a long :) and ;-) also O.o string and a whole lot of records here , just to increase the size of databse";
		    	sms.timestamp = System.currentTimeMillis();
		    	sms.type = MainDb.TYPE_INCOMING;
		    	sms.tid = 1;
		    	objs.add(sms);
		    	}
	    	mdb.addToMessages(objs);

	    	BlackListObj bObj = new BlackListObj();
	    	bObj.block_incoming_call = false;
	    	bObj.block_outgoing_call = false;
	    	bObj.hide_call_log = false;
	    	bObj.hide_sms = false;
	    	
	    	bObj.name = "Anuj Tenani";
	    	bObj.phone ="+919968910469";
	    	bObj.showCallnotification = true;
	    	bObj.showSMSnotification = true;
	    	mdb.addUserToBlacklist(bObj);
	    	
			return null;
		}
		@Override
		protected void onProgressUpdate(Integer... in){
			pDialog.setProgress(in[0]);
		}
		@Override
		protected void onPreExecute(){
			pDialog.show();
		}
		@Override
		protected void onPostExecute(Void v){
			pDialog.dismiss();
		}
		
		
		
	}
	*/
	
	int selectedPos;
	int selected;

	private AlertDialog showOptions() {
		selected = 0;
		CharSequence[] items = getResources().getStringArray(R.array.contacts_options);
		return new AlertDialog.Builder(this)
				.setSingleChoiceItems(items, selected, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						selected = which;
					}

				}).setPositiveButton(android.R.string.ok, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (selected) {
					//	case 1:
					//		unhide();
					//		break;
						case 0:
							BlackListObj black = blacklist.get(selectedPos);
							Intent x = new Intent(HiddenContacts.this,AddContact.class);
							x.putExtra("bundle", Utils.convertToBundle(black));
							startActivity(x);
							break;
						case 1:
							showDeleteConfirmDialog();
							break;
						}
					}

				}).setNegativeButton(android.R.string.cancel, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

					}

				}).create();
	}
	
	
	
	private static final int DIALOG_OPTS = 10;
	@Override
	public Dialog onCreateDialog(int dialog){
		switch(dialog){
		case DIALOG_OPTS:
			return showOptions();
		}
		return null;
	}
	/*
	private void unhide(){
		new Unhide(this).execute();

	}
*/
	private void showDeleteConfirmDialog() {
		new AlertDialog.Builder(this)
				.setMessage(R.string.delete_confirm_dialog)
				.setPositiveButton(R.string.delete, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						try {
							db.deleteUserFromBlacklist(blacklist
									.get(selectedPos));
							db.deleteAllMessagesFromUser(blacklist
									.get(selectedPos).id);
							db.deleteAllLogOf(blacklist.get(selectedPos).id);
							loadData();
						} catch (Exception e) {
						}
					}

				}).setNegativeButton(android.R.string.cancel, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
					}

				}).create().show();

	}
/*
	private class Unhide extends AsyncTask<Void,Void,Void>{

		ProgressDialog pDialog;
		public Unhide(Context ctx){
			pDialog = new ProgressDialog(ctx);
			pDialog.setMessage(getString(R.string.loading));
		}
		
		@Override
		protected Void doInBackground(Void... arg0) {
			BlackListObj person = blacklist.get(selectedPos);
			new SMSHelper(getApplicationContext()).putMessagesBack(person, db.getMessages(person.id));
			new CallLogHelper(getApplicationContext()).putCallsBack(person, db.getCallLogs(person.id));
			
			return null;
		}
		
		@Override
		protected void onPreExecute(){
			pDialog.show();
		}
		@Override
		protected void onPostExecute(Void voids){
			pDialog.dismiss();
			showToast("Contact put back inside messages");
		}
		
	}
	*/

	/*
	View stub;
	 boolean isMarkMultiple;
	private void enterMarkMultiple(){
		if(stub==null){
			stub = ((ViewStub)findViewById(R.id.viewStub1)).inflate();
			Button del = (Button) stub.findViewById(R.id.button1);
			del.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					showDeleteConfirmDialog();
				}
			});
			Button unhide = (Button)findViewById(R.id.button2);
			unhide.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					//unhide here;
				}
			});
			
		}
		isMarkMultiple = true;
		stub.setVisibility(View.VISIBLE);
		
	}
	private void exitMarkMultiple(){
		isMarkMultiple = false;
	}
	*/
	
	private class LoadData extends AsyncTask<Void, Void, ArrayList<BlackListObj>> {

		@Override
		protected ArrayList<BlackListObj> doInBackground(Void... params) {
			ArrayList<BlackListObj> obj = db.getList();
			// HashMap<Integer,Integer>total = db.getTotalSMSCounts();
			// HashMap<Integer,Integer>unread = db.getUnreadCount();

			// loop over and add any total/unread count to it
			/*
			 * for(BlackListObj ob:obj){ if(total.containsKey(ob.id)){
			 * ob.totalSMSCount = total.get(ob.id); }
			 * if(unread.containsKey(ob.id)){ ob.unreadSMSCount =
			 * unread.get(ob.id); }
			 * 
			 * }
			 */
			return obj;
		}

		@Override
		protected void onPostExecute(ArrayList<BlackListObj> obj) {
			pBar.setVisibility(View.GONE);
			if (obj.size() == 0) {
				showNoContactsView();
				lv.setVisibility(ListView.INVISIBLE);
			} else {
				hideNoContactsView();
				lv.setVisibility(ListView.VISIBLE);
			}
			blacklist = obj;
			adapter.notifyDataSetChanged();
		}

		@Override
		protected void onPreExecute() {
			pBar.setVisibility(View.VISIBLE);
		}
	}

	 private View noFolders;
	 private void showNoContactsView(){
		 if(noFolders==null){
			 noFolders = ((ViewStub)findViewById(R.id.no_contacts)).inflate();
			 ArrowViewNew ar = (ArrowViewNew) noFolders.findViewById(R.id.arrowView);
			 ar.getTextView().setText(R.string.no_contacts_msg);
			 ar.getTextView().setGravity(Gravity.RIGHT);
			 ar.setCornerRadius(0.0f);
//			 ar.setContent(getString(R.string.no_contacts_msg));
			 ar.setArrowPosition(ArrowView.ARROW_TOP_RIGHT);
//			 ar.setContentGravity(Gravity.LEFT);
			 noFolders.findViewById(R.id.button1).setOnClickListener(new View.OnClickListener(){
	
				@Override
				public void onClick(View v) {
					start(Help.class);
				}
				 
			 });
		 }
		 noFolders.setVisibility(View.VISIBLE);
	 }
	 private void hideNoContactsView(){
		 if(noFolders!=null){
			 noFolders.setVisibility(View.GONE);
		 }
	 }
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.hidden_contacts, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.item1:
			start(AddContact.class);
			break;
		case R.id.item2:
			 start(Help.class);
			 break;
		 case R.id.item3:
			 start(Settings.class);
			 break;
		}

		return super.onOptionsItemSelected(item);
	}


	
	private class MyAdapter extends BaseAdapter {

		LayoutInflater inflater;
		public MyAdapter(Context context){
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}
		
		@Override
		public int getCount() {
			return blacklist.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				convertView = inflater.inflate(
						R.layout.hidden_contacts_listview, parent, false);
				holder = new ViewHolder();
				holder.name = (TextView) convertView
						.findViewById(R.id.textView1);
				holder.phone = (TextView) convertView
						.findViewById(R.id.textView2);
				holder.unreadSMSCount = (TextView) convertView
						.findViewById(R.id.textView3);
				holder.unreadCallCount = (TextView) convertView
						.findViewById(R.id.textView4);
				holder.call = (ImageButton) convertView
						.findViewById(R.id.imageButton1);
			//	holder.call.setVisibility(View.GONE);
				holder.call.setFocusable(false);
				holder.call.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						int pos = (Integer) v.getTag();
						startActivity(Utils.callIntent(blacklist.get(pos).phone));
					}

				});
				
				
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			BlackListObj b = blacklist.get(position);

			if (b.name.equals("")) {
				holder.name.setText(b.phone);
				holder.phone.setVisibility(View.INVISIBLE);
			} else {
				holder.name.setText(b.name);
				holder.phone.setText(b.phone);
				holder.phone.setVisibility(View.VISIBLE);
			}
			holder.call.setTag(position);

			if (b.unreadSMSCount > 0) {				
				holder.unreadSMSCount.setText(new StringBuilder().append(b.unreadSMSCount).toString());
				holder.unreadSMSCount.setVisibility(View.VISIBLE);
			} else {
				holder.unreadSMSCount.setVisibility(View.GONE);
			}

			if (b.unreadCallCount > 0) {
				holder.unreadCallCount.setVisibility(View.VISIBLE);
				holder.unreadCallCount.setText(new StringBuilder().append(b.unreadCallCount).toString());
			} else {
				holder.unreadCallCount.setVisibility(View.GONE);
			}

			return convertView;

		}

	}

	static class ViewHolder {
		public TextView name, phone, unreadSMSCount, unreadCallCount;
		public ImageButton call;
	}

	private void loadData() {
		new LoadData().execute();
	}
	
	
	private class UpgradeApp extends AsyncTask<Void,Void,Void>{

		ProgressDialog pDialog;
		public UpgradeApp(Context ctx){
			pDialog = new ProgressDialog(ctx);
			pDialog.setCancelable(false);
			pDialog.setMessage("Upgrading...");
		}
		
		@Override
		protected Void doInBackground(Void... arg0) {
			   MainDb db = new MainDb(getApplicationContext());
		        if(db.checkDataBase()){
		        	
		        PrimaryDb pdb = DbHelper.getPrimaryDb(getApplicationContext());
		        pdb.restoreDatabaseBackup(db.getDatabaseFile());
		        /*

		        ArrayList<BlackListObj> bObjects = db.getList();
		        ArrayList<CallObj>cObjects = db.getCallLogs();
		        ArrayList<SMSObj>sObjects = db.getMessages();
		        
		        
		        pdb.addToCalls(cObjects);
		        pdb.addToMessages(sObjects);
		        pdb.addUserToBlacklist(bObjects);
	*/	        
		        
		        //finally complete the database;
		        db.renameDatabase();
		        
		        }
			return null;
		}
		
		@Override
		protected void onPreExecute(){
			pDialog.show();
		}
		@Override
		protected void onPostExecute(Void v){
			pDialog.dismiss();
		}
	}
	
	private void upgradeApp(){
		MainDb db = new MainDb(getApplicationContext());
	    if(db.checkDataBase()){
	    	new UpgradeApp(this).execute();
	    }
	}

	private void checkForGoSMS(){
		if(prefs.needUpdate()){
		String[] packages = new String[]{
				"com.handcent.nextsms",
				"com.jb.gosms",
				"com.netqin.ps"
		};
		PackageManager pm = this.getPackageManager();
		for(String packag : packages){
			try{
			pm.getPackageInfo(packag, 0);
			prefs.useGoSMSHack(true);
			toggleMonitorState();
			prefs.needUpdate(false);
			break;
			}catch(PackageManager.NameNotFoundException e){
			}
		}
		}
		
	}
	
	private void toggleMonitorState(){
		boolean newValue = prefs.useGoSMSHack();
		if(newValue){
			if(!MonitorService.isRunning){
				Context context = getApplicationContext();
	    		context.startService(new Intent(context,MonitorService.class));
	    		AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
	    		Intent x= new Intent(context,MonitorService.class);
	    		PendingIntent in = PendingIntent.getService(context, 0, x, 0);
	    		Long interval = new Long(10000);
	    		manager.setRepeating(AlarmManager.RTC, System.currentTimeMillis()+interval, interval,in);
	    	}
		}else{
			if(MonitorService.isRunning){
				stopService(new Intent(getApplicationContext(),MonitorService.class));
				AlarmManager manager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
    			Intent x= new Intent(getApplicationContext(),MonitorService.class);
    			PendingIntent in = PendingIntent.getService(getApplicationContext(), 0, x, 0);
    			manager.cancel(in);
			}
		}
	}
	
	
	NotificationUtil nUtil;
	private final BroadcastReceiver mSMSReciever = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(SMSIntent.SMS_RECEIVED)) {
				Bundle b = intent.getExtras();
				for(BlackListObj bObj:blacklist){
					if(bObj.id == b.getInt("id")){
						bObj.unreadSMSCount++;
					}
				}
				adapter.notifyDataSetChanged();
				}
			}
		};
		

		@Override
		public void onResume() {
			super.onResume();
			loadData();
			toggleMonitorState();
			SHOWING_HIDDEN_CONTACTS = true;
			registerReceiver(mSMSReciever, new IntentFilter(SMSIntent.SMS_RECEIVED));
		}
		@Override
		public void onPause(){
			super.onPause();
			SHOWING_HIDDEN_CONTACTS = false;
			unregisterReceiver(mSMSReciever);
		}
	}