package com.smartanuj.sms.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import com.smartanuj.fileutils.FileUtils;
import com.smartanuj.sms.HiddenContacts;
import com.smartanuj.sms.obj.BlackListObj;
import com.smartanuj.sms.obj.SMSObj;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class Utils {
/*
    private static final int ONE_DAY = 24*60*60*1000;

    
    public static String betterTime(Long time){
    	
 	long deficit = System.currentTimeMillis()-time;
	if(deficit < ONE_DAY){
		//give time
	    SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
	    Date date = new Date(time);
	    return sdf.format(date);
	}else{
			SimpleDateFormat sdf = new SimpleDateFormat("MMM d hh:mm a");
		    Date date = new Date(time);
		    return sdf.format(date);
		}
    }
    
    */
    
    
    
    public static Intent homeIntent(Context ctx){
    	Intent i = new Intent(ctx,HiddenContacts.class);
		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		return i;
    }

    public static Bundle convertToBundle(BlackListObj black){
	    Bundle x = new Bundle();
	    x.putInt("id",black.id);
	    x.putBoolean("block_call_incoming",black.block_incoming_call);
	    x.putBoolean("block_call_outgoing",black.block_outgoing_call);
	    x.putBoolean("hide_call_log",black.hide_call_log);
	    x.putBoolean("hide_sms",black.hide_sms);
	    x.putString("name",black.name);
	    x.putString("phone",black.phone);
	    x.putBoolean("showCallnotification",black.showCallnotification);
	    x.putBoolean("showSMSnotification",black.showSMSnotification);
	    x.putLong("last_sms_recieved", black.last_sms_recieved);
	    x.putInt("totalCallCount",black.totalCallCount);
	    x.putInt("totalSMSCount",black.totalSMSCount);
	    x.putInt("unreadCallCount",black.unreadCallCount);
	    x.putInt("unreadSMSCount",black.unreadSMSCount);
	    x.putString("phoneFormatted",black.phoneFormatted);
	    return x;
    }
    public static BlackListObj convertToBlacklistObj(Bundle b){
	if(b!=null){
		BlackListObj black = new BlackListObj();
	black.id = b.getInt("id");
	black.block_incoming_call = b.getBoolean("block_call_incoming");
	black.block_outgoing_call = b.getBoolean("block_call_outgoing");
	black.hide_call_log = b.getBoolean("hide_call_log");
	black.hide_sms = b.getBoolean("hide_sms");
	black.last_sms_recieved = b.getLong("last_sms_recieved");
	black.name = b.getString("name");
	black.phone = b.getString("phone");
	black.phoneFormatted = b.getString("phoneFormatted");
	black.showCallnotification = b.getBoolean("showCallnotification");
	black.showSMSnotification = b.getBoolean("showSMSnotification");
	black.totalSMSCount = b.getInt("totalSMSCount");
	black.totalCallCount = b.getInt("totalCallCount");
	black.unreadCallCount = b.getInt("unreadCallCount");
	black.unreadSMSCount = b.getInt("unreadSMSCount");
	return black;
	}else{
	    return null;
	}

    }
    
    public static void saveDraft(Context ctx,String phone,String message) throws IOException{
    	File f = new File(ctx.getCacheDir(),phone);
    	if(!f.exists()){
    		f.createNewFile();
    	}
    	FileUtils.writeStringToFile(message, f);
    	return;
    }
    public static String getDraft(Context ctx,String phone) throws IOException{
    	File f = new File(ctx.getCacheDir(),phone);
    	if(f.exists() && f.canRead()){
    		return FileUtils.getTextFromFile(f);
    	}
    	return "";
    }
    
    public static Intent callIntent(String phone) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:"+phone));
        return callIntent;
    }
    
    
    public static Bundle converSMSToBundle(SMSObj obj){
    	Bundle b = new Bundle();
    	b.putInt("mid", obj.mid);
    	b.putInt("tid", obj.tid);
    	b.putString("text", obj.text);
    	b.putString("subject", obj.subject);
    	b.putInt("type", obj.type);
    	b.putInt("isSeen", obj.isSeen);
    	b.putLong("timestamp", obj.timestamp);
    	return b;
    }
    
    public static SMSObj converBundleToSMSObj(Bundle b){
    	SMSObj obj = new SMSObj();
    	obj.isSeen = b.getInt("isSeen");
    	obj.mid = b.getInt("mid");
    	obj.subject = b.getString("subject");
    	obj.text = b.getString("text");
    	obj.tid = b.getInt("tid");
    	obj.timestamp = b.getLong("timestamp");
    	obj.type = b.getInt("type");
    	return obj;
    }
    public static void covertBundleToObj(Object obj , Bundle b) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException{
    	Iterator<String>iter = b.keySet().iterator();
    	while(iter.hasNext()){
    		String key = iter.next();
        	obj.getClass().getField(key).set(obj,b.get(key)); 		
    	}
    	
    }
 
    /*
    public static SMSObj getSMSObject(Bundle b){
    	SMSObj sms = new SMSObj();
    	sms.mid = b.getInt("mid");
    	sms.text = b.getString("text");
    	sms.subject = b.getString("subject");
    	sms.tid = b.getInt("tid");
    	sms.timestamp = b.getLong("timestamp");
    	sms.type = b.getInt("type");
    	return sms;
    	
    }
    public static Bundle getBundleFromSMS(SMSObj sms){
    	Bundle b = new Bundle();
    	b.putInt("mid", sms.mid);
    	b.putInt("tid", sms.tid);
    	b.putInt("type", sms.type);
    	b.putString("subject", sms.subject);
    	b.putString("text", sms.text);
    	b.putLong("timestamp", sms.timestamp);
    	return b;
    }
*/
    
}
