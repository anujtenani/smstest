package com.smartanuj.sms.util;

import java.lang.ref.SoftReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import android.graphics.Bitmap;

public class MemoryCache {
    public static Map<String, Bitmap> cache;
    public static HashMap<String,SoftReference<Bitmap>> softCache;
    public static final int MAX_ENTRIES = 150;
    public MemoryCache(){
    	initCache();
    }
    
    public static void initCache(){
    	cache = new LinkedHashMap<String, Bitmap>(MAX_ENTRIES+1, .75F, true) {
	        @Override
			public boolean removeEldestEntry(Map.Entry eldest) {
	            return size() > MAX_ENTRIES;
	        }
	    };
	    cache = Collections.synchronizedMap(cache);
	    softCache = new HashMap<String,SoftReference<Bitmap>>();
    }
    public static synchronized Bitmap get(String id){
       if(cache!=null&&!cache.containsKey(id)){
            return null;
        }
       Bitmap bm = cache.get(id);
       
       if(bm!=null && !bm.isRecycled())	   return bm; 

       return null;
    }
    
    public static synchronized void put(String id, Bitmap bitmap){
	if(cache==null){initCache();}
		cache.put(id,bitmap);
    }

    public static synchronized void removeEntry(String entry){
    	cache.remove(entry);
    }
    
    
    public static synchronized void clear() {
	try{
	//recycle each bitmap and clear the cache
	Iterator<String> i = cache.keySet().iterator();
	while(i.hasNext()){
		    String key = i.next();
		    Bitmap x = cache.get(key);
		    if(x!=null&&!x.isRecycled()){ 			x.recycle();	    }
	}
	cache.clear();
	
	Iterator<String> soft = softCache.keySet().iterator();
	while(soft.hasNext()){
		    String key = soft.next();
		    Bitmap b = softCache.get(key).get();
		    if(b!=null&&!b.isRecycled()){ 			b.recycle(); 	    }
	}
	
   softCache.clear();
	}catch(Exception e){}
    
 
    
    }
    

   
}