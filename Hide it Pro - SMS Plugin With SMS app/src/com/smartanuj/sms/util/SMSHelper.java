package com.smartanuj.sms.util;

import java.util.ArrayList;
import com.contactpicker.compatibility.ContactPickerBase;
import com.contactpicker.compatibility.ContactPickerNew;
import com.contactpicker.compatibility.ContactPickerOld;
import com.smartanuj.sms.obj.BlackListObj;
import com.smartanuj.sms.obj.SMSObj;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.widget.Toast;

public class SMSHelper {

	Context context;
	public SMSHelper(Context ctx){
		context = ctx;
	}
	
    public void updateMessage(String newMessage,String smsId){
    
    	Uri uri = Uri.parse("content://sms");
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues();
		values.put("body",newMessage);
		try{
			cr.update(uri,values,"_id="+smsId, null);
			Toast.makeText(context,"Message Edited",Toast.LENGTH_SHORT).show();

		}catch(Exception e){
			Toast.makeText(context,"Error Occured while editing the message",Toast.LENGTH_LONG).show();					}
    }
    
    public void deleteMessage(String smsId){
    	Uri uri = Uri.parse("content://sms");
		ContentResolver cr = context.getContentResolver();
		try{
			cr.delete(uri,"_id="+smsId,null);
		//	Log.i("Anuj","Deleting:"+smsId);
		}catch(Exception e){
		}
    }
    public void addMessage(String origId , String thread_id ,String body,String address,String person,String date,String type){
    	ContentResolver cr  = context.getContentResolver();
		ContentValues values = new ContentValues();
		values.put("_id",origId);
	//	values.put("thread_id",thread_id);
		values.put("body",body);
		values.put("address",address);
		values.put("person",person);
		values.put("date",date);
		values.put("type",type);
		values.put("read","0");
	//	values.put("status","-1");
	//	values.put("locked","0");
		cr.insert(Uri.parse("content://sms"),values);
    }
    
    /** substracts obj1 from obj2 to give you messages that are not in obj2 **/
    public ArrayList<SMSObj>removeDuplicates(ArrayList<SMSObj>obj1 , ArrayList<SMSObj> obj2){
    	ArrayList<SMSObj>newMsg = new ArrayList<SMSObj>();
    	
    	ArrayList<Long>timestamps = new ArrayList<Long>();
    	for(SMSObj obj:obj2){
    		timestamps.add(obj.timestamp);
    	}
    	
    	for(SMSObj x:obj1){
    		if(!timestamps.contains(x.timestamp)){
    			newMsg.add(x);
    		}
    	}
    	
    	return newMsg;
    }
    /*
    public void exposedColumns(){
    	
    	Uri uri = Uri.parse("content://sms");
    	ContentResolver cr = context.getContentResolver();
    	cr.delete(uri,"thread_id = '' OR thread_id = null OR body = '' OR body = null", null);
    	Cursor c = cr.query(uri, null, null, null, null);
    	String[] cols = c.getColumnNames();
    	c.moveToFirst();
    	c.moveToPosition(c.getCount()-(int)(Math.random()*c.getCount())-1);
    	for(String col:cols){
//    		Log.i("Anuj",col);
   // 		Log.i("Anuj",col+""+c.getString(c.getColumnIndex(col)));
    	}
   // 	c.close();
    	
   // 	c = cr.query(uri, null, null, null, null);
    	HashMap<String,String>map = new HashMap<String,String>();
    	int size = c.getCount();
    	c.moveToFirst();
    	for(int i=0;i<size;i++){
    		map.put("thread:"+c.getString(c.getColumnIndexOrThrow("thread_id")),"body:"+ c.getString(c.getColumnIndexOrThrow("body")));
    		c.moveToNext();
    	}
    	c.close();
    	Iterator<String>iter = map.keySet().iterator();
    	Iterator<Entry<String,String>>iter2 = map.entrySet().iterator();
    	while(iter2.hasNext()){
    		Log.i("Anuj",iter2.next().getKey()+":");
    		Log.i("Anuj", iter2.next().getValue()+"");
    	}
    	
    }
    */
	public ArrayList<SMSObj> getMessages(String address){
		final String number = address;
		ArrayList<SMSObj> data = new ArrayList<SMSObj>();
  		Cursor c = null;
		ContentResolver cr = context.getContentResolver();

  		Long id = getPersonId(cr,address);
  		Uri uri = Uri.parse("content://sms");
 // 		Log.i("Anuj","URI:"+uri.toString());
  		try{
  			c = cr.query(uri, null, "address = '"+number.toString()+"' OR person = '"+id+"'", null, "date DESC");
  			if(c==null){
  			//	Toast.makeText(context,"No Messages Found",Toast.LENGTH_SHORT).show();
  			}
  			c.moveToFirst();
  			int messages = c.getCount();

  			for(int i=0;i<messages;i++){
  			 SMSObj obj = new SMSObj();
  			obj.mid = c.getInt(c.getColumnIndexOrThrow("_id"));
  			obj.tid = c.getInt(c.getColumnIndexOrThrow("thread_id"));
  			obj.text = c.getString(c.getColumnIndexOrThrow("body"));
//  		obj.from = c.getString(c.getColumnIndexOrThrow("address"));
  			// = c.getString(c.getColumnIndexOrThrow("person"));
  			obj.timestamp = c.getLong(c.getColumnIndexOrThrow("date"));
  			obj.type = c.getInt(c.getColumnIndexOrThrow("type"));
  			c.moveToNext();
  			data.add(obj);
  			}
  		}catch(Exception e){
  			e.printStackTrace();
  		}
  		
  		return data;
  	}
	
	public void putMessagesBack(BlackListObj person,ArrayList<SMSObj>messages){
		ContentResolver cr = context.getContentResolver();
		Long personId = getPersonId(cr,person.phone);
		
		
		ArrayList<SMSObj>inDatabase = getMessages(person.phone);
		ArrayList<SMSObj> messagesToAdd = removeDuplicates(messages,inDatabase);
		Uri uri  = Uri.parse("content://sms");
		
		int tid = 0;
		//find existing _id and thread_id;
		Cursor c= cr.query(uri, new String[]{"thread_id"}, "address = "+person.phone, null, null);
		if(c!=null && c.getCount() > 0){
			c.moveToFirst();
			tid = c.getInt(c.getColumnIndexOrThrow("thread_id"));
		}
		int size = messagesToAdd.size();
		ContentValues[] cvs = new ContentValues[size];
		for(int i=0;i<size;i++){
			SMSObj sms = messagesToAdd.get(i);
			ContentValues cv = new ContentValues();
			if(tid>0){
				cv.put("thread_id",tid);
			}
			
			cv.put("address",person.phone);
			cv.put("read","0");
				cv.put("person", personId);
			cv.put("body", sms.text);
			cv.put("date", sms.timestamp);
			cv.put("type", sms.type);
			cvs[i] = cv;
		}
		cr.bulkInsert(uri, cvs);
		
		
	}
	
	private Long getPersonId(ContentResolver cr,String phone){
		ContactPickerBase cpicker;
		if(Build.VERSION.SDK_INT > Build.VERSION_CODES.DONUT){
			cpicker = new ContactPickerNew();
		}else{
			cpicker = new ContactPickerOld();
		}
		return cpicker.getPersonId(cr, phone);
		
	}
	
/*	
	public void echoMessages(){
  		Uri uri = Uri.parse("content://sms");
		ContentResolver cr = context.getContentResolver();
		Cursor c = cr.query(uri, null, null, null, "date DESC");
		if(c==null){
		  		Log.i("Anuj","cursor is null");
				Toast.makeText(context,"No Messages Found",Toast.LENGTH_SHORT).show();
			}else{
			c.moveToFirst();
			int messages = c.getCount();
  		Log.i("Anuj","total Messages:"+messages);
  			for(int i=0;i<messages;i++){
				Log.i("Anuj",""+c.getString(c.getColumnIndexOrThrow("address")));
			
				Log.i("Anuj",""+c.getString(c.getColumnIndex("body")));
			
				Log.i("Anuj",""+c.getString(c.getColumnIndexOrThrow("person")));
				Log.i("Anuj","---------------------");
/*				data[i][1] = c.getString(c.getColumnIndexOrThrow("_id"));
				data[i][2] = c.getString(c.getColumnIndexOrThrow("thread_id"));
				data[i][3] = c.getString(c.getColumnIndexOrThrow("body"));
				data[i][4] = c.getString(c.getColumnIndexOrThrow("address"));
				data[i][5] = c.getString(c.getColumnIndexOrThrow("person"));
				data[i][6] = c.getString(c.getColumnIndexOrThrow("date"));
				data[i][7] = c.getString(c.getColumnIndexOrThrow("type"));
				data[i][8] = "not_hidden";
				
				c.moveToNext();
			}
			}
		
	}
	
	public String[][] getAllMessages(){
  		String[][] data = null;
  		Cursor c = null;
  		Uri uri = Uri.parse("content://sms");
		ContentResolver cr = context.getContentResolver();
  		try{
  			c = cr.query(uri, null, null, null, "date DESC");
  			if(c==null){
  		  		Log.i("Anuj","cursor is null");
  				Toast.makeText(context,"No Messages Found",Toast.LENGTH_SHORT).show();
  			}
  			c.moveToFirst();
  			int messages = c.getCount();
	  		Log.i("Anuj","total Messages:"+messages);

  			data = new String[messages][9];
  			for(int i=0;i<messages;i++){
  				data[i][0] = c.getString(c.getColumnIndexOrThrow("_id"));
  				data[i][1] = c.getString(c.getColumnIndexOrThrow("_id"));
  				data[i][2] = c.getString(c.getColumnIndexOrThrow("thread_id"));
  				data[i][3] = c.getString(c.getColumnIndexOrThrow("body"));
  				data[i][4] = c.getString(c.getColumnIndexOrThrow("address"));
  				data[i][5] = c.getString(c.getColumnIndexOrThrow("person"));
  				data[i][6] = c.getString(c.getColumnIndexOrThrow("date"));
  				data[i][7] = c.getString(c.getColumnIndexOrThrow("type"));
  				data[i][8] = "not_hidden";
  				c.moveToNext();
  			}
  		}catch(Exception e){
  			
  		}
  		
  		return data;
  	}
  	*/
    
}
