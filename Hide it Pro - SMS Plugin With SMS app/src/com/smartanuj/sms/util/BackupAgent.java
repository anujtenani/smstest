package com.smartanuj.sms.util;

import java.io.IOException;

import com.smartanuj.dbnew.PrimaryDb;

import android.app.backup.BackupAgentHelper;
import android.app.backup.BackupDataInput;
import android.app.backup.BackupDataOutput;
import android.app.backup.FileBackupHelper;
import android.app.backup.SharedPreferencesBackupHelper;
import android.os.ParcelFileDescriptor;
import android.util.Log;

public class BackupAgent extends BackupAgentHelper {
    @Override
    public void onCreate() {
    	Log.d("Anuj", "onCreate called");
    	String db = getDatabasePath(PrimaryDb.DATABASE_NAME).getAbsolutePath();
    	FileBackupHelper hosts = new FileBackupHelper(this, db);
    	addHelper("database", hosts);
    }

    @Override
    public void onBackup(ParcelFileDescriptor oldState, BackupDataOutput data,
         ParcelFileDescriptor newState) throws IOException {
            synchronized (PrimaryDb.DbLock) {
                    super.onBackup(oldState, data, newState);
            }
    }

    @Override
    public void onRestore(BackupDataInput data, int appVersionCode,
                    ParcelFileDescriptor newState) throws IOException {
            Log.d("Anuj", "onRestore called");
            synchronized (PrimaryDb.DbLock) {
                    Log.d("Anuj", "onRestore in-lock");
                    super.onRestore(data, appVersionCode, newState);
            }
    }
}