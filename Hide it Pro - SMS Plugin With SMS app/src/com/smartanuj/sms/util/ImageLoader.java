package com.smartanuj.sms.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.Map;
import java.util.Stack;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.smartanuj.dbnew.PrefManager;
import com.smartanuj.http.HttpUtils;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.widget.ImageView;

public class ImageLoader {
    
	
    private Map<ImageView, String> imageViews=Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
    HttpUtils http;
    String MMSThumbsDir;
    public ImageLoader(Context context,String MMSThumbsDir){
    	init(context,Thread.NORM_PRIORITY-1,MMSThumbsDir);
    }
    public ImageLoader(Context context,int PRIORITY,String MMSThumbsDir){
    	init(context,PRIORITY,MMSThumbsDir);
    }
    private void init(Context context,int PRIORITY,String MMSThumbsDir){
    	  photoLoaderThread.setPriority(PRIORITY);
    		if(MemoryCache.cache==null){
    	            new MemoryCache();
    	        }
    			http = new HttpUtils();
    			this.MMSThumbsDir = MMSThumbsDir;
    }
   

    public void DisplayImage(String url,String otp, Activity activity, ImageView imageView,int REPLACEMENT_RESOURCE_ID){
        imageViews.put(imageView, url);
        Bitmap bitmap = MemoryCache.get(url);
        if(bitmap!=null){
            imageView.setImageBitmap(bitmap);
        }
        else{
            queuePhoto(url,otp, activity, imageView);
            imageView.setImageResource(REPLACEMENT_RESOURCE_ID);
        }
    }
    
    public void DisplayImage(String url,String otp, Activity activity, ImageView imageView){
        imageViews.put(imageView, url);
        Bitmap bitmap = MemoryCache.get(url);
        if(bitmap!=null){
            imageView.setImageBitmap(bitmap);
        }
        else{
            queuePhoto(url,otp, activity, imageView);
            imageView.setImageBitmap(null);
        }
    }
        
    private void queuePhoto(String uri,String otp, Activity activity, ImageView imageView)
    {
        //This ImageView may be used for other images before. So there may be some old tasks in the queue. We need to discard them. 
        photosQueue.Clean(imageView);
        PhotoToLoad p = new PhotoToLoad(uri,otp, imageView);
        synchronized(photosQueue.photosToLoad){
            photosQueue.photosToLoad.push(p);
            photosQueue.photosToLoad.notifyAll();
        }
        
        //start thread if it's not started yet
        if(photoLoaderThread.getState()==Thread.State.NEW)
            photoLoaderThread.start();
    	}
    
    private Bitmap getBitmap(String uri,String otp)     {
	 Bitmap bitmap = MemoryCache.get(uri);
	        if(bitmap!=null)     return bitmap;
   //try loading from filecache
	   File thumb = new File(MMSThumbsDir,uri);
	   if(thumb.exists()&&thumb.canRead()){
		   Bitmap bit = BitmapFactory.decodeFile(thumb.getAbsolutePath());
		   if(bit!=null)  return bit;
	   }
		   //download from website
		   try{
		   String url = PrefManager.URI_PREFIX+"thumb/"+uri+"/"+otp;
		   	http.copy(url, thumb);
	    	return 	BitmapFactory.decodeFile(thumb.getAbsolutePath());
		   }catch(Exception e){
			   e.printStackTrace();
			   thumb.delete();
		   }
		   
		  return null;
    }
		    

    //decodes image and scales it to reduce memory consumption
    private Bitmap decodeFile(File f){
        try {
            //decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);
            //Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE=240;
            int width_tmp=o.outWidth, height_tmp=o.outHeight;
            int scale=1;
            while(true){
                if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)
                    break;
                width_tmp/=2;
                height_tmp/=2;
                scale*=2;
            }
            
            //decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {}
    	 catch(Error er){}
        return null;
    }
    
    //Task for the queue
    private class PhotoToLoad
    {
	/* here url is actually thumbnail path */
        public String otp;
        public String uri;
        public ImageView imageView;
        public PhotoToLoad(String uri,String otp, ImageView i){
        	this.uri = uri;
        	this.otp = otp; 
            imageView=i;
        }
    }
    
    PhotosQueue photosQueue=new PhotosQueue();
    
    public void stopThread()
    {
        photoLoaderThread.interrupt();
	exec.shutdownNow();
	
    }
    
    //stores list of photos to download
    class PhotosQueue
    {
        private Stack<PhotoToLoad> photosToLoad=new Stack<PhotoToLoad>();
        
        //removes all instances of this ImageView
        public void Clean(ImageView image)
        {	
            try{
            for(int j=0 ;j<photosToLoad.size();){
                if(photosToLoad.get(j).imageView==image)
                    photosToLoad.remove(j);
                else
                    ++j;
            }
            }catch(Exception e){}
        }
    }
    

    ExecutorService exec = Executors.newFixedThreadPool(2);
    class Loader implements Runnable{
	PhotoToLoad photoToLoad;
	public Loader(PhotoToLoad photoToLoad){
	    this.photoToLoad = photoToLoad;
	}
	@Override
	public void run() {
	    Bitmap bmp=getBitmap(photoToLoad.uri,photoToLoad.otp);
	    MemoryCache.put(photoToLoad.uri, bmp);
            String tag=imageViews.get(photoToLoad.imageView);
            if(tag!=null && tag.equals(photoToLoad.uri)){
                BitmapDisplayer bd=new BitmapDisplayer(bmp, photoToLoad.imageView);
                Activity a=(Activity)photoToLoad.imageView.getContext();
                a.runOnUiThread(bd);
            }
	    
	}
	
	
    }
    
    class PhotosLoader extends Thread {
        @Override
		public void run() {
            try {
                while(true)
                {
                    //thread waits until there are any images to load in the queue
                    if(photosQueue.photosToLoad.size()==0)
                        synchronized(photosQueue.photosToLoad){
                            photosQueue.photosToLoad.wait();
                        }
                    if(photosQueue.photosToLoad.size()!=0)
                    {
                        PhotoToLoad photoToLoad;
                        synchronized(photosQueue.photosToLoad){
                            photoToLoad=photosQueue.photosToLoad.pop();
                    }
                        
/*                      exec.execute(new Loader(photoToLoad));
 */                        
                        Bitmap bmp = getBitmap(photoToLoad.uri,photoToLoad.otp);
                        MemoryCache.put(photoToLoad.uri, bmp);
                        String tag = imageViews.get(photoToLoad.imageView);
                        if(tag!=null && tag.equals(photoToLoad.uri)){
                            BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad.imageView);
                            Activity a = (Activity)photoToLoad.imageView.getContext();
                            a.runOnUiThread(bd);
                        }
//                       
                    }
                    if(Thread.interrupted()){
                	break;
                    }
                }
            } catch (InterruptedException e) {
                //allow thread to exit
            }
        }
    }
    
    PhotosLoader photoLoaderThread=new PhotosLoader();
    
    //Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable
    {
        Bitmap bitmap;
        ImageView imageView;
        public BitmapDisplayer(Bitmap b, ImageView i){bitmap=b;imageView=i;}
        @Override
		public void run()
        {
//        	Log.i("Anuj","rounding corners");
            if(bitmap!=null) imageView.setImageBitmap(bitmap);
        }
    }

    public Bitmap roundCorners(Bitmap bitmap){
    	Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        
        
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        
        final float roundPx = 25;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }
    public void clearCache() {
    	MemoryCache.clear();
    }

}
