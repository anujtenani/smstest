package com.smartanuj.sms.util;


import com.smartanuj.dbnew.PrefManager;
import com.smartanuj.sms.HiddenContacts;
import com.smartanuj.sms.R;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;

public class NotificationUtil {

	 private  Context context;
    private static final int SMS_NOTIF_ID = 111;
    private static final int CALL_NOTIF_ID = 112;
    private static final int NOTIFICATION_ICON = R.drawable.ic_stat_info;
    private NotificationManager nMgr;
    private PrefManager prefs;
    private String NOTIF_TITLE;
    private String TICKER_TEXT;
    private String NOTIF_SUBTITLE;
    
    
    public NotificationUtil(Context ctx){
	context  = ctx;
	nMgr = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);	
	prefs = PrefManager.getInstance(ctx);
	Resources r = ctx.getResources();

	if(prefs.useCustomNotification()){
	//	Log.i("Anuj","custom notification");
		NOTIF_TITLE = prefs.getNotificationTitle();
		TICKER_TEXT = NOTIF_TITLE;
		NOTIF_SUBTITLE = prefs.getNotificationSubtitle();
	}
	
	NOTIF_TITLE = NOTIF_TITLE == null ? r.getString(R.string.audio_manager) : NOTIF_TITLE;
	TICKER_TEXT = TICKER_TEXT == null ? r.getString(R.string.notif_ticker_text) : TICKER_TEXT;
	NOTIF_SUBTITLE =  NOTIF_SUBTITLE == null ?r.getString(R.string.notif_subtitle) : NOTIF_SUBTITLE;
	
	
	
    }
    
    private Intent getLaunchIntentForPackage(String packageName){
    	
    	try{
    		return context.getPackageManager().getLaunchIntentForPackage(packageName);
    		}catch(Exception e){
    	 }
    	 return null;
    }
    
    private Intent getNotificationIntent(){
    	Intent notificationIntent = null;
    	notificationIntent = getLaunchIntentForPackage("com.smartanuj.hideitpro");
    	 
    	 if(notificationIntent == null){
    		 notificationIntent = getLaunchIntentForPackage("com.smartanuj.hideitprokey");
    	 }
    	 
    	 if(notificationIntent == null){
    		 notificationIntent = new Intent("com.smartanuj.hideitpro.launch");
    		 ResolveInfo info = context.getPackageManager().resolveActivity(notificationIntent, 0);
    		 if(info ==null){
        		 notificationIntent = new Intent(context,HiddenContacts.class);         		 
        	 }
    	 }
    	 notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		 notificationIntent.putExtra("sms", true);
    	 return notificationIntent;
    }
//    private Notification notif;
    public void createSMSNotification(int newMessages){
    	
    if(HiddenContacts.SHOWING_HIDDEN_CONTACTS){return;}	
	 // instantiate
	 int icon = NOTIFICATION_ICON;
//	 CharSequence tickerText = null;
	 
	 
	 
	 
	 long when = System.currentTimeMillis();
	 //create it
	 Notification notification = new Notification(icon, TICKER_TEXT, when);
	 notification.flags = Notification.FLAG_AUTO_CANCEL;
	 if(prefs.playNotifSound()){
		 notification.defaults |= Notification.DEFAULT_SOUND;
	 }
	 if(prefs.playNotifVibration()){
		 notification.defaults |= Notification.DEFAULT_VIBRATE;
	 }
	 
	 //add latest event info
	 //set the pending intent
	 Intent notificationIntent = getNotificationIntent();
	// notificationIntent.setAction(SMSIntent.GO_TO_SMS);
	 PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

	 
	 CharSequence contentTitle = NOTIF_TITLE;
	 CharSequence contentText = NOTIF_SUBTITLE.replaceAll("%s", new StringBuilder().append(newMessages).toString());


	 
//	 CharSequence contentText = newMessages+" new issues detected with your audio streams";
	 notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
	 nMgr.notify(SMS_NOTIF_ID, notification);
    }
    
    public void cancelSMSNotification(){
	try{
	    nMgr.cancel(SMS_NOTIF_ID);
	}catch(Exception e){}
    }
    
//    Notification notif;
    public void createCallNotification(int newMessages){
	 // instantiate
	 int icon = NOTIFICATION_ICON;
	 long when = System.currentTimeMillis();
	 //create it
	 Notification notification = new Notification(icon, TICKER_TEXT, when);
	 notification.flags = Notification.FLAG_AUTO_CANCEL;
	 if(prefs.playNotifSound()){
		 notification.defaults |= Notification.DEFAULT_SOUND;
	 }
	 if(prefs.playNotifVibration()){
		 notification.defaults |= Notification.DEFAULT_VIBRATE;
	 }
	 //add latest event info
	 Intent notificationIntent = getNotificationIntent();
//	 notificationIntent.setAction(SMSIntent.GO_TO_CALL_LOGS);
	 PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
	 
	 
	 CharSequence contentTitle = NOTIF_TITLE;
	 CharSequence contentText = NOTIF_SUBTITLE.replaceAll("%s", new StringBuilder().append(newMessages).toString());

	 notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
	 nMgr.notify(CALL_NOTIF_ID, notification);
    }
    
    
    public void cancelCallNotification(){
	try{
		//apply temporary notification block
	    nMgr.cancel(CALL_NOTIF_ID);
	}catch(Exception e){}
    }
    
}
