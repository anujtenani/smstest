package com.smartanuj.sms;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.smartanuj.dbnew.PrefManager;

public class ThemedActivity extends ActionBarActivity{

	PrefManager prefs;
	Resources r;
	@Override
	public void onCreate(Bundle icicle){
		prefs = PrefManager.getInstance(this);
		r = getResources();
		prefs.changeThemes(this);
		super.onCreate(icicle);
	}
	
	public void start(Class cls){
		startActivity(new Intent(this,cls));
	}
}
