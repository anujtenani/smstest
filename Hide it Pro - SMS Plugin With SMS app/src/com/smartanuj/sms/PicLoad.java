package com.smartanuj.sms;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import com.smartanuj.dbnew.PrefManager;
import com.smartanuj.http.HttpUtils;
import com.smartanuj.imageutils.ImageUtils;
import com.sony.zoomview.DynamicZoomControl;
import com.sony.zoomview.ImageZoomView;
import com.sony.zoomview.LongPressZoomListener;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

public class PicLoad extends Activity{
	
	
	private String URL;
	File bitmapFile;
	ImageZoomView image;
	ProgressBar pBar;
	HttpUtils http;
	DynamicZoomControl mZoomControl;
	
	@Override
	public void onCreate(Bundle icicle){

		super.onCreate(icicle);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.picload);
		
		String uri = getIntent().getExtras().getString("uri");
		String otp = getIntent().getExtras().getString("otp");
		URL = PrefManager.URI_PREFIX+"data/"+uri+"/"+otp;
//		Log.i("Anuj", "Loading :"+URL);
		PrefManager prefs = PrefManager.getInstance(this);
		bitmapFile = new File(prefs.getMMSPicturesDir(),uri);
		
		image = (ImageZoomView) findViewById(R.id.imageView1);
		mZoomControl = new DynamicZoomControl();
		LongPressZoomListener mZoomListener = new LongPressZoomListener(getApplicationContext());
		mZoomListener.setZoomControl(mZoomControl);
		image.setOnTouchListener(mZoomListener);
		mZoomControl.setAspectQuotient(image.getAspectQuotient());
		
		image.setZoomState(mZoomControl.getZoomState());
		image.setImage(BitmapFactory.decodeResource(getResources(), R.drawable.empty));		
		resetZoomState();
		pBar = (ProgressBar) findViewById(R.id.progressBar1);
		http = new HttpUtils();
		new LoadTask().execute();
	}
	  private void resetZoomState() {
			mZoomControl.getZoomState().setPanX(0.5f);
			mZoomControl.getZoomState().setPanY(0.5f);
			mZoomControl.getZoomState().setZoom(1f);
			mZoomControl.getZoomState().notifyObservers();
	    }

	
	Bitmap mBit;
	
	private class LoadTask extends AsyncTask<Void,Void,Boolean>{

		@Override
		protected Boolean doInBackground(Void... params) {
			//download the file 
			try {
				if(bitmapFile.exists() && bitmapFile.canRead()){
					mBit = ImageUtils.retrieveBitmap(bitmapFile.getAbsolutePath());					
				}
				if(mBit==null){
						if(URL != null && bitmapFile!=null){
							http.copy(URL, bitmapFile);
						}
						mBit = ImageUtils.retrieveBitmap(bitmapFile.getAbsolutePath());					
				}
				return true;
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			return false;
		}
		@Override
		public void onPreExecute(){
			pBar.setVisibility(View.VISIBLE);
		}
		
		@Override
		public void onPostExecute(Boolean what){
			if(mBit!=null){
				image.setImage(mBit);
//				Log.i("Anuj", "width:"+mBit.getWidth()+":height:"+mBit.getHeight());
				resetZoomState();
				//set bitmap
			}else{
				
				//show retry dialog;
			}
			pBar.setVisibility(View.GONE);
		}
		
	}
	
	@Override
	public boolean onKeyDown(int keyCode,KeyEvent event){
		switch(keyCode){
		case KeyEvent.KEYCODE_BACK:
			http.cancelHttp();
		}
		return super.onKeyDown(keyCode, event);
	}

}
