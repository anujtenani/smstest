package com.smartanuj.sms;

import android.content.Context;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.regex.Matcher;
import com.smartanuj.arrowview.RoundedRectWithTriangle;
import com.smartanuj.dbnew.DbHelper;
import com.smartanuj.dbnew.MainDb;
import com.smartanuj.dbnew.PrefManager;
import com.smartanuj.dbnew.PrimaryDb;
import com.smartanuj.fileutils.FileUtils;
import com.smartanuj.imageutils.MyThumbUtil;
import com.smartanuj.sms.obj.BlackListObj;
import com.smartanuj.sms.obj.CallObj;
import com.smartanuj.sms.obj.SMSIntent;
import com.smartanuj.sms.obj.SMSObj;
import com.smartanuj.sms.obj.SmileySheet;
import com.smartanuj.sms.obj.WebMMSObj;
import com.smartanuj.sms.obj.WebMMSObj.ToUpload;
import com.smartanuj.sms.util.ImageLoader;
import com.smartanuj.sms.util.NotificationUtil;
import com.smartanuj.sms.util.Utils;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.ClipboardManager;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.text.style.ImageSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.AdapterView.OnItemClickListener;

public class CompoundView extends ThemedActivity {

	ArrayList<SMSObj> smsData = new ArrayList<SMSObj>();
	private ArrayList<CallObj> calldata = new ArrayList<CallObj>();
//	MainDb db;
	private PrimaryDb db;
	private BlackListObj bObj;
	private NotificationUtil nUtil;
	private Resources r;
	private boolean smileysEnabled;
	
	private Integer[] screens = new Integer[1];

	int selectedPos;
	private int selected;

	private MySMSAdapterNew smsadapter;
	private MyCallAdapter calladapter;
	private EditText edit;
	
//	private ImageButton attachFiles;
	private TextView charCounter;
	private ProgressBar smsPBar, callPBar;
	
	public static int SHOWING_PERSON_ID = -1;
	
	final TextWatcher mTextEditorWatcher = new TextWatcher() {
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,int count) {
			int len = s.length();
			int c = ((len-1) / 160) + 1;
			int charLeft = (160 * c) - len;
			charCounter.setText(charLeft + "/" + c);
		}

		@Override
		public void afterTextChanged(Editable s) {
		}
	};

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.compound);
		setIntentData();
		r = getResources();
		smileysEnabled = prefs.smileysEnabled();
		
//		db = new MainDb(this);
		db = DbHelper.getPrimaryDb(this);
		setupView();

		if (bObj.name != null && !bObj.name.equals("")) {
			setTitle(bObj.name + " " + bObj.phone);
		} else {
			setTitle(bObj.phone);
		}
		final Bundle b = getIntent().getExtras();
		Uri u = b.getParcelable(Intent.EXTRA_STREAM);
		int orientation = b.getInt("orientation");
		if(u!=null){
		final ToUpload toUpload = new ToUpload(u.getPath(),orientation);
		if(toUpload.path != null){
			vp.post(new Runnable(){public void run(){
					showMMSView(toUpload);				
				}
				});
			}
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.compound, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.clear:
			switch (screens[vp.getCurrentItem()]) {
			case Screens.SMS_SCREEN:
				showSmsDeleteConfirmDialog();
				break;
			case Screens.CALL_SCREEN:
				showCallDeleteConfirmDialog();
				break;

			}
			break;
		case R.id.editContact:
			Intent x = new Intent(getApplicationContext(), AddContact.class);
			x.putExtra("bundle", Utils.convertToBundle(bObj));
			startActivity(x);
			break;

		case android.R.id.home:
			finish();
			break;
		case R.id.attach:
			Intent i = new Intent(Intent.ACTION_PICK);
			i.setType("image/*");
//			Intent chooserIntent = Intent.createChooser(i, "Pick");
			
			Intent customPick = new Intent("android.intent.action.PICK_CUSTOM");
			customPick.setType("image/*");
			
			Intent hideItPro = new Intent("android.intent.action.PICK_HIP");
			hideItPro.setType("image/*");
			Intent chooserIntent = Intent.createChooser(i, "Pick Image");
			chooserIntent.putExtra("android.intent.extra.INITIAL_INTENTS", new Intent[] {hideItPro, customPick });
//			startActivity(chooserIntent);
			
			startActivityForResult(chooserIntent,1);
			break;
		}

		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean onKeyDown(int keyCode,KeyEvent keyEvent){
		switch(keyCode){
		case KeyEvent.KEYCODE_BACK:
			if(screens[vp.getCurrentItem()] == Screens.SMS_SCREEN){
				if(edit.hasFocus()){
					edit.clearFocus();
					return true;
				}
			}
			break;
		}
		return super.onKeyDown(keyCode, keyEvent);
	}

	
	LoadSMSData smsLoader;
	
	private void loadSMSData() {
		if (smsadapter != null
				&& (smsLoader == null || smsLoader.getStatus() != AsyncTask.Status.RUNNING)) {
			smsLoader = new LoadSMSData();
			smsLoader.execute();
		}
	}

	LoadCallData callLoader;

	private void loadCallData() {
		if (calladapter != null
				&& (callLoader == null || callLoader.getStatus() != AsyncTask.Status.RUNNING)) {
			callLoader = new LoadCallData();
			;
			callLoader.execute();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		SHOWING_PERSON_ID = bObj.id;
//		Log.i("Anuj","SHOWINGID:"+bObj.id);
//		LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
		if(bObj.hide_sms){
		loadSMSData();
		IntentFilter smsfilter = new IntentFilter(SMSIntent.SMS_DELIVERED);
		smsfilter.addAction(SMSIntent.SMS_RECEIVED);
		smsfilter.addAction(SMSIntent.SMS_SENT);
		//manager.registerReceiver(mSMSReciever, smsfilter);
		registerReceiver(mSMSReciever, smsfilter);
		
		}
		
		if(bObj.hide_call_log){
		loadCallData();
		IntentFilter callfilter = new IntentFilter(SMSIntent.CALL_TYPE_INCOMING);
		callfilter.addAction(SMSIntent.CALL_TYPE_MISSED);
		callfilter.addAction(SMSIntent.CALL_TYPE_OUTGOING);
		//manager.registerReceiver(mCallReciever, callfilter);
		registerReceiver(mCallReciever, callfilter);
		}
		this.editSetDraft();

	}
	private void editSetDraft(){
		try{
		if(edit!=null){
			String draft = Utils.getDraft(getApplicationContext(), bObj.phone);
			edit.setText(draft);
		}
		}catch(Exception e){}

	}

	@Override
	public void onPause() {
		super.onPause();
		SHOWING_PERSON_ID = -1;
//		LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
		if(bObj.hide_sms){
//			manager.unregisterReceiver(mSMSReciever);
			unregisterReceiver(mSMSReciever);
			db.markReadSMS(bObj.id);
		}
		
		if(bObj.hide_call_log){
//			manager.unregisterReceiver(mCallReciever);
			unregisterReceiver(mCallReciever);
			db.markReadCallLogOf(bObj.id);
		}
		try{
		Utils.saveDraft(this, bObj.phone, edit.getText().toString());
		}catch(Exception e){
			e.printStackTrace();
		}

	}
	

	/*
	 * private void setActionTitle(int arg0){
	 * Log.i("Anuj","screen:"+screens[arg0]); switch(screens[arg0]){ case
	 * Screens.SMS_SCREEN: Log.i("Anuj","messages");
	 * setTitle("Messages from "+bObj.phone); break; case Screens.CALL_SCREEN:
	 * Log.i("Anuj","callLog"); setTitle("Call Logs of "+bObj.phone); break; } }
	 */
	private ViewPager vp;

	private void setupView() {
		// set the screens to show.

		if (bObj.hide_sms && bObj.hide_call_log) {
			screens = new Integer[] { Screens.SMS_SCREEN, Screens.CALL_SCREEN };
		} else if (bObj.hide_sms) {
			screens = new Integer[] { Screens.SMS_SCREEN };
		} else if (bObj.hide_call_log) {
			screens = new Integer[] { Screens.CALL_SCREEN };
		}
		// setActionTitle(0);

		vp = (ViewPager) findViewById(R.id.viewPager);

		vp.setAdapter(new MyPagerAdapter(this));

		// Bind the title indicator to the adapter
		if(screens.length<2){
			findViewById(R.id.pagerTabStrip).setVisibility(View.GONE);
///			View v = ((ViewStub)findViewById(R.id.indicator_stub)).inflate();
//			TitlePageIndicator titleIndicator = (TitlePageIndicator) 			v.findViewById(R.id.titles);
//			titleIndicator.setViewPager(vp);
		}
		

	}
	WebMMSObj mmsObj;
	private void sendSMS(String text){
		//if edit text is gone make sure you upload the stuff correctly;
		if(mmsView!=null&&mmsView.getTag() != null){
			//upload the stuff first
			TelephonyManager tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
			smsPBar.setIndeterminate(false);
			smsPBar.setMax(100);
			smsPBar.setVisibility(View.VISIBLE);
			ToUpload toUp = (ToUpload) mmsView.getTag();
			File f = new File(toUp.path);
			
			mmsObj = new WebMMSObj(prefs.getMMSPicturesDir(),bObj.phone,tel.getLine1Number(),f,toUp.orientation,true, new WebMMSObj.ProgressListener() {
				
				@Override
				public void onResult(final String result) {
					runOnUiThread(new Runnable(){
						public void run(){
							smsPBar.setIndeterminate(true);
							smsPBar.setVisibility(View.GONE);
							if(result!=null){
							Matcher m = PrimaryDb.pattern.matcher(result);
								if(m.find()){
									hideMMSView();							
									sendSMS(result);
									return;
								}
							}
							showError(result);
						}
					});
				}
				
				@Override
				public void onPercentUpload(final int percent) {
					runOnUiThread(new Runnable(){
						
						public void run(){
							smsPBar.setProgress(percent);
						}
					});
				}

				@Override
				public void onCancelled() {
					runOnUiThread(new Runnable(){
						public void run(){
							smsPBar.setIndeterminate(true);
							smsPBar.setVisibility(View.GONE);
							showError("Cancelled");					
							}
						});

				}

				@Override
				public void onError() {
					runOnUiThread(new Runnable(){
						public void run(){
							smsPBar.setIndeterminate(true);
							smsPBar.setVisibility(View.GONE);
							showError("Unable to connect to internet");					
							}
						});
				}

			});
			mmsObj.doSend();
			showToast("Sending file...");	
			return;
		}
		if(text.trim().length() < 1){
			return;
		}
		SMSObj obj = new SMSObj();
		obj.text = text;
		// obj.from = "self";
		obj.tid = bObj.id;
		obj.timestamp = System.currentTimeMillis();
		obj.subject = "";
		
		obj.type = MainDb.TYPE_OUTGOING_SENDING;
		smsData.add(obj);
		smsadapter.notifyDataSetChanged();
		smsListView.postDelayed(new Runnable(){

			@Override
			public void run() {
				smsListView.setSelection(smsData.size());
				
			}
			
		},1000);

		noSMSMsg.setVisibility(TextView.GONE);
		edit.setText("");
		
//		obj.type = MainDb.TYPE_OUTGOING;
		obj.mid = db.addToMessages(obj);
		new SendSMSTask().execute(obj);
	}
	private class SendSMSTask extends AsyncTask<SMSObj,Void,Void>{

		@Override
		protected Void doInBackground(SMSObj... arg0) {
			SMSObj obj = arg0[0];
			
			SmsManager mgr = SmsManager.getDefault();
				
			if (obj.text.length() > 160) {
				ArrayList<String> parts = mgr.divideMessage(obj.text);
				int numParts = parts.size();
				ArrayList<PendingIntent> sentIntents = new ArrayList<PendingIntent>();
				ArrayList<PendingIntent> deliveryIntents = new ArrayList<PendingIntent>();

				for (int i = 0; i < numParts; i++) {
					sentIntents.add(SentBroadcast(obj));
					deliveryIntents.add(DeliveredBroadcast(obj));
				}
				mgr.sendMultipartTextMessage(bObj.phone, null, parts, sentIntents,
						deliveryIntents);
			} else {
				mgr.sendTextMessage(bObj.phone, null, obj.text, SentBroadcast(obj),DeliveredBroadcast(obj));
			}
			
			
			return null;
		}
		
		@Override
		protected void onPreExecute(){
			
			

			
		}
		
	}

	private TextView noCallMsg, noSMSMsg;
	private ListView smsListView,callListView;
	
	 private class MyPagerAdapter extends PagerAdapter {
		LayoutInflater inflater;

		public MyPagerAdapter(Context ctx) {
			inflater = (LayoutInflater) ctx
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			return screens.length;
		}

		@Override
		public Object instantiateItem(View collection, int position) {
			View v = null;
			switch (screens[position]) {
			case Screens.SMS_SCREEN:
				v = inflater.inflate(R.layout.sms_thread, null);
				smsadapter = new MySMSAdapterNew(CompoundView.this,inflater);
				smsListView = (ListView) v.findViewById(R.id.listView1);
				smsListView.setAdapter(smsadapter);
				smsListView.setOnItemClickListener(new OnItemClickListener(){

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,		int position, long arg3) {
						if(smsData.get(position).type == PrimaryDb.TYPE_OUTGOING_FAILED){
							showSMSRetryDialog(smsData.get(position));
						}else{
							Matcher m = PrimaryDb.pattern.matcher(smsData.get(position).text);
							if(m.find()){
								//show the image
								Intent i = new Intent(getApplicationContext(),PicLoad.class);
								i.putExtra("uri", m.group(1));
								i.putExtra("otp", m.group(2));
								startActivity(i);
							}
						}
					}
					
				});
				smsListView.setOnItemLongClickListener(new OnItemLongClickListener(){

					@Override
					public boolean onItemLongClick(AdapterView<?> arg0,
							View arg1, int arg2, long arg3) {
						if (edit.hasFocus()) {
							edit.clearFocus();
						} else {
							selectedPos = arg2;
							showSMSOptions();
						}						
						return false;
					}
					
				});
				
				charCounter = (TextView) v.findViewById(R.id.textView2);
				charCounter.setVisibility(View.GONE);
		/*		attachFiles = (ImageButton)v.findViewById(R.id.imageButton1);
				attachFiles.setVisibility(View.GONE);
				attachFiles.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						Intent i = new Intent(Intent.ACTION_PICK);
						i.setType("image/*");
//						Intent chooserIntent = Intent.createChooser(i, "Pick");
						
						Intent customPick = new Intent("android.intent.action.PICK_CUSTOM");
						customPick.setType("image/*");
						
						Intent hideItPro = new Intent("android.intent.action.PICK_HIP");
						hideItPro.setType("image/*");
						Intent chooserIntent = Intent.createChooser(i, "Pick Image");
						chooserIntent.putExtra("android.intent.extra.INITIAL_INTENTS", new Intent[] {hideItPro, customPick });
//						startActivity(chooserIntent);
						
						startActivityForResult(chooserIntent,1);
					}
					
				});
				*/
				edit = (EditText) v.findViewById(R.id.editText1);
				editSetDraft();
				edit.setOnFocusChangeListener(new OnFocusChangeListener() {

					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						if (hasFocus) {
//							edit.setLines(4);
							if(smsData.size()>0){
								smsListView.setSelection(smsData.size()-1);
							}
							charCounter.setVisibility(View.VISIBLE);
//							attachFiles.setVisibility(View.VISIBLE);
							// charCounter.setText(String.valueOf(edit.getText().toString().length()));

						} else {
							charCounter.setVisibility(View.GONE);
//							attachFiles.setVisibility(View.GONE);
//							edit.setLines(1);
							hideKeyboard();
						}
					}

				});
				edit.addTextChangedListener(mTextEditorWatcher);
				final ImageButton send = (ImageButton) v.findViewById(R.id.button1);
				send.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						hideKeyboard();
						edit.clearFocus();
						try {
								sendSMS(edit.getText().toString());
						} catch (Exception e) {
							e.printStackTrace();
							showToast(getString(R.string.error));
						}
					}
				});
				smsPBar = (ProgressBar) v.findViewById(R.id.progressBar1);
				smsPBar.setVisibility(View.VISIBLE);
				noSMSMsg = (TextView) v.findViewById(R.id.textView1);
				noSMSMsg.setVisibility(View.GONE);
				if (smsData.size() == 0) {
					loadSMSData();
				}
				mmsLayout = (LinearLayout) v.findViewById(R.id.linearLayout1);
				removeMMS = (ImageButton) v.findViewById(R.id.imageButton2);
				removeMMS.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						if(mmsObj!=null){
							mmsObj.cancelUpload();
						}
						hideMMSView();
					}
					
				});
				mmsView = (TextView)v.findViewById(R.id.textView3);

				
				break;
			case Screens.CALL_SCREEN:
				v = inflater.inflate(R.layout.calllog, null);

				callListView = (ListView) v
						.findViewById(R.id.listView1);
				calladapter = new MyCallAdapter(inflater);
				callListView.setAdapter(calladapter);

				callListView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						selectedPos = arg2;
						showCallOptions();
					}

				});
				noCallMsg = (TextView) v.findViewById(R.id.textView1);
				noCallMsg.setVisibility(View.GONE);

				callPBar = (ProgressBar) v.findViewById(R.id.progressBar1);
				callPBar.setVisibility(View.VISIBLE);
				if (calldata.size() == 0) {
					loadCallData();
				}
				break;
			}
			((ViewPager) collection).addView(v, 0);
			return v;

		}

	
		@Override
		public void destroyItem(View collection, int position, Object view) {
			((ViewPager) collection).removeView((View) view);
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == (object);
		}

		
		
		  @Override
     	  public CharSequence getPageTitle(int position){
     		  switch(screens[position]){
     		  case Screens.SMS_SCREEN:
     			  return getString(R.string.messages);
     		  case Screens.CALL_SCREEN:
     			  return getString(R.string.call_logs);
     		  }
     		  return "";
     	  }

		
	}

	// async tasks
	 
	private class LoadSMSData extends AsyncTask<Void, Void, ArrayList<SMSObj>> {

		@Override
		protected ArrayList<SMSObj> doInBackground(Void... params) {
			return db.getMessages(bObj.id);
		}

		@Override
		protected void onPostExecute(ArrayList<SMSObj> ret) {
			smsPBar.setVisibility(View.GONE);
			if (ret.size() == 0) {
				noSMSMsg.setText(R.string.no_sms_msg);
				noSMSMsg.setVisibility(View.VISIBLE);
				hideKeyboard();
			} else {
				noSMSMsg.setVisibility(View.GONE);
			}
			
			smsData = ret;
			smsadapter.notifyDataSetChanged();
			if(smsData.size()>0){
				smsListView.setSelection(smsData.size()-1);
			}
		
		}

		@Override
		protected void onPreExecute() {
			noSMSMsg.setVisibility(View.GONE);
			smsPBar.setVisibility(View.VISIBLE);
		}
	}

	private class LoadCallData extends AsyncTask<Void, Void, ArrayList<CallObj>> {

		@Override
		protected ArrayList<CallObj> doInBackground(Void... params) {
			if (bObj == null) {
				db.markReadAllCallLogs();
				return db.getCallLogs();
			} else {
				db.markReadCallLogOf(bObj.id);
				return db.getCallLogs(bObj.id);
			}
		}

		@Override
		protected void onPostExecute(ArrayList<CallObj> ret) {
			callPBar.setVisibility(View.GONE);

			if (ret.size() == 0) {
				noCallMsg.setText(R.string.no_call_msg);
				noCallMsg.setVisibility(View.VISIBLE);
			} else {
				noCallMsg.setVisibility(View.GONE);
			}
			calldata = ret;
			calladapter.notifyDataSetChanged();
		}

		@Override
		protected void onPreExecute() {
			noCallMsg.setVisibility(View.GONE);
			callPBar.setVisibility(View.VISIBLE);
		}
	}

	// adapters
/*
	private class MySMSAdapter extends BaseAdapter {

		RelativeLayout.LayoutParams params;
		LayoutInflater mInflater;

		public MySMSAdapter(LayoutInflater mInflater) {
			params = new RelativeLayout.LayoutParams(
					LayoutParams.FILL_PARENT,
					LayoutParams.FILL_PARENT);
			this.mInflater = mInflater;
		}

		@Override
		public int getCount() {
			return smsData.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewSMSHolder holder;
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.sms_thread_listview,
						parent, false);
				
				
				holder = new ViewSMSHolder();
				holder.sms = (TextView) convertView.findViewById(R.id.textView1);
				holder.date = (TextView) convertView.findViewById(R.id.textView2);
	//			holder.rl =(RelativeLayout)convertView.findViewById(R.id.relativeLayout1);
				// holder.sender = (TextView)findViewById(R.id.textView3);
				convertView.setTag(holder);
			} else {
				holder = (ViewSMSHolder) convertView.getTag();
			}
			SMSObj obj = smsData.get(position);
			
			if(smileysEnabled){
			SpannableStringBuilder spannable = new SpannableStringBuilder(obj.text);
			
			int count = SmileySheet.sheet.length;
			for(int i=0;i<count;i++){
				Matcher m = SmileySheet.sheet[i].matcher(obj.text);
				while(m.find()){
					spannable.setSpan(new ImageSpan(getApplicationContext(),SmileySheet.icon[i],ImageSpan.ALIGN_BASELINE), m.start(), m.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
				}
				
			}
			
			
			holder.sms.setText(spannable);
			}else{
				holder.sms.setText(obj.text);				
			}
//			if(obj.type == MainDb.TYPE_OUTGOING_SENDING){
//				holder.date.setText("Sending...");
//			}else{
				holder.date.setText(Utils.betterTime(obj.timestamp));
//			}
			params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.FILL_PARENT);

			if (obj.type == MainDb.TYPE_INCOMING) {
				holder.sms.setBackgroundResource(R.drawable.rcvd);
				params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
				holder.sms.setLayoutParams(params);
				holder.date.setGravity(Gravity.LEFT);
			} else {
				holder.sms.setBackgroundResource(R.drawable.sent);
				params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
				holder.sms.setLayoutParams(params);
				holder.date.setGravity(Gravity.RIGHT);
			}
			return convertView;

		}

	}

	static class ViewSMSHolder {
		public TextView sms, date;
//		public RelativeLayout rl;
	}
	/*
	private class MySMSAdapterNew extends BaseAdapter {

		LayoutInflater mInflater;
		private int pixels_20,pixels_5;
		Activity a;
		ImageLoader loader;
		public MySMSAdapterNew(Activity a,LayoutInflater mInflater) {
			this.mInflater = mInflater;
			pixels_20 = (int) inPixels(20);
			pixels_5 = (int) inPixels(5);
			this.a = a;
			this.loader = new ImageLoader(a);
		}
		private float inPixels(float dip){
			return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, getResources().getDisplayMetrics());
		}

		@Override
		public int getCount() {
			return smsData.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewSMSHolderNew holder;
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.sms_thread_listview_new,parent, false);
				holder = new ViewSMSHolderNew();
				holder.sms = (TextView) convertView.findViewById(R.id.textView1);
				holder.date = (TextView) convertView.findViewById(R.id.textView2);
				holder.drawable = new RoundedRectWithTriangle(pixels_20);
//				holder.drawable.setArrowPosition(RoundedRectWithTriangle.ARROW_TOP_LEFT);
//				holder.drawable.setBgColor(Color.GRAY);
				holder.drawable.cornerRadius(0.0f);
				convertView.setTag(holder);
			} else {
				holder = (ViewSMSHolderNew) convertView.getTag();
			}
			SMSObj obj = smsData.get(position);
			
			Log.i("Anuj", obj.type+" type");
			switch(obj.type){
			case PrimaryDb.TYPE_OUTGOING_FAILED:
				holder.drawable.setBgColor(Color.RED);
				break;
			case PrimaryDb.TYPE_OUTGOING_SENDING:
				holder.drawable.setBgColor(Color.GRAY);
				break;
			default:
				holder.drawable.setBgColor(Color.DKGRAY);
				break;
					
			}
			Matcher m1  = PrimaryDb.pattern.matcher(obj.text);

			if(m1.find()){
				holder.image.setVisibility(View.VISIBLE);
				loader.DisplayImage(m1.group(1), m1.group(2), a, holder.image);
			
			}else if(smileysEnabled){
				holder.image.setVisibility(View.GONE);
			SpannableStringBuilder spannable = new SpannableStringBuilder(obj.text);
			
			int count = SmileySheet.sheet.length;
			for(int i=0;i<count;i++){
				Matcher m2 = SmileySheet.sheet[i].matcher(obj.text);
				while(m2.find()){
					spannable.setSpan(new ImageSpan(getApplicationContext(),SmileySheet.icon[i],ImageSpan.ALIGN_BASELINE), m2.start(), m2.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
				}
				
			}
				holder.sms.setText(spannable);
			}else{
				holder.image.setVisibility(View.GONE);
				holder.sms.setText(obj.text);				
			}
			holder.date.setText(Utils.betterTime(obj.timestamp));
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
//			holder.sms.setMinimumWidth(100);
			if (obj.type == MainDb.TYPE_INCOMING) {
				holder.drawable.setArrowPosition(RoundedRectWithTriangle.ARROW_TOP_LEFT);
				holder.sms.setBackgroundDrawable(holder.drawable);
				holder.sms.setPadding(pixels_5, pixels_20/2+pixels_5, pixels_5, pixels_5);
				params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
				holder.sms.setLayoutParams(params);
				holder.date.setGravity(Gravity.LEFT);
				holder.sms.setGravity(Gravity.LEFT);
			} else  {
				holder.drawable.setArrowPosition(RoundedRectWithTriangle.ARROW_BOTTOM_RIGHT);
				holder.sms.setBackgroundDrawable(holder.drawable);
				holder.sms.setPadding(pixels_5, pixels_5, pixels_5,pixels_20/2+pixels_5);
				params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
				holder.sms.setLayoutParams(params);
				holder.sms.setGravity(Gravity.RIGHT);
				holder.date.setGravity(Gravity.RIGHT);
			}

			return convertView;

		
		}

		

	}

	
	static class ViewSMSHolderNew {
		public TextView sms,date;
		public ImageView image;
		public RoundedRectWithTriangle drawable;
	}
	*/
	
	private class MySMSAdapterNew extends BaseAdapter {

		LayoutInflater mInflater;
		private int pixels_20,pixels_5;
		Activity a;
		ImageLoader loader;
		public MySMSAdapterNew(Activity a,LayoutInflater mInflater) {
			this.mInflater = mInflater;
			pixels_20 = (int) inPixels(24);
			pixels_5 = (int) inPixels(5);
			this.a = a;
			this.loader = new ImageLoader(a,prefs.getMMSPicturesDir()+"/.thumbs");
		}
		private float inPixels(float dip){
			return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, getResources().getDisplayMetrics());
		}

		@Override
		public int getCount() {
			return smsData.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			SMSObj obj = smsData.get(position);
			RowHolder holder;
			if(obj.type == PrimaryDb.TYPE_INCOMING){
			//	IncomingRowHolder holder;
				if (convertView == null) {
					RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
					params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

					convertView = mInflater.inflate(R.layout.sms_thread_listview_new,parent, false);
					holder = new RowHolder();
					holder.image = (ImageView)convertView.findViewById(R.id.imageView1);
					holder.sms = (TextView) convertView.findViewById(R.id.textView1);
					holder.sms.setTextColor(Color.WHITE);
					holder.date = (TextView) convertView.findViewById(R.id.textView2);
					
					holder.drawable = new RoundedRectWithTriangle(pixels_20);
					holder.drawable.cornerRadius(0.0f);
					holder.drawable.setArrowPosition(RoundedRectWithTriangle.ARROW_TOP_LEFT);
					holder.group = (LinearLayout) convertView.findViewById(R.id.group);
					holder.group.setLayoutParams(params);
					holder.group.setPadding(pixels_5, pixels_20/2+pixels_5, pixels_5, pixels_5);
					holder.group.setBackgroundDrawable(holder.drawable);
					holder.sms.setGravity(Gravity.LEFT);
					holder.date.setGravity(Gravity.LEFT);
					
					convertView.setTag(holder);
				} else {
					holder = (RowHolder) convertView.getTag();
				}
			}else{
			//	OutgoingRowHolder holder;
				if (convertView == null) {
					RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
					params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					convertView = mInflater.inflate(R.layout.sms_thread_listview_new,parent, false);
					holder = new RowHolder();
					holder.image = (ImageView)convertView.findViewById(R.id.imageView1);
					holder.sms = (TextView) convertView.findViewById(R.id.textView1);
					holder.sms.setTextColor(Color.WHITE);
					holder.date = (TextView) convertView.findViewById(R.id.textView2);
					holder.drawable = new RoundedRectWithTriangle(pixels_20);
					holder.drawable.cornerRadius(0.0f);
					holder.drawable.setArrowPosition(RoundedRectWithTriangle.ARROW_BOTTOM_RIGHT);
					
					holder.group = (LinearLayout) convertView.findViewById(R.id.group);
					holder.group.setLayoutParams(params);
					holder.group.setPadding(pixels_5, pixels_5, pixels_5,pixels_20/2+pixels_5);
					holder.group.setBackgroundDrawable(holder.drawable);
					holder.sms.setGravity(Gravity.RIGHT);

					holder.date.setGravity(Gravity.RIGHT);
					convertView.setTag(holder);
				} else {
					holder = (RowHolder) convertView.getTag();
				}

			}
			
			switch(obj.type){
			case PrimaryDb.TYPE_OUTGOING_FAILED:
				holder.drawable.setBgColor(Color.RED);
				break;
			case PrimaryDb.TYPE_OUTGOING_SENDING:
				holder.drawable.setBgColor(0xc3005493);
				break;
			case PrimaryDb.TYPE_INCOMING:
				holder.drawable.setBgColor(0xc300a51d);				
				break;
			case PrimaryDb.TYPE_OUTGOING_DELIVERED:
				holder.drawable.setBgColor(0xc30086ec);
				break;
			default:
				holder.drawable.setBgColor(0xc30078d3);
				break;
			}
			//holder.group.invalidate();
			//holder.group.setBackgroundDrawable(holder.drawable);

			Matcher m1  = PrimaryDb.pattern.matcher(obj.text);

			if(m1.find()){
				
				holder.image.setVisibility(View.VISIBLE);
				loader.DisplayImage(m1.group(1), m1.group(2), a, holder.image,R.drawable.empty);
				holder.sms.setVisibility(View.GONE);
			}else if(smileysEnabled){
			
			SpannableStringBuilder spannable = new SpannableStringBuilder(obj.text);
			int count = SmileySheet.sheet.length;
			for(int i=0;i<count;i++){
				Matcher m2 = SmileySheet.sheet[i].matcher(obj.text);
				while(m2.find()){
					spannable.setSpan(new ImageSpan(getApplicationContext(),SmileySheet.icon[i],ImageSpan.ALIGN_BASELINE), m2.start(), m2.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
				}
			}
				holder.sms.setText(spannable);
				holder.image.setVisibility(View.GONE);
				holder.sms.setVisibility(View.VISIBLE);

			}else{
				holder.image.setVisibility(View.GONE);
				holder.sms.setVisibility(View.VISIBLE);
				holder.sms.setText(obj.text);	

			}
			holder.date.setText(DateUtils.getRelativeTimeSpanString(CompoundView.this, obj.timestamp, true));
//			holder.date.setText(Utils.betterTime(obj.timestamp));


			return convertView;

		
		}
		

		public int getViewTypeCount() {
		    return 2;
		}
		public int getItemViewType(int position) {

		    //we have an image so we are using an ImageRow
		    if (smsData.get(position).type == PrimaryDb.TYPE_INCOMING) return 0;
		    //we don't have an image so we are using a Description Row
		    else return 1;
		}		

	}
	
	static class RowHolder {
		public TextView sms,date;
		public ImageView image;
		public LinearLayout group;
		public RoundedRectWithTriangle drawable;
	}


	private class MyCallAdapter extends BaseAdapter {

		LayoutInflater mInflater;

		public MyCallAdapter(LayoutInflater mInflater) {
			this.mInflater = mInflater;
		}

		@Override
		public int getCount() {
			return calldata.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewCallHolder holder;
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.calllog_listview,
						parent, false);
				holder = new ViewCallHolder();
				holder.name = (TextView) convertView
						.findViewById(R.id.textView1);
				holder.phone = (TextView) convertView
						.findViewById(R.id.textView2);
				holder.type = (ImageView) convertView
						.findViewById(R.id.imageView1);
				holder.date = (TextView) convertView
						.findViewById(R.id.textView3);

				convertView.setTag(holder);
			} else {
				holder = (ViewCallHolder) convertView.getTag();
			}

			CallObj call = calldata.get(position);
			switch (call.type) {
			case android.provider.CallLog.Calls.INCOMING_TYPE:
				holder.type.setImageResource(R.drawable.sym_call_incoming);
				break;
			case android.provider.CallLog.Calls.MISSED_TYPE:
				holder.type.setImageResource(R.drawable.sym_call_missed);
				break;
			case android.provider.CallLog.Calls.OUTGOING_TYPE:
				holder.type.setImageResource(R.drawable.sym_call_outgoing);
				break;
			}

			holder.name.setText(call.name);
			holder.phone.setText(call.phone);
			holder.date.setText(DateUtils.getRelativeTimeSpanString(CompoundView.this, call.timestamp, true));

//			holder.date.setText(Utils.betterTime(call.timestamp));

			return convertView;

		}

	}

	static class ViewCallHolder {
		public TextView name, phone, date;
		public ImageView type;
	}

	// popup dialogs
	private void showSMSOptions() {
		selected = 0;
		CharSequence[] items = r.getStringArray(R.array.sms_options);
		new AlertDialog.Builder(this)
				.setSingleChoiceItems(items, selected,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,int which) {
								selected = which;
							}

						})
				.setPositiveButton(android.R.string.ok,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								switch (selected) {
								case 10:
									try {
										startActivity(Utils.callIntent(bObj.phone));
									} catch (Exception e) {

									}
									break;
								case 1:
									// copy the text
									ClipboardManager clip = (ClipboardManager) getApplicationContext()
											.getSystemService(
													Context.CLIPBOARD_SERVICE);
									clip.setText(smsData.get(selectedPos).text);
									showToast(getString(R.string.text_copied_to_clipboard));
									break;
								case 0:
									// delete this message
									int id = db.deleteMessage(smsData.get(selectedPos));
									if (id == 1) {
										smsData.remove(selectedPos);
										smsadapter.notifyDataSetChanged();
									} else if (id < 1) {
										showToast(getString(R.string.error));
									}
									break;
								case 3:
									// share the text
									Intent i = new Intent(Intent.ACTION_SEND);
									i.putExtra(Intent.EXTRA_TEXT,
											smsData.get(selectedPos).text);
									i.setType("plain/text");
									startActivity(Intent.createChooser(i,getString(R.string.send_via)));
									break;
								}
							}

						})
				.setNegativeButton(android.R.string.cancel,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {

							}

						}).create().show();
	}

	private void showCallOptions() {
		selected = 0;
		CharSequence[] items = new CharSequence[] {
				getString(R.string.call) +" "+ calldata.get(selectedPos).phone, r.getString(R.string.delete) };
		new AlertDialog.Builder(this)
				.setSingleChoiceItems(items, selected,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,int which) {
								selected = which;
							}

						})
				.setPositiveButton(android.R.string.ok,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								switch (selected) {
								case 0:
									try {
										startActivity(Utils.callIntent(calldata
												.get(selectedPos).phone));
									} catch (Exception e) {
										e.printStackTrace();
									}
									break;
								case 1:
									// delete this message
									int id = db.deleteCallLog(calldata
											.get(selectedPos).cid);
									if (id == 1) {
										calldata.remove(selectedPos);
										calladapter.notifyDataSetChanged();
									} else if (id < 1) {
										showToast(getString(R.string.error));
									}
									break;
								}
							}

						})
				.setNegativeButton(android.R.string.cancel,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {

							}

						}).create().show();
	}

	private void showCallDeleteConfirmDialog() {
		new AlertDialog.Builder(this)
				.setMessage(R.string.delete_call_log_confirm)
				.setPositiveButton("Delete All",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								if (bObj != null) {
									db.deleteAllLogOf(bObj.id);
								} else {
									db.clearLog();
								}
								loadCallData();
							}
						})
				.setNegativeButton(android.R.string.cancel,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {

							}
						}).create().show();
	}

	private void showSmsDeleteConfirmDialog() {
		new AlertDialog.Builder(this)
				.setMessage(R.string.delete_sms_log_confirm)
				.setPositiveButton(R.string.delete,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,int which) {
								db.deleteAllMessagesFromUser(bObj.id);
								loadSMSData();
							}
						})
				.setNegativeButton(android.R.string.cancel,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						}).create().show();
	}

	// /misc functions and broadcast recievers
	private void setIntentData() {
		Bundle b = getIntent().getBundleExtra("bundle");
		bObj = Utils.convertToBlacklistObj(b);
	}

	private void hideKeyboard() {
		InputMethodManager imm = (InputMethodManager) getApplicationContext()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(edit.getApplicationWindowToken(), 0);
	}

	private final BroadcastReceiver mCallReciever = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (bObj == null) {
				loadCallData();
				new NotificationUtil(context).cancelCallNotification();
				return;
			}

			Bundle b = intent.getExtras();
			if (b.getInt("id") == bObj.id) {
				loadCallData();
				new NotificationUtil(context).cancelCallNotification();
				return;
			}
		}

	};

	private PendingIntent SentBroadcast(SMSObj obj) {
		Intent i = new Intent(SMSIntent.SMS_SENT);
		i.putExtras(Utils.converSMSToBundle(obj));
		return PendingIntent.getBroadcast(this, 111, i, PendingIntent.FLAG_UPDATE_CURRENT);
	}

	private PendingIntent DeliveredBroadcast(SMSObj obj) {
		if(prefs.delivertReports()){
			Intent i = new Intent(SMSIntent.SMS_DELIVERED);
			i.putExtras(Utils.converSMSToBundle(obj));
			return PendingIntent.getBroadcast(this, 111, i, PendingIntent.FLAG_UPDATE_CURRENT);
		}
		return null;
	}

	private final BroadcastReceiver mSMSReciever = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			
			String action = intent.getAction();
			if (action.equals(SMSIntent.SMS_SENT)) {
				SMSObj obj = Utils.converBundleToSMSObj(intent.getExtras());
				int resultCode = getResultCode();
				switch (resultCode) {
				case Activity.RESULT_OK:
					obj.type = PrimaryDb.TYPE_OUTGOING_SENT;
					break;
				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
				case SmsManager.RESULT_ERROR_NO_SERVICE:
				case SmsManager.RESULT_ERROR_NULL_PDU:
				case SmsManager.RESULT_ERROR_RADIO_OFF:
					obj.type = PrimaryDb.TYPE_OUTGOING_FAILED;
					break;
				}
				for(SMSObj sms:smsData){
					if(sms.mid == obj.mid){
						sms.type = obj.type;
					}
				}try{
				smsadapter.notifyDataSetChanged();
				}catch(Exception e){}
			} else if (action.equals(SMSIntent.SMS_DELIVERED)) {
				SMSObj obj = Utils.converBundleToSMSObj(intent.getExtras());
				obj.type = PrimaryDb.TYPE_OUTGOING_DELIVERED;
				for(SMSObj sms:smsData){
					if(sms.mid == obj.mid){
						sms.type = obj.type;
					}
				}
				smsadapter.notifyDataSetChanged();
			}else if (intent.getAction().equals(SMSIntent.SMS_RECEIVED)) {
				Bundle b = intent.getExtras();
				SMSObj obj = Utils.converBundleToSMSObj(b);
				if (b.getInt("id") == bObj.id) {
					if (nUtil == null) {
						nUtil = new NotificationUtil(context);
					}
					nUtil.cancelSMSNotification();
					smsData.add(obj);
					smsadapter.notifyDataSetChanged();
					smsListView.post(new Runnable(){
						public void run(){
							smsListView.setSelection(smsData.size()-1);
						}
					});
//					loadSMSData();
//					smsData = db.getMessages(bObj.id);
//					smsadapter.notifyDataSetChanged();

				}
			}
		}

	};

	private static final int REQUEST_PICK_FILE = 1;
	public void onActivityResult(int requestCode,int resultCode,Intent i){
		if(resultCode == Activity.RESULT_OK && requestCode == REQUEST_PICK_FILE){
					Uri data = i.getData();
					String path = null;
					int orientation = 0;
					if(data.toString().startsWith("content")){
						//content uri
						Cursor c = getContentResolver().query(data, new String[]{MediaColumns.DATA,MediaColumns.DISPLAY_NAME,MediaColumns.SIZE,MediaStore.Images.ImageColumns.ORIENTATION}, null	,null,null);
						int dataColumn = c.getColumnIndex(MediaColumns.DATA);
						int sizeColumn = c.getColumnIndex(MediaColumns.SIZE);
						int name = c.getColumnIndex(MediaColumns.DISPLAY_NAME);
						int orientationIndex = c.getColumnIndex(MediaStore.Images.ImageColumns.ORIENTATION);
						if(c!=null&&dataColumn > -1){
							c.moveToFirst();
							path = c.getString(dataColumn);
							if(orientationIndex > -1){
								orientation = c.getInt(orientationIndex);
							}
						}
					}else{
						path = data.getPath();
					}
				if(path!=null){
					showMMSView(path,orientation);
				}
		}		
	}

	 
	private LinearLayout mmsLayout;
	private ImageButton removeMMS;
	private TextView mmsView;

	private void showMMSView(ToUpload toUpload){
	File toSend = new File(toUpload.path);
	Log.i("Anuj", toSend.getAbsolutePath());
	mmsObj = null;
	if(toUpload.path!= null && FileUtils.Mime.isImage(toUpload.path) && mmsLayout!=null){
		mmsLayout.setVisibility(View.VISIBLE);
//			removeMMS.setVisibility(View.VISIBLE);
//			mmsView.setVisibility(View.VISIBLE);
		edit.setVisibility(View.GONE);
		//put an thumbnail in edittext;
		Bitmap bit = MyThumbUtil.getSquareThumbnail(toUpload.path, MyThumbUtil.KIND_MICRO,toUpload.orientation);
		if(bit!=null){
			SpannableStringBuilder builder = new SpannableStringBuilder();
			builder.append("image");
			builder.setSpan(new ImageSpan(this,bit), 0, 5,SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
			mmsView.setText(builder);
			mmsView.setTag(toUpload);
			}else{
				hideMMSView();
			}
		}
	}
	
	private void showMMSView(String path, int orientation){
		showMMSView(new ToUpload(path,orientation));
	}
	
	private void hideMMSView(){
		edit.setVisibility(View.VISIBLE);
		mmsLayout.setVisibility(View.GONE);
	
//		removeMMS.setVisibility(View.GONE);
//		mmsView.setVisibility(View.GONE);
		mmsView.setTag(null);
	}
	
	private void showSMSRetryDialog(final SMSObj obj){
		new AlertDialog.Builder(this)
		.setItems(new CharSequence[]{"Retry","Delete"}, new DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch(which){
				case 0:
					obj.timestamp = System.currentTimeMillis();
					new SendSMSTask().execute(obj);
					//retry sending;
				case 1:
					//delete the sms;
				}
			}
			
		}).setPositiveButton(android.R.string.cancel, new DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
			
		}).create().show();
	}
	
	private void showToast(String msg) {
		Crouton.clearCroutonsForActivity(this);
		Crouton.showText(this, msg, Style.CONFIRM);
	}
	private void showError(String msg){
		Crouton.clearCroutonsForActivity(this);
		Crouton.showText(this, msg, Style.ALERT);
	}

	 class Screens {

			public static final int SMS_SCREEN = 1;
			public static final int CALL_SCREEN = 2;
		}

}
