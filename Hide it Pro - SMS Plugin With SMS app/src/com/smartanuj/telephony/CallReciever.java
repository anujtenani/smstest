package com.smartanuj.telephony;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Set;
import com.smartanuj.dbnew.DbHelper;
import com.smartanuj.dbnew.PrimaryDb;
import com.smartanuj.sms.obj.BlackListObj;
import com.smartanuj.sms.obj.CallObj;
import com.smartanuj.sms.obj.SMSIntent;
import com.smartanuj.sms.obj.TempObj;
import com.smartanuj.sms.util.NotificationUtil;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.BaseColumns;
import android.provider.CallLog;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;

public class CallReciever extends BroadcastReceiver{
   

    private Context ctx;
    private PrimaryDb bDb;    
    @Override
	public void onReceive(Context context, Intent intent) {
//	this.intent = intent;
	ctx = context;
        Bundle bundle = intent.getExtras();
        if(null == bundle)                return; 
    
        Set<String> keys = bundle.keySet();
/*        for (String key : keys) {
           Log.i("Error", key + "="+ bundle.getString(key));
        }   
       Log.i("Error","----------------------");
 */      
//       bDb = new MainDb(context);
        bDb = DbHelper.getPrimaryDb(ctx);
 //       tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        //the following states are possible
        //state nothing --> precedes outgoing call , has intent.extra_phone_number
        //state ringing --> has phonenumber in incoming_number
        //state offhook --> does not have any data
        //state idle --> incoming number only, no outgoing number

        //so basically do this
        //add the incoming/outgoing number in a database , then in idle state wait for 1-2 seconds
        //then query the calllog for that number --> retrieve it and put it in our log , deleting the log
        // show the notification if call was blocked
        
        
        String state = bundle.getString(TelephonyManager.EXTRA_STATE);
        if(state==null){
            //that means its an outgoing call
      //  	Log.i("Anuj","outgoing");
            String phone = bundle.getString(Intent.EXTRA_PHONE_NUMBER);
      //      Log.i("Anuj","Phone:"+phone);
            if(phone!=null){
                BlackListObj black = bDb.isBlackList(phone);
               
                if(black.id>0){
                	 if(black.block_outgoing_call){
             	         abortCallNew();
//             	         cancelNotificationNew();
                    }
                	//add this to temp database
              	   if(black.hide_call_log){
                TempObj temp = new TempObj();
                temp.name = black.name;
                temp.phone = phone;
                temp.type = CallLog.Calls.OUTGOING_TYPE;
                temp.UID = black.id;
                temp.showNotif = black.showCallnotification;
                SingleTon.black.add(temp);
              	   }                
//                new TempDb(context).addToTemp(temp);
 	   //    		Log.i("Anuj","added to temp outgoing");
                }
            }
        }
        else if(state.equals(TelephonyManager.EXTRA_STATE_RINGING)){
     //   	Log.i("Anuj","incoming");
            //get the phone number
            String phone = bundle.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
        //    Log.i("Anuj",phone);
            if(phone!=null){
               BlackListObj black = bDb.isBlackList(phone);
              
               if(black.id>0){
            	   
            	   if(black.block_incoming_call){
            	         abortCallNew();
            	         cancelNotificationNew();
                   	}
        //    	   Log.i("Anuj","id = "+black.id);
         //   	   Log.i("Anuj","hide log = "+black.hide_call_log);

        	   if(black.hide_call_log){

            	   
        		   //add this to a temp database
            	   //and in IDLE check if there are any pending call log deletion to be performed and do that
        	       TempObj temp = new TempObj();
        	       temp.name = black.name;
        	       temp.phone = phone;
        	       temp.type = CallLog.Calls.INCOMING_TYPE;
        	       temp.UID = black.id;
                   temp.showNotif = black.showCallnotification;
                   SingleTon.black.add(temp);

//        	       new TempDb(context).addToTemp(temp);
        //	       Log.i("Anuj","added to temp incoming");
        //	       ContentResolver resolver = ctx.getContentResolver();
         //          Cursor c = resolver.query(CallLog.Calls.CONTENT_URI,null,CallLog.Calls.NUMBER+"=?",new String[]{temp.phone},CallLog.Calls.DEFAULT_SORT_ORDER);
        //           c.registerContentObserver(new MyDataObserver(null));
        	   }
        	       
             }
             }
        }else if(state.equals(TelephonyManager.EXTRA_STATE_IDLE)){
       // 	Log.i("Anuj","idle");
            hideFromLog();
	    
        }
        /*
        try {
            //detect if the call is incoming call or outgoing call
            
            
            // Java reflection to gain access to TelephonyManager's
            // ITelephony getter
     //       tm.listen(mListener, PhoneStateListener.LISTEN_CALL_STATE);
              
            String incomingNumber = bundle.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
            
            if (incomingNumber.equals("")){
             //     Log.v(TAG,"BYE BYE BYE" );
            }
            /*
            else{

            telephonyService.answerRingingCall();
            Log.v(TAG,"HELLO HELLO HELLO" );
            }
            


        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,
                    "FATAL ERROR: could not connect to telephony subsystem");
            Log.e(TAG, "Exception object: " + e);
        }

*/
        }
/*
    IncomingListener mListener;
    private class IncomingListener extends PhoneStateListener{
		    @Override
		    public void onCallStateChanged(int state,String incomingNumber){
			switch(state){
			case TelephonyManager.CALL_STATE_IDLE:
			    Log.i("Anuj","IDLE");
		//	    getLastCallLogEntry(ctx);
//			    Log.i("Anuj","Phone:"+phoneNumber);
		//	    Log.i("Anuj","Phone:"+incomingNumber);
		//	    Timer t= new Timer();
//			    tm.listen(mListener, PhoneStateListener.LISTEN_NONE);
//			    t.schedule(new TimerTask(){
//				@Override
//				public void run() {
//				    cr.delete(CallLog.Calls.CONTENT_URI, CallLog.Calls.NUMBER+"="+phoneNumber, null);				    
//				}
//			    },3000);
			    break;
			case TelephonyManager.CALL_STATE_OFFHOOK:
			    Log.i("Anuj","OUTGOING:"+intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER));
			    break;
			case TelephonyManager.CALL_STATE_RINGING:
			    Log.i("Anuj","INCOMING:"+incomingNumber);
			    break;
			}
		    }
      
    }
    */
 
    private void abortCallNew(){
   // 	Log.i("Anuj","Aborting call");
    	 try {
             //String serviceManagerName = "android.os.IServiceManager";
             String serviceManagerName = "android.os.ServiceManager";
             String serviceManagerNativeName = "android.os.ServiceManagerNative";
             String telephonyName = "com.android.internal.telephony.ITelephony";

             Class telephonyClass;
             Class telephonyStubClass;
             Class serviceManagerClass;
             Class serviceManagerStubClass;
             Class serviceManagerNativeClass;
             Class serviceManagerNativeStubClass;

             Method telephonyCall;
             Method telephonyEndCall;
             Method telephonyAnswerCall;
             Method getDefault;

             Method[] temps;
             Constructor[] serviceManagerConstructor;

             // Method getService;
             Object telephonyObject;
             Object serviceManagerObject;

             telephonyClass = Class.forName(telephonyName);
             telephonyStubClass = telephonyClass.getClasses()[0];
             serviceManagerClass = Class.forName(serviceManagerName);
             serviceManagerNativeClass = Class.forName(serviceManagerNativeName);

             Method getService = // getDefaults[29];
                     serviceManagerClass.getMethod("getService", String.class);

             Method tempInterfaceMethod = serviceManagerNativeClass.getMethod(
                     "asInterface", IBinder.class);

             Binder tmpBinder = new Binder();
             tmpBinder.attachInterface(null, "fake");

             serviceManagerObject = tempInterfaceMethod.invoke(null, tmpBinder);
             IBinder retbinder = (IBinder) getService.invoke(serviceManagerObject, "phone");
             Method serviceMethod = telephonyStubClass.getMethod("asInterface", IBinder.class);

             telephonyObject = serviceMethod.invoke(null, retbinder);
             //telephonyCall = telephonyClass.getMethod("call", String.class);
             telephonyEndCall = telephonyClass.getMethod("endCall");
             //telephonyAnswerCall = telephonyClass.getMethod("answerRingingCall");

             telephonyEndCall.invoke(telephonyObject);

         } catch (Exception e) {
             e.printStackTrace();
         }
    }
    /*
    private void abortCall(){
	try{
			Class c = Class.forName(tm.getClass().getName());
          Method m = c.getDeclaredMethod("getITelephony",new Class[0]);
          m.setAccessible(true);
          ITelephony telephonyService = (ITelephony) m.invoke(tm,new Object[0]);  
          telephonyService.silenceRinger();
          telephonyService.endCall();
	}catch(Exception e){
	    e.printStackTrace();
	}

    }
    */
    
    private void cancelNotificationNew(){
//    	Log.i("Anuj","Cancel Notification");

   	 try {
            //String serviceManagerName = "android.os.IServiceManager";
            String serviceManagerName = "android.os.ServiceManager";
            String serviceManagerNativeName = "android.os.ServiceManagerNative";
            String telephonyName = "com.android.internal.telephony.ITelephony";

            Class telephonyClass;
            Class telephonyStubClass;
            Class serviceManagerClass;
            Class serviceManagerStubClass;
            Class serviceManagerNativeClass;
            Class serviceManagerNativeStubClass;

            Method telephonyCall;
            Method telephonyEndCall;
            Method telephonyAnswerCall;
            Method getDefault;

            Method[] temps;
            Constructor[] serviceManagerConstructor;

            // Method getService;
            Object telephonyObject;
            Object serviceManagerObject;

            telephonyClass = Class.forName(telephonyName);
            telephonyStubClass = telephonyClass.getClasses()[0];
            serviceManagerClass = Class.forName(serviceManagerName);
            serviceManagerNativeClass = Class.forName(serviceManagerNativeName);

            Method getService = // getDefaults[29];
                    serviceManagerClass.getMethod("getService", String.class);

            Method tempInterfaceMethod = serviceManagerNativeClass.getMethod(
                    "asInterface", IBinder.class);

            Binder tmpBinder = new Binder();
            tmpBinder.attachInterface(null, "fake");

            serviceManagerObject = tempInterfaceMethod.invoke(null, tmpBinder);
            IBinder retbinder = (IBinder) getService.invoke(serviceManagerObject, "phone");
            Method serviceMethod = telephonyStubClass.getMethod("asInterface", IBinder.class);

            telephonyObject = serviceMethod.invoke(null, retbinder);
            //telephonyCall = telephonyClass.getMethod("call", String.class);
            telephonyEndCall = telephonyClass.getMethod("cancelMissedCallsNotification");
            //telephonyAnswerCall = telephonyClass.getMethod("answerRingingCall");

            telephonyEndCall.invoke(telephonyObject);

        } catch (Exception e) {
            e.printStackTrace();
        }
   }
    /*
    private void cancelNotification(){
	try{
	  Class c = Class.forName(tm.getClass().getName());
          Method m = c.getDeclaredMethod("getITelephony");
          m.setAccessible(true);
          com.android.internal.telephony.ITelephony telephonyService = (ITelephony) m.invoke(tm);  
          telephonyService = (ITelephony) m.invoke(tm);
          telephonyService.cancelMissedCallsNotification();
	}catch(Exception e){
	    e.printStackTrace();
	}

    }
    */
   
    private void hideFromLog(){
//	final TempDb temp = new TempDb(ctx);
//	final ArrayList<TempObj>test = temp.getAllFromTemp();
//	Log.i("Anuj",SingleTon.black.size()+" singleton");
    final ArrayList<TempObj> test = new ArrayList<TempObj>();
	test.addAll(SingleTon.black);
	SingleTon.black.clear();
	
	for(TempObj t:test){
	    if(t.showNotif){
	    	new NotificationUtil(ctx).createCallNotification(test.size());
	    	break;
	    	}
		}
        if(test.size()>0){
        	  cancelNotificationNew();
        }

        new Thread(){
        	public void run(){
        		int tries = 0;
        		int maxTries = 10;
//        		Log.i("Anuj", "starting thread");
        		while(test.size()>0){
	//				Log.i("Anuj", "looping:size:"+test.size());

        			if(tries > maxTries){
      //          		Log.i("Anuj", "exceeded maxtries");
        				break;
        			}
        			try{
        				int length = test.size();
        				for(int i=0;i<length;i++){
        //					Log.i("Anuj", "deleting:"+test.get(0).phone);
        					if(deleteFromLog(test.get(0))){
        						if(test.size() == 1){test.clear();}
        						else{ test.remove(0);      }
        	//					Log.i("Anuj", "removgin:"+test.get(0)+":size:"+test.size());
        					}
        				}
        				tries++;
        				try {
							sleep(1000);
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}
        			}catch(Exception e){
        				e.printStackTrace();
        				tries++;
        				try {
							sleep(1000);
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}
        			}
        		}
        	}
        	
        }.start();
 /*       
        Timer timer= new Timer();
        timer.schedule(new TimerTask(){
		@Override
		public void run() {
//		    Log.i("Anuj","Running tasks");
			try{
		 	runTheTasks(test);
			}catch(Exception e){
				
			}
		}
	},4000);
*/

    }
    
    private boolean deleteFromLog(TempObj t){
//		Log.i("Anuj", "deletinhhug from log "+t.phone);
//		Log.i("Anuj","Processing:"+t.phone);
		ContentResolver resolver = ctx.getContentResolver();
		Cursor c = resolver.query(CallLog.Calls.CONTENT_URI,null,CallLog.Calls.NUMBER+"=?",new String[]{t.phone},CallLog.Calls.DEFAULT_SORT_ORDER);
//		Log.i("Anuj","cnt:"+c.getCount());
		int del = 0;
		if(c!=null&&c.getCount()>0){
			c.moveToFirst();
			for(int i=0;i<c.getCount();i++){
			CallObj obj = new CallObj();
			obj.uid = t.UID;
			obj.duration = c.getLong(c.getColumnIndexOrThrow(CallLog.Calls.DURATION));
			obj.timestamp= c.getLong(c.getColumnIndexOrThrow(CallLog.Calls.DATE));
			obj.type = c.getInt(c.getColumnIndexOrThrow(CallLog.Calls.TYPE));
			obj.phone = t.phone;
			obj.name = t.name;
			obj.cid = c.getInt(c.getColumnIndexOrThrow(BaseColumns._ID));
			//now we have the object add it to calls db
			bDb.addToCallLog(obj);
			bDb.updateLastTimestamp(t.UID,System.currentTimeMillis());
			int deleted = resolver.delete(CallLog.Calls.CONTENT_URI, BaseColumns._ID+"="+obj.cid,null);
//			Log.i("Anuj", "deleted:"+deleted);
			if(deleted > 0){
				
					sendBroadcast(obj);
				}
			del += deleted;
//			Log.i("Anuj", "del:"+del);
			c.moveToNext();
			}
			c.close();
//			Log.i("Anuj", "del:"+del);
			if(del > 0){
//				Log.i("Anuj", "returinig");
				return true;
			}
			
//			cancelNotificationNew();
		}
		return false;
    }
    
    private void sendBroadcast(CallObj obj){
    	LocalBroadcastManager manager = LocalBroadcastManager.getInstance(ctx);
		Intent i = new Intent(SMSIntent.CALL_TYPE_INCOMING);
		i.putExtra("id",obj.uid);
		manager.sendBroadcast(i);

    }
    /*
   private class MyDataObserver extends ContentObserver{

	public MyDataObserver(Handler handler) {
	    super(handler);
	}
	
	@Override
	public void onChange(boolean selfChange){
//		final TempDb temp = new TempDb(ctx);
//		final ArrayList<TempObj>test = temp.getAllFromTemp();
//	 	runTheTasks(test);
	}
    } 
    */
}
    
