package com.smartanuj.telephony;


import java.util.ArrayList;

import com.smartanuj.sms.obj.BlackListObj;
import com.smartanuj.sms.obj.CallObj;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.CallLog;

public class CallLogHelper {

    Context context;
    public CallLogHelper(Context ctx){
	context = ctx;
    }
    
    /** delete all calls from callog for returns number of rows affected 
     * based on the type i.e CallLog.Calls.INCOMING_TYPE,CallLog.Calls.MISSED_TYPE
     * or CallLog.Calls.OUTGOING_TYPE , or CallLogHelper.TYPE_ALL;
     * */
    public static final int TYPE_ALL = -1;
    public int deleteLogForNumber(String phone,int type){
	int rows = -1;
	
	try{
	ContentResolver cr = context.getContentResolver();
	switch(type){
	case TYPE_ALL:
		rows = cr.delete(CallLog.CONTENT_URI, CallLog.Calls.NUMBER+"="+phone,null);
		break;
	case CallLog.Calls.INCOMING_TYPE:
		rows = cr.delete(CallLog.CONTENT_URI, CallLog.Calls.NUMBER+"="+phone+" AND "+CallLog.Calls.TYPE+"="+CallLog.Calls.INCOMING_TYPE,null);
		break;	    
	case CallLog.Calls.OUTGOING_TYPE:
		rows = cr.delete(CallLog.CONTENT_URI, CallLog.Calls.NUMBER+"="+phone+" AND "+CallLog.Calls.TYPE+"="+CallLog.Calls.OUTGOING_TYPE,null);
		break;	    
	case CallLog.Calls.MISSED_TYPE:
		rows = cr.delete(CallLog.CONTENT_URI, CallLog.Calls.NUMBER+"="+phone+" AND "+CallLog.Calls.TYPE+"="+CallLog.Calls.MISSED_TYPE,null);
		break;	    
	
	}

	}catch(Exception e){e.printStackTrace();}
	return rows;
    }
    
    public ArrayList<CallObj> getCallLog(String phone){
    	ArrayList<CallObj> calls = new ArrayList<CallObj>();
    	try{
    		ContentResolver cr = context.getContentResolver();
    		Cursor c = cr.query(CallLog.CONTENT_URI, null, CallLog.Calls.NUMBER +"="+phone, null, null);
    		if(c!=null){
    			c.moveToFirst();
    			int count = c.getCount();
    			int TIME_INDEX = c.getColumnIndexOrThrow(CallLog.Calls.DATE);
    			int TYPE = c.getColumnIndex(CallLog.Calls.TYPE);
    			int DURATION = c.getColumnIndex(CallLog.Calls.DURATION);
    			for(int i=0;i<count;i++){
    				CallObj call = new CallObj();
    				call.timestamp = c.getLong(TIME_INDEX);
    				call.type  =c.getInt(TYPE);
    				call.duration = c.getLong(DURATION);
    				calls.add(call);
    			}
    		}
    	
    	}catch(Exception e){
    	
    	}
    	return calls;
    }
    
	public void putCallsBack(BlackListObj person,ArrayList<CallObj>calls){
		
		ArrayList<CallObj>inDatabase = getCallLog(person.phone);
		ArrayList<CallObj> callsToAdd = removeDuplicates(calls,inDatabase);

		
		if(calls.size()>0){
		//find existing _id and thread_id;
		ContentResolver cr = context.getContentResolver();
		int size = callsToAdd.size();
		ContentValues[] cvs = new ContentValues[size];
		for(int i=0;i<size;i++){
			CallObj call = callsToAdd.get(i);
			ContentValues cv = new ContentValues();
			cv.put(CallLog.Calls.DATE, call.timestamp);
			cv.put(CallLog.Calls.DURATION, call.duration);
			cv.put(CallLog.Calls.NUMBER, call.phone);
			cv.put(CallLog.Calls.TYPE,call.type);
			cv.put(CallLog.Calls.NEW,0);
			cvs[i] = cv;
		}
		cr.bulkInsert(CallLog.CONTENT_URI, cvs);
		}
	}
    
	 public ArrayList<CallObj>removeDuplicates(ArrayList<CallObj>obj1 , ArrayList<CallObj> obj2){
	    	ArrayList<CallObj>newMsg = new ArrayList<CallObj>();
	    	
	    	ArrayList<Long>timestamps = new ArrayList<Long>();
	    	for(CallObj obj:obj2){
	    		timestamps.add(obj.timestamp);
	    	}
	    	
	    	for(CallObj x:obj1){
	    		if(!timestamps.contains(x.timestamp)){
	    			newMsg.add(x);
	    		}
	    	}
	    	
	    	return newMsg;
	    }
	
    /*
    public int deleteCallLogForCalls(ArrayList<CallObj>calls){
	int rows = -1;
	try{
		ContentResolver cr = context.getContentResolver();
		
	for(CallObj call:calls){
	rows = cr.delete(CallLog.CONTENT_URI, CallLog.Calls._ID+"="+call.id,null);
	}
	}catch(Exception e){e.printStackTrace();}
	return rows;
    }
    */
    /*
    public ArrayList<CallObj> getCallLogForNumber(String phone){
	ArrayList<CallObj>calls=  new ArrayList<CallObj>();
	try{
		ContentResolver cr = context.getContentResolver();
		Cursor c = cr.query(CallLog.CONTENT_URI, new String[]{
			CallLog.Calls._ID,CallLog.Calls.DURATION,CallLog.Calls.TYPE,CallLog.Calls.DATE,
		}, CallLog.Calls.NUMBER+"="+phone,null,null);
		if(c!=null){
		    int count = c.getCount();
		    c.moveToFirst();
		    for(int i=0;i<count;i++){
			CallObj obj = new CallObj();
			obj.duration = c.getLong(c.getColumnIndexOrThrow(CallLog.Calls.DURATION));
			obj.timestamp= c.getLong(c.getColumnIndexOrThrow(CallLog.Calls.DATE));
			obj.type = c.getInt(c.getColumnIndexOrThrow(CallLog.Calls.TYPE));
			obj.phone = phone;
			obj.name = c.getString(c.getColumnIndexOrThrow(CallLog.Calls.CACHED_NAME));
			obj.cid = c.getInt(c.getColumnIndexOrThrow(CallLog.Calls._ID));
			calls.add(obj);
		    }
		}
	}catch(Exception e){
	    e.printStackTrace();
	}
	return calls;
    }
    */

    
}
