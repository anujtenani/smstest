package com.smartanuj.dbnew;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import com.smartanuj.fileutils.FileUtils;
import com.smartanuj.sms.obj.BlackListObj;
import com.smartanuj.sms.obj.CallObj;
import com.smartanuj.sms.obj.SMSObj;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Environment;
import android.telephony.PhoneNumberUtils;


public class MainDb {

    
    
//    private final ContentResolver cr;
    private SQLiteDatabase myDatabase; 
    private final Context ctx;
    
    private static String DB_PATH = "";
    private static final String DB_NAME = "aqua.mp3";
    private static final String BLACKLIST_TABLE = "blacklist";
    private static final String SMS_TABLE = "sms";
    private static final String CALLS_TABLE = "calls";
 //   private static Uri CONTENT_URI;
    public static final String basePath = "/Android/DO_NOT_DELETE/";
    
    public MainDb(Context ctx){
    	File temp = new File(Environment.getExternalStorageDirectory(),basePath);
    	if(temp==null||!temp.exists()){
    		temp.mkdirs();
    	}
    	DB_PATH = temp.getAbsolutePath()+File.separator;
    	this.ctx = ctx;
    }

    
    

    
    //columns name
    private static final String ID = "id";    
    private static final String LAST_SMS_RECEIVED = "last_sms_received";
    private static final String PHONE = "phone";
    private static final String PHONEFORMATTED = "phoneFormatted";

    private static final String NAME = "name";
    private static final String HIDE_SMS = "hide_sms";
    private static final String HIDE_CALL_LOG = "hide_call_log";
    private static final String BLOCK_INCOMING_CALL = "block_incoming_calls";
    private static final String BLOCK_OUTGOING_CALL = "block_outgoing_calls";
    private static final String SMS_NOTIFICATION = "showSMSNotification";
    private static final String CALL_NOTIFICATION = "showCallNotification";
    private static final String ADDED = "added";
    
    
    private static final String UID = "uid";
    private static final String TYPE = "type";
    private static final String DURATION = "duration";
    private static final String SEEN = "seen";

    
    private static final String SUBJECT = "sms_subject";
    private static final String BODY = "sms_body";

    
    /** searches for a phone number in blacklist and returns the blacklistObj if it is */
    public BlackListObj isBlackList(String phone){
	BlackListObj obj=new BlackListObj();
		try{
//			phone = phone.replaceAll("[-+ ]", "");
//			phone =    PhoneNumberUtils.formatNumber(phone);
//			Log.i("Anuj","matching:"+phone);
		    openReadOnly();
		    String sql = "SELECT * FROM "+BLACKLIST_TABLE+"";
		    Cursor c = myDatabase.rawQuery(sql,null);
		    if(c!=null){
				int count = c.getCount();
				c.moveToFirst();
				int PHONE_FORMAT_INDEX = c.getColumnIndexOrThrow(PHONEFORMATTED);

			for(int i=0;i<count;i++){
				String ph = c.getString(PHONE_FORMAT_INDEX);
//				Log.i("Anuj","matching-"+phone+":"+ph);
				if(PhoneNumberUtils.compare(phone, ph)){
				    obj.id = c.getInt(c.getColumnIndexOrThrow(ID));
				    obj.phone = c.getString(c.getColumnIndexOrThrow(PHONE));
				    obj.name = c.getString(c.getColumnIndexOrThrow(NAME));
				    obj.hide_sms = c.getInt(c.getColumnIndexOrThrow(HIDE_SMS)) == 1?true:false;
				    obj.block_incoming_call = c.getInt(c.getColumnIndexOrThrow(BLOCK_INCOMING_CALL)) == 1?true:false;
				    obj.block_outgoing_call = c.getInt(c.getColumnIndexOrThrow(BLOCK_OUTGOING_CALL)) == 1?true:false;
				    obj.hide_call_log = c.getInt(c.getColumnIndexOrThrow(HIDE_CALL_LOG)) == 1?true:false;
				    obj.showSMSnotification = c.getInt(c.getColumnIndexOrThrow(SMS_NOTIFICATION)) == 1?true:false;
				    obj.showCallnotification = c.getInt(c.getColumnIndexOrThrow(CALL_NOTIFICATION)) == 1?true:false;
				    break;
				}

				    c.moveToNext();
				}
		    c.close();
		    }
		    closeDb();	    
		}catch(Exception e){
		    e.printStackTrace();
		}finally{
		    closeDb();
		}
	return obj;
    }
 
    public ArrayList<BlackListObj>getList(){
	ArrayList<BlackListObj>list = new ArrayList<BlackListObj>();
		try{
		    openReadOnly();
		    String sql = "SELECT blacklist.*,COUNT(DISTINCT calls.id) AS callsCount,COUNT( DISTINCT sms.id) as smsCount FROM blacklist LEFT JOIN calls ON calls.uid = blacklist.id AND calls.seen = 0  LEFT JOIN sms ON sms.uid = blacklist.id AND sms.seen = 0  GROUP BY blacklist.id ORDER BY "+LAST_SMS_RECEIVED+" DESC";
		    Cursor c = myDatabase.rawQuery(sql, null);
		    //Cursor c = myDatabase.query(BLACKLIST_TABLE, null, null,null,null,null,LAST_SMS_RECEIVED+" DESC");
		    if(c!=null){
		    	int count = c.getCount();
				c.moveToFirst();
				int ID_INDEX = c.getColumnIndexOrThrow(ID);
				int PHONE_INDEX = c.getColumnIndexOrThrow(PHONE);
				int NAME_INDEX = c.getColumnIndexOrThrow(NAME);
				int HIDE_SMS_INDEX = c.getColumnIndexOrThrow(HIDE_SMS);
				int BLOCK_INCOMING_INDEX = c.getColumnIndexOrThrow(BLOCK_INCOMING_CALL);
				int BLOCK_OUTGOING_INDEX = c.getColumnIndexOrThrow(BLOCK_OUTGOING_CALL);
				int HIDE_CALL_LOG_INDEX = c.getColumnIndexOrThrow(HIDE_CALL_LOG);
				int SMS_NOTIF_INDEX = c.getColumnIndexOrThrow(SMS_NOTIFICATION);
				int CALL_NOTIF_INDEX = c.getColumnIndexOrThrow(CALL_NOTIFICATION);
				int UNREAD_CALLS_COUNT = c.getColumnIndexOrThrow("callsCount");
				int UNREAD_SMS_COUNT = c.getColumnIndexOrThrow("smsCount");
				for(int i=0;i<count;i++){
				    BlackListObj obj = new BlackListObj();
				    obj.id = c.getInt(ID_INDEX);
				    obj.phone = c.getString(PHONE_INDEX);
				    obj.name = c.getString(NAME_INDEX);
				    obj.hide_sms = c.getInt(HIDE_SMS_INDEX) == 1?true:false;
				    obj.block_incoming_call = c.getInt(BLOCK_INCOMING_INDEX) == 1?true:false;
				    obj.block_outgoing_call = c.getInt(BLOCK_OUTGOING_INDEX) == 1?true:false;
				    obj.hide_call_log = c.getInt(HIDE_CALL_LOG_INDEX) == 1?true:false;
				    obj.showSMSnotification = c.getInt(SMS_NOTIF_INDEX) == 1?true:false;
				    obj.showCallnotification = c.getInt(CALL_NOTIF_INDEX) == 1?true:false;
				    obj.unreadCallCount = c.getInt(UNREAD_CALLS_COUNT);
				    obj.unreadSMSCount = c.getInt(UNREAD_SMS_COUNT);
				    list.add(obj);
				    c.moveToNext();
				}
			    c.close();
			}
		    closeDb();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
		    closeDb();
		}
	return list;
    }
    
    private ArrayList<BlackListObj> getBlackListArrayFromCursor(Cursor c){
    	ArrayList<BlackListObj>list = new ArrayList<BlackListObj>();
		int count = c.getCount();
		c.moveToFirst();
		int ID_INDEX = c.getColumnIndexOrThrow(ID);
		int PHONE_INDEX = c.getColumnIndexOrThrow(PHONE);
		int NAME_INDEX = c.getColumnIndexOrThrow(NAME);
		int HIDE_SMS_INDEX = c.getColumnIndexOrThrow(HIDE_SMS);
		int BLOCK_INCOMING_INDEX = c.getColumnIndexOrThrow(BLOCK_INCOMING_CALL);
		int BLOCK_OUTGOING_INDEX = c.getColumnIndexOrThrow(BLOCK_OUTGOING_CALL);
		int HIDE_CALL_LOG_INDEX = c.getColumnIndexOrThrow(HIDE_CALL_LOG);
		int SMS_NOTIF_INDEX = c.getColumnIndexOrThrow(SMS_NOTIFICATION);
		int CALL_NOTIF_INDEX = c.getColumnIndexOrThrow(CALL_NOTIFICATION);
		
		for(int i=0;i<count;i++){
		    BlackListObj obj = new BlackListObj();
		    obj.id = c.getInt(ID_INDEX);
		    obj.phone = c.getString(PHONE_INDEX);
		    obj.name = c.getString(NAME_INDEX);
		    obj.hide_sms = c.getInt(HIDE_SMS_INDEX) == 1?true:false;
		    obj.block_incoming_call = c.getInt(BLOCK_INCOMING_INDEX) == 1?true:false;
		    obj.block_outgoing_call = c.getInt(BLOCK_OUTGOING_INDEX) == 1?true:false;
		    obj.hide_call_log = c.getInt(HIDE_CALL_LOG_INDEX) == 1?true:false;
		    obj.showSMSnotification = c.getInt(SMS_NOTIF_INDEX) == 1?true:false;
		    obj.showCallnotification = c.getInt(CALL_NOTIF_INDEX) == 1?true:false;
		    list.add(obj);
		    c.moveToNext();
		}
		return list;
    }
    
    public int updateLastTimestamp(int id,long timestamp){
		int ret = -1;
		try{
		    openWritable();
		    ContentValues v = new ContentValues();
		    v.put(LAST_SMS_RECEIVED, timestamp);
		    ret = myDatabase.update(BLACKLIST_TABLE,v, ID+"="+id, null);
		    closeDb();	    
		}catch(Exception e){
			e.printStackTrace();
		}finally{
		    closeDb();	    
		}
	
	return ret;
    }
    
    private int deleteUserFromBlacklist(int id){
		int ret = -1;
		try{
		    openWritable();
		    ret = myDatabase.delete(BLACKLIST_TABLE, ID+"="+id, null);
		    closeDb();	    
		}catch(Exception e){
			e.printStackTrace();
		}finally{
		    closeDb();	    
		}
		
	return ret;
    }
    
    public int deleteUserFromBlacklist(BlackListObj obj){
    	return deleteUserFromBlacklist(obj.id);
    }
    

    
    /** will update the user if the phone number already exists
     *  //is being used
     **/
    public int addUserToBlacklist(BlackListObj obj){
	boolean isUpdate = false;
	int ret = -1;
	
		if(obj.id!=0){
		    isUpdate  = true;
		}
	   
	    ContentValues v = new ContentValues();
	    v.put(PHONE, obj.phone);	    
	    v.put(NAME, obj.name);
	    v.put(HIDE_SMS,obj.hide_sms==true?1:0);
	    v.put(HIDE_CALL_LOG, obj.hide_call_log==true?1:0);
	    v.put(BLOCK_INCOMING_CALL,obj.block_incoming_call==true?1:0);
	    v.put(BLOCK_OUTGOING_CALL,obj.block_outgoing_call==true?1:0);
	    v.put(SMS_NOTIFICATION, obj.showSMSnotification == true?1:0);
	    v.put(CALL_NOTIFICATION, obj.showCallnotification == true?1:0);
	    v.put(ADDED,System.currentTimeMillis());
	    v.put(LAST_SMS_RECEIVED, obj.last_sms_recieved);
	    v.put(PHONEFORMATTED, obj.phoneFormatted);
//		Log.i("Anuj","formatted:"+obj.phoneFormatted);

	    try{
	    	openWritable();
		    if(isUpdate){
			ret = myDatabase.update(BLACKLIST_TABLE, v, "id="+obj.id,null);
		    }else{
			ret = (int) myDatabase.insert(BLACKLIST_TABLE, null,v);
		 }
	    closeDb();	    
		    }catch(Exception e){
			e.printStackTrace();
		    }finally{
			closeDb();
		    }
	    return ret;
    }	

    
    
    /** functions from callLog /
    */

    
    /** give a call id and it will delete that particular log message
     * 
     * returns number of rows affected or -1 if error occurred
     * */
    public synchronized int deleteCallLog(int cid){
	int rows = -1;
		try{
			    openWritable();
			    rows =  myDatabase.delete(CALLS_TABLE, ID+"="+cid,null);
			    closeDb();
		
			}catch(Exception e){
				e.printStackTrace();
			}finally{
			    closeDb();
			}
    return rows;
    }
    
    public synchronized ArrayList<CallObj> getCallLogs(int uid){
	 ArrayList<CallObj>obj = new  ArrayList<CallObj>();
	try{
	    openReadOnly();
	    String sql = "SELECT "+CALLS_TABLE+".*,"+BLACKLIST_TABLE+".phone,"+BLACKLIST_TABLE+".name FROM "+CALLS_TABLE+" LEFT JOIN "+BLACKLIST_TABLE+" on "+BLACKLIST_TABLE+".id = "+CALLS_TABLE+".uid WHERE "+CALLS_TABLE+".uid = "+uid+" ORDER BY "+ADDED+" DESC";

	    Cursor c = myDatabase.rawQuery(sql, null);
	    if(c!=null){
		int count = c.getCount();
		c.moveToFirst();
		for(int i = 0;i<count;i++){
		    CallObj call = new CallObj();
		    call.duration = c.getLong(c.getColumnIndex(DURATION));
		    call.timestamp = c.getLong(c.getColumnIndex(ADDED));
		    call.cid = c.getInt(c.getColumnIndex(ID));
		    call.type = c.getInt(c.getColumnIndex(TYPE));
		    call.phone = c.getString(c.getColumnIndexOrThrow(PHONE));
		    call.name = c.getString(c.getColumnIndexOrThrow(NAME));

		    call.uid = uid;
		    obj.add(call);
		    c.moveToNext();
		}
	    c.close();
	    }
	    closeDb();
	}catch(Exception e){ 
		e.printStackTrace();
	}finally{
	    closeDb();
	}
	return obj;
    }
    
    
    public synchronized ArrayList<CallObj> getCallLogs(){
	 ArrayList<CallObj>obj = new  ArrayList<CallObj>();
	try{
	    openReadOnly();
	    String sql = "SELECT "+CALLS_TABLE+".*,"+BLACKLIST_TABLE+".phone,"+BLACKLIST_TABLE+".name FROM "+CALLS_TABLE+" LEFT JOIN "+BLACKLIST_TABLE+" on "+BLACKLIST_TABLE+".id = "+CALLS_TABLE+".uid ORDER BY "+ADDED+" DESC";
	    Cursor c = myDatabase.rawQuery(sql,null);
	    if(c!=null){
		int count = c.getCount();
		c.moveToFirst();
		for(int i = 0;i<count;i++){
		    CallObj call = new CallObj();
		    call.duration = c.getLong(c.getColumnIndex(DURATION));
		    call.timestamp = c.getLong(c.getColumnIndex(ADDED));
		    call.cid = c.getInt(c.getColumnIndex(ID));
		    call.type = c.getInt(c.getColumnIndex(TYPE));
		    call.phone = c.getString(c.getColumnIndexOrThrow(PHONE));
		    call.name = c.getString(c.getColumnIndexOrThrow(NAME));
		    call.uid = c.getInt(c.getColumnIndex(UID));
		    obj.add(call);
		    c.moveToNext();
		}
	    c.close();
	    }
	    closeDb();
	}catch(Exception e){
		e.printStackTrace();
	}finally{
	    closeDb();
	}
	return obj;
   }
    
    public synchronized int addToCallLog(CallObj obj){
		int id = 0;
		ContentValues v = new ContentValues();
		v.put(UID,obj.uid);
		v.put(TYPE, obj.type);
		v.put(SEEN, 0);
		v.put(ADDED,obj.timestamp);
		v.put(DURATION,obj.duration);
		
		try{
		    openWritable();
		   id =  (int) myDatabase.insert(CALLS_TABLE, null,v);
		//   Log.i("Anuj","added to logs:"+obj.phone);
		    closeDb();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
		    closeDb();
		}
		return id;
    }
    
    /** returns a hashmap containing the uid and unread count*/
 /*   private synchronized HashMap<Integer,Integer> getUnseenCount(){
	HashMap<Integer,Integer>counts = new HashMap<Integer,Integer>();
	try{
	    openReadOnly();
	    String sql = "SELECT uid,COUNT(*) as cnt FROM calls WHERE seen=0 GROUP BY uid";
	    Cursor c = myDatabase.rawQuery(sql,null);
	    if(c!=null){
		int size = c.getCount();
		c.moveToFirst();
	    
		for(int i=0;i<size;i++){
		    counts.put(c.getInt(c.getColumnIndexOrThrow(UID)), c.getInt(c.getColumnIndexOrThrow("cnt")));
		    c.moveToNext();
		}
	    c.close();
	    }
	    closeDb();
	}catch(Exception e){
	}finally{
	    closeDb();
	}
	return counts;
    }
   */ 
    /** returns the count of all the unread calls in database **/
 /*
      private synchronized int getTotalUnseenCount(){
 
	int unread = 0;
	try{
	    openReadOnly();
	    String sql = "SELECT COUNT(*) as cnt FROM calls WHERE seen=0";
	    Cursor c = myDatabase.rawQuery(sql,null);
	    if(c!=null){
		int size = c.getCount();
		c.moveToFirst();
	    
		for(int i=0;i<size;i++){
		    unread =  c.getInt(c.getColumnIndexOrThrow("cnt"));
		    c.moveToNext();
		}
	    c.close();
	    }
	    closeDb();
	}catch(Exception e){
	}finally{
	    closeDb();
	}
	return unread;
    }
  */  
    /** returns a hashmap containing the uid and total count*/
  /*  private synchronized HashMap<Integer,Integer>getTotalCountsCall(){
	HashMap<Integer,Integer>counts = new HashMap<Integer,Integer>();
	try{
	    openReadOnly();
	    String sql = "SELECT uid,COUNT(*) as cnt FROM calls GROUP BY uid";
	    Cursor c = myDatabase.rawQuery(sql,null);
	    if(c!=null){
		int size = c.getCount();
		c.moveToFirst();
	    
		for(int i=0;i<size;i++){
		    counts.put(c.getInt(c.getColumnIndexOrThrow(UID)), c.getInt(c.getColumnIndexOrThrow("cnt")));
		    c.moveToNext();
		}
	    c.close();
	    }
	    closeDb();
	}catch(Exception e){
	}finally{
	    closeDb();
	}
	return counts;
    }
    */
    
   /* private synchronized int deleteLogEntry(CallObj obj){
		if(obj.cid==0){
		    return 0;
		}
		int id = 0;
		try{
		    openWritable();
		    id =  (int) myDatabase.delete(CALLS_TABLE,ID+"="+obj.cid, null);
		    closeDb();
	
		}catch(Exception e){
		}finally{
		    closeDb();
		}
		return id;
	    }
    */
    
    public synchronized int deleteAllLogOf(int uid){
	if(uid==0){
	    return 0;
	}
	int id = 0;
	try{
	    openWritable();
	    id =  myDatabase.delete(CALLS_TABLE,UID+"="+uid, null);
	    closeDb();

	}catch(Exception e){
		e.printStackTrace();
	}finally{
	    closeDb();
	}
	return id;
    }
    
    public synchronized int clearLog(){
	int id = 0;
	try{
	    openWritable();
	    id =  myDatabase.delete(CALLS_TABLE,null, null);
	    closeDb();

	}catch(Exception e){
		e.printStackTrace();
	}finally{
	    closeDb();
	}
	return id;
    }
    /**
     * 
     * @param uid = thread to mark read for
     * @return number of rows affected
     */
    public synchronized int markReadCallLogOf(int uid){
	int ret = 0;
	try{
	    openWritable();
	    ContentValues v= new ContentValues();
	    v.put(SEEN, 1);
	    ret =myDatabase.update(CALLS_TABLE, v,UID+"="+uid,null);
	    closeDb();

	}catch(Exception e){
		e.printStackTrace();
	}finally{
	    closeDb();
	}
	return ret;
    }

    
    public synchronized int markReadAllCallLogs(){
	int ret = 0;
	try{
	    openWritable();
	    ContentValues v= new ContentValues();
	    v.put(SEEN, 1);
	    ret = myDatabase.update(CALLS_TABLE, v,null,null);
	    closeDb();

	}catch(Exception e){
		e.printStackTrace();
	}finally{
	    closeDb();
	}
	return ret;
    }
    
    
    
    /** functions from smsDb */
    
    public static final int TYPE_INCOMING = 1;
    public static final int TYPE_OUTGOING = 2;
    public static final int TYPE_OUTGOING_DELIVERED = 3;
    public static final int TYPE_DRAFT = 5;
    public static final int TYPE_OUTGOING_SENDING = 6;

    public synchronized ArrayList<SMSObj> getMessages(int uid){
	 ArrayList<SMSObj>obj = new  ArrayList<SMSObj>();
	try{
	    openReadOnly();
	    Cursor c = myDatabase.query(SMS_TABLE, null,UID+"="+uid, null,null,null, ADDED+" ASC",null);
	    if(c!=null){
			int count = c.getCount();
			c.moveToFirst();
			int ID_INDEX = c.getColumnIndexOrThrow(ID);
			int TEXT_INDEX = c.getColumnIndexOrThrow(BODY);
			int TYPE_INDEX = c.getColumnIndexOrThrow(TYPE);
			int TIMESTAMP_INDEX = c.getColumnIndexOrThrow(ADDED);
//			int FROM_INDEX = c.getColumnIndexOrThrow(ID);
			
		for(int i = 0;i<count;i++){
		    SMSObj sms = new SMSObj();
		    sms.mid = c.getInt(ID_INDEX);
		    sms.text = c.getString(TEXT_INDEX);
		    sms.type = c.getInt(TYPE_INDEX);
		    sms.timestamp = c.getLong(TIMESTAMP_INDEX);
//		    sms.from = c.getString(c.getColumnIndexOrThrow(PHONE));
		    sms.tid = uid;
		    obj.add(sms);
		    c.moveToNext();
		}
		c.close();

	    }
	    closeDb();

	}catch(Exception e){
		e.printStackTrace();
	}finally{
	    closeDb();
	}
	return obj;
    }
    
    public synchronized ArrayList<SMSObj> getMessages(){
   	 ArrayList<SMSObj>obj = new  ArrayList<SMSObj>();
   	try{
   	    openReadOnly();
   	    Cursor c = myDatabase.query(SMS_TABLE,null, null, null,null,null, ADDED+" ASC",null);
   	    if(c!=null){
   			int count = c.getCount();
   			c.moveToFirst();
   			int ID_INDEX = c.getColumnIndexOrThrow(ID);
   			int TEXT_INDEX = c.getColumnIndexOrThrow(BODY);
   			int TYPE_INDEX = c.getColumnIndexOrThrow(TYPE);
   			int TIMESTAMP_INDEX = c.getColumnIndexOrThrow(ADDED);
//   			int FROM_INDEX = c.getColumnIndexOrThrow(ID);
   			int UID_INDEX = c.getColumnIndexOrThrow(UID);
   		for(int i = 0;i<count;i++){
   		    SMSObj sms = new SMSObj();
   		    sms.mid = c.getInt(ID_INDEX);
   		    sms.text = c.getString(TEXT_INDEX);
   		    sms.type = c.getInt(TYPE_INDEX);
   		    sms.timestamp = c.getLong(TIMESTAMP_INDEX);
//   		    sms.from = c.getString(c.getColumnIndexOrThrow(PHONE));
   		    sms.tid = c.getInt(UID_INDEX);
   		    obj.add(sms);
   		    c.moveToNext();
   		}
   		c.close();

   	    }
   	    closeDb();

   	}catch(Exception e){
   		e.printStackTrace();
   	}finally{
   	    closeDb();
   	}
   	return obj;
       }
    
    
    public synchronized int addToMessages(SMSObj obj){
    		int id = 0;
			ContentValues v = new ContentValues();
			v.put(UID,obj.tid);
//			v.put(PHONE,obj.from);
			v.put(TYPE, obj.type);
			v.put(SEEN, 0);
			v.put(SUBJECT, obj.subject);
			v.put(BODY, obj.text);
			v.put(ADDED, obj.timestamp);
		try{
		    openWritable();
		    id =  (int) myDatabase.insert(SMS_TABLE, null,v);
		    closeDb();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
		    closeDb();
		}
	return id;
    }
    
    public synchronized int addToMessages(ArrayList<SMSObj> objs){
		int id = 0;
		openWritable();
		myDatabase.beginTransaction();
		for(SMSObj obj:objs){
		ContentValues v = new ContentValues();
		v.put(UID,obj.tid);
//		v.put(PHONE,obj.from);
		v.put(TYPE, obj.type);
		v.put(SEEN, 0);
		v.put(SUBJECT, obj.subject);
		v.put(BODY, obj.text);
		v.put(ADDED, obj.timestamp);
	    id =  (int) myDatabase.insert(SMS_TABLE, null,v);
		}
		
		myDatabase.setTransactionSuccessful();
		myDatabase.endTransaction();
		return id;
}
    
    public synchronized int deleteMessage(SMSObj obj){
		if(obj.mid==0){
		    return 0;
		}
		int id = 0;
		try{
		    openWritable();
		    id =  myDatabase.delete(SMS_TABLE,ID+"="+obj.mid, null);
		    closeDb();
	
		}catch(Exception e){
			e.printStackTrace();
		}finally{
		    closeDb();
		}
		return id;
    }
    
    public synchronized int deleteAllMessagesFromUser(int uid){
		if(uid==0){
		    return 0;
		}
		int id = 0;
		try{
		    openWritable();
		    id =  myDatabase.delete(SMS_TABLE,UID+"="+uid, null);
		    closeDb();
		}catch(Exception e){
		    e.printStackTrace();
		}finally{
		    closeDb();
		}
		return id;
    }
  
    
    /** returns a hashmap containiung the uid and unread count*/
    /*
    private synchronized HashMap<Integer,Integer> getUnreadCount(){
	HashMap<Integer,Integer>counts = new HashMap<Integer,Integer>();
	try{
	    openReadOnly();
	    String sql = "SELECT uid,COUNT(*) as cnt FROM `sms` WHERE seen=0 GROUP BY uid";
	    Cursor c = myDatabase.rawQuery(sql,null);
	    if(c!=null){
		int size = c.getCount();
		c.moveToFirst();
	    
		for(int i=0;i<size;i++){
		    counts.put(c.getInt(c.getColumnIndexOrThrow("uid")), c.getInt(c.getColumnIndexOrThrow("cnt")));
		    c.moveToNext();
		}
		    c.close();

	    }
	    closeDb();

	}catch(Exception e){
		e.printStackTrace();
	}finally{
	    closeDb();
	}
	return counts;
    }
    
    /** returns the count of all the unread messages in databse **/
    public synchronized int getTotalUnreadSMSCount(){
	int unread = 0;
	try{
	    openReadOnly();
	    String sql = "SELECT COUNT(*) as cnt FROM `sms` WHERE seen=0";
	    Cursor c = myDatabase.rawQuery(sql,null);
	    if(c!=null){
		int size = c.getCount();
		c.moveToFirst();
	    
		for(int i=0;i<size;i++){
		    unread =  c.getInt(c.getColumnIndexOrThrow("cnt"));
		    c.moveToNext();
		}
		    c.close();

	    }
	    closeDb();

	}catch(Exception e){
		e.printStackTrace();

	}finally{
	    closeDb();
	}
	return unread;
    }
    
    /** returns a hashmap containiung the uid and total count*/
 /*   private synchronized HashMap<Integer,Integer>getTotalSMSCounts(){
	HashMap<Integer,Integer>counts = new HashMap<Integer,Integer>();
	try{
	    openReadOnly();
	    String sql = "SELECT uid,COUNT(*) as cnt FROM `sms` GROUP BY uid";
	    Cursor c = myDatabase.rawQuery(sql,null);
	    if(c!=null){
		int size = c.getCount();
		c.moveToFirst();
	    
		for(int i=0;i<size;i++){
		    counts.put(c.getInt(c.getColumnIndexOrThrow("uid")), c.getInt(c.getColumnIndexOrThrow("cnt")));
		    c.moveToNext();
		}
	    c.close();
	    }
	    closeDb();
	}catch(Exception e){
		e.printStackTrace();

	}finally{
	    closeDb();
	}
	return counts;
    }
   */ 
 
    /**
     * 
     * @param uid = thread to mark read for
     * @return number of rows affected
     */
    public synchronized int markReadSMS(int uid){
	int ret = 0;
	try{
	    openWritable();
	    ContentValues v= new ContentValues();
	    v.put(SEEN, 1);
	    ret =myDatabase.update(SMS_TABLE, v,UID+"="+uid,null);
	    closeDb();

	}catch(Exception e){
		e.printStackTrace();

	}finally{
	    closeDb();
	}
	return ret;
    }
    ///////////////////////////////////// crap stuff /////////////////////////
    

	    private  void closeDb(){
	    	if(myDatabase != null){
	    		myDatabase.close();
	    	}
	 //   	 super.close();
	    }
	
    public void createDataBase() throws IOException{
	 
    	boolean dbExist = checkDataBase();
  	//dbExist = false;
    	if(dbExist){
    		//do nothing - database already exist
    	}else{
    	    try {
        	//	emptyDatabases();
    			copyDataBase();
    	//		mirrorTable();
        	//	unzipAndCopy();
    		} catch (IOException e) {
        		e.printStackTrace();
        	}
    	}
 
    }
    

    public boolean checkDataBase(){
    	SQLiteDatabase checkDB = null;
    	String myPath = DB_PATH + DB_NAME;
    	if(new File(myPath).exists()){
    	try{
    		
    		checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
    	}catch(SQLiteException e){
    		//database does't exist yet.
    	}
    	if(checkDB != null){
    		checkDB.close();
    	}
    	}
    	return checkDB != null ? true : false;
    }
    public void renameDatabase(){
    	String myPath = DB_PATH + DB_NAME;
    	FileUtils.IO.renameFile(new File(myPath), new File(DB_PATH,"aqua_updated.mp3"));
    }
    
    private void copyDataBase() throws IOException{
	 
    	//Open your local db as the input stream
   	InputStream myInput = ctx.getAssets().open(DB_NAME);
    	// Path to the just created empty db
    	String outFileName = DB_PATH + DB_NAME;
    	
    	//Open the empty db as the output stream
    	FileOutputStream myOutput = new FileOutputStream(outFileName);
    	
    	//transfer bytes from the inputfile to the outputfile
    	byte[] buffer = new byte[1024*8];
    	int length;

    	while((length = myInput.read(buffer))>0){
    		myOutput.write(buffer,0,length);
    	}


    	//Close the streams
    	myOutput.flush();
    	myOutput.close();
    	myInput.close();
 
    }
    
    private void openReadOnly() throws SQLException{
	try{
        String myPath = DB_PATH + DB_NAME;
    	myDatabase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
	}catch(Exception e){
	    e.printStackTrace();
	}
    }
	
    
    
    private void openWritable() throws SQLException{
	try{
    	String myPath = DB_PATH + DB_NAME;
    	myDatabase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
	}catch(Exception e){
	    e.printStackTrace();
	}
    }
    
    public File getDatabaseFile(){
    	return new File(DB_PATH,DB_NAME);
    	
    	
    	
    }

    
    
}
