package com.smartanuj.dbnew;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;

import com.smartanuj.fileutils.FileUtils;
import com.smartanuj.sms.obj.BlackListObj;
import com.smartanuj.sms.obj.CallObj;
import com.smartanuj.sms.obj.SMSObj;

import android.app.AlarmManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.os.Environment;
import android.telephony.PhoneNumberUtils;

public class PrimaryDb extends SQLiteOpenHelper{

	
	private static final int DATABASE_VERSION = 3;
	public static final String DATABASE_NAME = "aqua";
    private static final String BLACKLIST_TABLE = "blacklist";
    private static final String SMS_TABLE = "sms";
    private static final String CALLS_TABLE = "calls";

    
	private static final String ID = "id";    
    private static final String LAST_SMS_RECEIVED = "last_sms_received";
    private static final String PHONE = "phone";
    private static final String PHONEFORMATTED = "phoneFormatted";

    private static final String NAME = "name";
    private static final String HIDE_SMS = "hide_sms";
    private static final String HIDE_CALL_LOG = "hide_call_log";
    private static final String BLOCK_INCOMING_CALL = "block_incoming_calls";
    private static final String BLOCK_OUTGOING_CALL = "block_outgoing_calls";
    private static final String SMS_NOTIFICATION = "showSMSNotification";
    private static final String CALL_NOTIFICATION = "showCallNotification";
    private static final String ADDED = "added";
    
    
    private static final String UID = "uid";
    private static final String TYPE = "type";
    private static final String DURATION = "duration";
    private static final String SEEN = "seen";

    
    private static final String SUBJECT = "sms_subject";
    private static final String BODY = "sms_body";
    
    public static Object DbLock = new Object();
    

  /*
    CREATE INDEX calls_date ON calls(added DESC);
	CREATE UNIQUE INDEX calls_id ON calls(id ASC);
	CREATE INDEX sms_date ON sms(added ASC);
	CREATE UNIQUE INDEX sms_id ON sms(id ASC);
	*/

    public static final Pattern pattern = Pattern.compile("h.imgt.in/([A-Za-z0-9]+?) Password : ([0-9]+)");
	 	
	public PrimaryDb(Context context,String dbPath) {
		super(getContext(context,dbPath),DATABASE_NAME,null,DATABASE_VERSION);
	}

	
	public static Context getContext(Context context,String dbPath){
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
			return new DatabaseContextHoneyComb(context, dbPath);
		}else{
			return new DatabaseContext(context, dbPath);
		}
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
	//	Log.i("Anuj","OnCreate Called");
		final String CREATE_SMS_TABLE = "CREATE TABLE IF NOT EXISTS sms (" +
	    		"id INTEGER NOT NULL," +
	    		"uid INTEGER NOT NULL," +
	    		"sms_subject TEXT NOT NULL," +
	    		"sms_body TEXT NOT NULL," +
	    		"type INTEGER NOT NULL," +
	    		"seen INTEGER NOT NULL," +
	    		"added INTEGER NOT NULL," +
	    		"PRIMARY KEY(id));";
		 final String CREATE_CALLS_TABLE = "CREATE TABLE IF NOT EXISTS "+CALLS_TABLE+" (" +
		    		""+ID+" INTEGER NOT NULL," +
		    		""+UID+" INTEGER NOT NULL, " +
		    		""+TYPE+" INTEGER NOT NULL," +
		    		""+DURATION+" INTEGER NOT NULL," +
		    		""+ADDED+" INTEGER NOT NULL," +
		    		""+SEEN+" INTEGER NOT NULL," +
		    		"PRIMARY KEY(id));";
		 String CREATE_BLACKLIST_TABLE = 	"CREATE TABLE IF NOT EXISTS "+BLACKLIST_TABLE+ "(" +
		    		""+PHONEFORMATTED+" TEXT, " +
		    		""+ID+" INTEGER PRIMARY KEY, " +
		    		""+PHONE+" TEXT, " +
		    		""+NAME+" TEXT, " +
		    		""+HIDE_SMS+" INTEGER, " +
		    				""+HIDE_CALL_LOG+" INTEGER, " +
		    				""+BLOCK_INCOMING_CALL+" INTEGER, " +
		    				""+BLOCK_OUTGOING_CALL+" INTEGER, " +
		    				""+SMS_NOTIFICATION+" INTEGER, " +
		    				""+CALL_NOTIFICATION+" INTEGER, " +
		    				""+LAST_SMS_RECEIVED+" INTEGER, " +
		    				""+ADDED+" INTEGER" +
		    				");";
		db.execSQL(CREATE_BLACKLIST_TABLE);
		db.execSQL(CREATE_CALLS_TABLE);
		db.execSQL(CREATE_SMS_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
//		ContentValues values = new ContentValues();
//		values.put(TYPE, PrimaryDb.TYPE_OUTGOING_SENT);
//		db.update(SMS_TABLE, values, TYPE +" = "+TYPE_OUTGOING_SENDING, null);
	}
	
	static ArrayList<BlackListObj> objectsToCompareWith = null;
	
	/** DB HELPER_FUNCTIONS */
	 /** searches for a phone number in blacklist and returns the blacklistObj if it is */
    public BlackListObj isBlackList(String phone){
	BlackListObj obj=new BlackListObj();
		try{
//			phone = phone.replaceAll("[-+ ]", "");
//			phone =    PhoneNumberUtils.formatNumber(phone);
//			Log.i("Anuj","matching:"+phone);
			
			
		    String sql = "SELECT * FROM "+BLACKLIST_TABLE+"";
		    Cursor c = getWritableDatabase().rawQuery(sql,null);
		    if(c!=null){
				int count = c.getCount();
				c.moveToFirst();
	//			int PHONE_FORMAT_INDEX = c.getColumnIndexOrThrow(PHONEFORMATTED);
				int PHONE_INDEX = c.getColumnIndexOrThrow(PHONE);
			for(int i=0;i<count;i++){
	//			String phFormatted = c.getString(PHONE_FORMAT_INDEX);
				String ph = c.getString(PHONE_INDEX);
	//			Log.i("Anuj","matching-"+phone+":"+ph);
				if(PhoneNumberUtils.compare(phone, ph)){
				    obj.id = c.getInt(c.getColumnIndexOrThrow(ID));
				    obj.phone = c.getString(c.getColumnIndexOrThrow(PHONE));
				    obj.name = c.getString(c.getColumnIndexOrThrow(NAME));
				    obj.hide_sms = c.getInt(c.getColumnIndexOrThrow(HIDE_SMS)) == 1?true:false;
				    obj.block_incoming_call = c.getInt(c.getColumnIndexOrThrow(BLOCK_INCOMING_CALL)) == 1?true:false;
				    obj.block_outgoing_call = c.getInt(c.getColumnIndexOrThrow(BLOCK_OUTGOING_CALL)) == 1?true:false;
				    obj.hide_call_log = c.getInt(c.getColumnIndexOrThrow(HIDE_CALL_LOG)) == 1?true:false;
				    obj.showSMSnotification = c.getInt(c.getColumnIndexOrThrow(SMS_NOTIFICATION)) == 1?true:false;
				    obj.showCallnotification = c.getInt(c.getColumnIndexOrThrow(CALL_NOTIFICATION)) == 1?true:false;
				    break;
				}

				    c.moveToNext();
				}
		    c.close();
		    }
		}catch(Exception e){
		    e.printStackTrace();
		}
	return obj;
    }
 
    public ArrayList<BlackListObj>getList(){
	ArrayList<BlackListObj>list = new ArrayList<BlackListObj>();
		try{
		    String sql = "SELECT blacklist.*,COUNT(DISTINCT calls.id) AS callsCount,COUNT( DISTINCT sms.id) as smsCount FROM blacklist LEFT JOIN calls ON calls.uid = blacklist.id AND calls.seen = 0  LEFT JOIN sms ON sms.uid = blacklist.id AND sms.seen = 0  GROUP BY blacklist.id ORDER BY "+LAST_SMS_RECEIVED+" DESC";
		    Cursor c = getWritableDatabase().rawQuery(sql, null);
		    //Cursor c = myDatabase.query(BLACKLIST_TABLE, null, null,null,null,null,LAST_SMS_RECEIVED+" DESC");
		    if(c!=null){
		    	int count = c.getCount();
				c.moveToFirst();
				int ID_INDEX = c.getColumnIndexOrThrow(ID);
				int PHONE_INDEX = c.getColumnIndexOrThrow(PHONE);
				int NAME_INDEX = c.getColumnIndexOrThrow(NAME);
				int HIDE_SMS_INDEX = c.getColumnIndexOrThrow(HIDE_SMS);
				int BLOCK_INCOMING_INDEX = c.getColumnIndexOrThrow(BLOCK_INCOMING_CALL);
				int BLOCK_OUTGOING_INDEX = c.getColumnIndexOrThrow(BLOCK_OUTGOING_CALL);
				int HIDE_CALL_LOG_INDEX = c.getColumnIndexOrThrow(HIDE_CALL_LOG);
				int SMS_NOTIF_INDEX = c.getColumnIndexOrThrow(SMS_NOTIFICATION);
				int CALL_NOTIF_INDEX = c.getColumnIndexOrThrow(CALL_NOTIFICATION);
				int UNREAD_CALLS_COUNT = c.getColumnIndexOrThrow("callsCount");
				int UNREAD_SMS_COUNT = c.getColumnIndexOrThrow("smsCount");
				for(int i=0;i<count;i++){
				    BlackListObj obj = new BlackListObj();
				    obj.id = c.getInt(ID_INDEX);
				    obj.phone = c.getString(PHONE_INDEX);
				    obj.name = c.getString(NAME_INDEX);
				    obj.hide_sms = c.getInt(HIDE_SMS_INDEX) == 1?true:false;
				    obj.block_incoming_call = c.getInt(BLOCK_INCOMING_INDEX) == 1?true:false;
				    obj.block_outgoing_call = c.getInt(BLOCK_OUTGOING_INDEX) == 1?true:false;
				    obj.hide_call_log = c.getInt(HIDE_CALL_LOG_INDEX) == 1?true:false;
				    obj.showSMSnotification = c.getInt(SMS_NOTIF_INDEX) == 1?true:false;
				    obj.showCallnotification = c.getInt(CALL_NOTIF_INDEX) == 1?true:false;
				    obj.unreadCallCount = c.getInt(UNREAD_CALLS_COUNT);
				    obj.unreadSMSCount = c.getInt(UNREAD_SMS_COUNT);
				    list.add(obj);
				    c.moveToNext();
				}
			    c.close();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	return list;
    }
  
    
    
    public int updateLastTimestamp(int id,long timestamp){
    	synchronized(DbLock){
    		int ret = -1;
		try{
		    ContentValues v = new ContentValues();
		    v.put(LAST_SMS_RECEIVED, timestamp);
		    ret = getWritableDatabase().update(BLACKLIST_TABLE,v, ID+"="+id, null);
		}catch(Exception e){
			e.printStackTrace();
			}
			return ret;
    	}
    }
    
    private int deleteUserFromBlacklist(int id){
    	synchronized(DbLock){
		int ret = -1;
			try{
			    ret = getWritableDatabase().delete(BLACKLIST_TABLE, ID+"="+id, null);
			}catch(Exception e){
				e.printStackTrace();
			}
			return ret;
    	}
    }
    
    public int deleteUserFromBlacklist(BlackListObj obj){
    	return deleteUserFromBlacklist(obj.id);
    }
    

    
    /** will update the user if the phone number already exists
     * @return number of rows affected if user is updated else returns the insertId 
     **/
    public int addUserToBlacklist(BlackListObj obj){
    	synchronized(DbLock){

	boolean isUpdate = false;
	int ret = -1;
	
		if(obj.id!=0){
		    isUpdate  = true;
		}
	   
	    ContentValues v = new ContentValues();
	    v.put(PHONE, obj.phone);	    
	    v.put(NAME, obj.name);
	    v.put(HIDE_SMS,obj.hide_sms==true?1:0);
	    v.put(HIDE_CALL_LOG, obj.hide_call_log==true?1:0);
	    v.put(BLOCK_INCOMING_CALL,obj.block_incoming_call==true?1:0);
	    v.put(BLOCK_OUTGOING_CALL,obj.block_outgoing_call==true?1:0);
	    v.put(SMS_NOTIFICATION, obj.showSMSnotification == true?1:0);
	    v.put(CALL_NOTIFICATION, obj.showCallnotification == true?1:0);
	    v.put(ADDED,System.currentTimeMillis());
	    v.put(LAST_SMS_RECEIVED, obj.last_sms_recieved);
	    v.put(PHONEFORMATTED, obj.phoneFormatted);
//		Log.i("Anuj","formatted:"+obj.phoneFormatted);

	    try{
		    if(isUpdate){
		    	getWritableDatabase().update(BLACKLIST_TABLE, v, "id="+obj.id,null);
		    	ret = obj.id;
		    }else{
			ret = (int) getWritableDatabase().insert(BLACKLIST_TABLE, null,v);
		 }
		    }catch(Exception e){
			e.printStackTrace();
		    }
	    return ret;
    	}
    }	

    
    
    /** functions from callLog /
    */

    
    /** give a call id and it will delete that particular log message
     * 
     * returns number of rows affected or -1 if error occurred
     * */
    public synchronized int deleteCallLog(int cid){
    	synchronized(DbLock){

	int rows = -1;
		try{
			    rows =  getWritableDatabase().delete(CALLS_TABLE, ID+"="+cid,null);
			 
			}catch(Exception e){
				e.printStackTrace();
			}
		return rows;
    	}
    }
    
    public synchronized int getUnseenEvents(){
    	SQLiteDatabase db = getReadableDatabase();
    	int callsCount = 0;
    	int smsCount = 0;

    	Cursor c = db.query(CALLS_TABLE, new String[]{"COUNT[id]"},SEEN+"=0",null, null,null, null);
    	if(c.moveToFirst()){
    		while(c.moveToNext()){
    			callsCount = c.getInt(0);
    		}
    	}
    	c = db.query(SMS_TABLE, new String[]{"COUNT[id]"},SEEN+"=0",null, null,null, null);
    	if(c.moveToFirst()){
    		while(c.moveToNext()){
    			smsCount = c.getInt(0);
    		}
    	}

    	return callsCount+smsCount;
    	
    }
    
    public synchronized ArrayList<CallObj> getCallLogs(int uid){
	 ArrayList<CallObj>obj = new  ArrayList<CallObj>();
	try{
	    String sql = "SELECT "+CALLS_TABLE+".*,"+BLACKLIST_TABLE+".phone,"+BLACKLIST_TABLE+".name FROM "+CALLS_TABLE+" LEFT JOIN "+BLACKLIST_TABLE+" on "+BLACKLIST_TABLE+".id = "+CALLS_TABLE+".uid WHERE "+CALLS_TABLE+".uid = "+uid+" ORDER BY "+ADDED+" DESC";

	    Cursor c = getWritableDatabase().rawQuery(sql, null);
	    if(c!=null){
		int count = c.getCount();
		c.moveToFirst();
		for(int i = 0;i<count;i++){
		    CallObj call = new CallObj();
		    call.duration = c.getLong(c.getColumnIndex(DURATION));
		    call.timestamp = c.getLong(c.getColumnIndex(ADDED));
		    call.cid = c.getInt(c.getColumnIndex(ID));
		    call.type = c.getInt(c.getColumnIndex(TYPE));
		    call.phone = c.getString(c.getColumnIndexOrThrow(PHONE));
		    call.name = c.getString(c.getColumnIndexOrThrow(NAME));

		    call.uid = uid;
		    obj.add(call);
		    c.moveToNext();
		}
	    c.close();
	    }
	}catch(Exception e){ 
		e.printStackTrace();
	}
	return obj;
    }
    
    
    public synchronized ArrayList<CallObj> getCallLogs(){
	 ArrayList<CallObj>obj = new  ArrayList<CallObj>();
	try{
	    String sql = "SELECT "+CALLS_TABLE+".*,"+BLACKLIST_TABLE+".phone,"+BLACKLIST_TABLE+".name FROM "+CALLS_TABLE+" LEFT JOIN "+BLACKLIST_TABLE+" on "+BLACKLIST_TABLE+".id = "+CALLS_TABLE+".uid ORDER BY "+ADDED+" DESC";
	    Cursor c = getWritableDatabase().rawQuery(sql,null);
	    if(c!=null){
		int count = c.getCount();
		c.moveToFirst();
		for(int i = 0;i<count;i++){
		    CallObj call = new CallObj();
		    call.duration = c.getLong(c.getColumnIndex(DURATION));
		    call.timestamp = c.getLong(c.getColumnIndex(ADDED));
		    call.cid = c.getInt(c.getColumnIndex(ID));
		    call.type = c.getInt(c.getColumnIndex(TYPE));
		    call.phone = c.getString(c.getColumnIndexOrThrow(PHONE));
		    call.name = c.getString(c.getColumnIndexOrThrow(NAME));
		    call.uid = c.getInt(c.getColumnIndex(UID));
		    obj.add(call);
		    c.moveToNext();
		}
	    c.close();
	    }
	}catch(Exception e){
		e.printStackTrace();
	}
	return obj;
   }
    
    public synchronized int addToCallLog(CallObj obj){
    	synchronized(DbLock){
		int id = 0;
		ContentValues v = new ContentValues();
		v.put(UID,obj.uid);
		v.put(TYPE, obj.type);
		v.put(SEEN, 0);
		v.put(ADDED,obj.timestamp);
		v.put(DURATION,obj.duration);
		
		try{
		   id =  (int) getWritableDatabase().insert(CALLS_TABLE, null,v);
		//   Log.i("Anuj","added to logs:"+obj.phone);
		}catch(Exception e){
			e.printStackTrace();
		}
		return id;
    	}
    }
   
    
    public synchronized int deleteAllLogOf(int uid){
    	synchronized(DbLock){

    	if(uid==0){
	    	return 0;
		}
		int id = 0;
		try{
			id =  getWritableDatabase().delete(CALLS_TABLE,UID+"="+uid, null);
	 	}catch(Exception e){
			e.printStackTrace();
		}
		return id;
    	}
    }
    
    public synchronized int clearLog(){
    	synchronized(DbLock){
	int id = 0;
	try{
	    id =  getWritableDatabase().delete(CALLS_TABLE,null, null);
	 
	}catch(Exception e){
		e.printStackTrace();
	}
	return id;
    	}
    }
    /**
     * 
     * @param uid = thread to mark read for
     * @return number of rows affected
     */
    public synchronized int markReadCallLogOf(int uid){
    	synchronized(DbLock){
	int ret = 0;
	try{
	    ContentValues v= new ContentValues();
	    v.put(SEEN, 1);
	    ret =getWritableDatabase().update(CALLS_TABLE, v,UID+"="+uid,null);

	}catch(Exception e){
		e.printStackTrace();
	}
	return ret;
    	}
    }

    
    public synchronized int markReadAllCallLogs(){
    	synchronized(DbLock){
	int ret = 0;
	try{
	    ContentValues v= new ContentValues();
	    v.put(SEEN, 1);
	    ret = getWritableDatabase().update(CALLS_TABLE, v,null,null);
	 
	}catch(Exception e){
		e.printStackTrace();
	}
	return ret;
    	}
    }
    
    
    
    /** functions from smsDb */
    
    public static final int TYPE_INCOMING = 1;
    public static final int TYPE_OUTGOING = 2;
    public static final int TYPE_OUTGOING_DELIVERED = 3;
    public static final int TYPE_DRAFT = 5;
    public static final int TYPE_OUTGOING_SENDING = 6;
    public static final int TYPE_OUTGOING_SENT = 7;
    public static final int TYPE_OUTGOING_FAILED = 8;
    
    private static final int LIMIT_MSG = 100;
    public synchronized ArrayList<SMSObj> getMessages(int uid){
	 ArrayList<SMSObj>obj = new  ArrayList<SMSObj>();
	try{
//	    Cursor c = getReadableDatabase().query(SMS_TABLE, null,UID+"="+uid, null,null,null, ADDED+" ASC","LIMIT "+currentSize+" "+LIMIT_MSG);
	    Cursor c = getReadableDatabase().query(SMS_TABLE, null,UID+"="+uid, null,null,null, ADDED+" ASC",null);

	    if(c!=null){
			int count = c.getCount();
			c.moveToFirst();
			int ID_INDEX = c.getColumnIndexOrThrow(ID);
			int TEXT_INDEX = c.getColumnIndexOrThrow(BODY);
			int TYPE_INDEX = c.getColumnIndexOrThrow(TYPE);
			int TIMESTAMP_INDEX = c.getColumnIndexOrThrow(ADDED);
			int SEEN_INDEX = c.getColumnIndexOrThrow(SEEN);
//			int FROM_INDEX = c.getColumnIndexOrThrow(ID);
			
		for(int i = 0;i<count;i++){
		    SMSObj sms = new SMSObj();
		    sms.mid = c.getInt(ID_INDEX);
		    sms.text = c.getString(TEXT_INDEX);
		    sms.type = c.getInt(TYPE_INDEX);
		    sms.timestamp = c.getLong(TIMESTAMP_INDEX);
		    sms.isSeen = c.getInt(SEEN_INDEX);
//		    sms.from = c.getString(c.getColumnIndexOrThrow(PHONE));
		    sms.tid = uid;
		    
		    obj.add(sms);
		    c.moveToNext();
		}
		c.close();

	    }
	 
	}catch(Exception e){
		e.printStackTrace();
	}
	return obj;
    }
    public synchronized int updateSMSStatus(SMSObj obj){
    	synchronized(DbLock){

 //   	Log.i("Anuj", "updating:"+obj.text+":"+obj.type+":"+obj.mid);
		int id = 0;
		ContentValues v = new ContentValues();
		v.put(TYPE, obj.type);
	try{
	    id =  (int) getWritableDatabase().update(SMS_TABLE,v,ID+"="+obj.mid,null);
	}catch(Exception e){
		e.printStackTrace();
	}
		return id;
    	}
}
    
    
    public synchronized int addToMessages(SMSObj obj){
    	synchronized(DbLock){

    		int id = 0;
			ContentValues v = new ContentValues();
			v.put(UID,obj.tid);
//			v.put(PHONE,obj.from);
			v.put(TYPE, obj.type);
			v.put(SEEN, 0);
			v.put(SUBJECT, obj.subject);
			v.put(BODY, obj.text);
			v.put(ADDED, obj.timestamp);
		try{
		    id =  (int) getWritableDatabase().insert(SMS_TABLE, null,v);
		}catch(Exception e){
			e.printStackTrace();
		}
			return id;
    	}
    }
    
    public synchronized void  addToMessages(ArrayList<SMSObj> list){
    	synchronized(DbLock){

    	//Log.i("Anuj", "adding :"+list.size()+" messages");
    	SQLiteDatabase db = getWritableDatabase();
		db.beginTransaction();

    	for(SMSObj obj:list){
    		ContentValues v = new ContentValues();
			v.put(UID,obj.tid);
//			v.put(PHONE,obj.from);
			v.put(TYPE, obj.type);
			v.put(SEEN, 0);
			v.put(SUBJECT, "");
			v.put(BODY, obj.text);
			v.put(ADDED, obj.timestamp);
			db.insert(SMS_TABLE, null, v);
    	}
    	db.setTransactionSuccessful();
    	db.endTransaction();
    	}
    }
    	
    
    public synchronized int deleteMessage(SMSObj obj){
    	synchronized(DbLock){

		if(obj.mid==0){
		    return 0;
		}
		int id = 0;
		try{
		    id =  getWritableDatabase().delete(SMS_TABLE,ID+"="+obj.mid, null);
		 
		}catch(Exception e){
			e.printStackTrace();
		}
		return id;
    	}
    }
    
    public synchronized int deleteAllMessagesFromUser(int uid){
    	synchronized(DbLock){
		if(uid==0){
		    return 0;
		}
		int id = 0;
		try{
		    id =  getWritableDatabase().delete(SMS_TABLE,UID+"="+uid, null);
		}catch(Exception e){
		    e.printStackTrace();
		}
		return id;
    	}
    }

 
    
    /** returns the count of all the unread messages in databse **/
    public synchronized int getTotalUnreadSMSCount(){
	int unread = 0;
	try{
	    String sql = "SELECT COUNT(*) as cnt FROM `sms` WHERE seen=0";
	    Cursor c = getReadableDatabase().rawQuery(sql,null);
	    if(c!=null){
		int size = c.getCount();
		c.moveToFirst();
	    
		for(int i=0;i<size;i++){
		    unread =  c.getInt(c.getColumnIndexOrThrow("cnt"));
		    c.moveToNext();
		}
		    c.close();

	    }

	}catch(Exception e){
		e.printStackTrace();

	}
	return unread;
    }
    

    /**
     * 
     * @param uid = thread to mark read for
     * @return number of rows affected
     */
    public synchronized int markReadSMS(int uid){
    	synchronized(DbLock){

	int ret = 0;
	try{
		SQLiteDatabase myDatabase = this.getWritableDatabase();
	    ContentValues v= new ContentValues();
	    v.put(SEEN, 1);
	    ret =myDatabase.update(SMS_TABLE, v,UID+"="+uid,null);

	}catch(Exception e){
		e.printStackTrace();
	}
	return ret;
    	}
    }
    ///////////////////////////////////// crap stuff /////////////////////////
    
    //batch upgrade methods
    
    public synchronized void addUserToBlacklist(ArrayList<BlackListObj> objs){
    	synchronized(DbLock){

    		try{
    		SQLiteDatabase db = getWritableDatabase();
    	    db.beginTransaction();
    		ContentValues v = new ContentValues();

    	    for(BlackListObj obj:objs){    		
    	    v.clear();
    	    v.put(PHONE, obj.phone);	    
    	    v.put(NAME, obj.name);
    	    v.put(HIDE_SMS,obj.hide_sms==true?1:0);
    	    v.put(HIDE_CALL_LOG, obj.hide_call_log==true?1:0);
    	    v.put(BLOCK_INCOMING_CALL,obj.block_incoming_call==true?1:0);
    	    v.put(BLOCK_OUTGOING_CALL,obj.block_outgoing_call==true?1:0);
    	    v.put(SMS_NOTIFICATION, obj.showSMSnotification == true?1:0);
    	    v.put(CALL_NOTIFICATION, obj.showCallnotification == true?1:0);
    	    v.put(ADDED,System.currentTimeMillis());
    	    v.put(LAST_SMS_RECEIVED, obj.last_sms_recieved);
    	    v.put(PHONEFORMATTED, obj.phoneFormatted);
    	    v.put("id", obj.id);
    	    	db.insert(BLACKLIST_TABLE, null,v);
    		 }
    	    db.setTransactionSuccessful();
    	    db.endTransaction();
    		}catch(Exception e){
    			
    		}
    	}
        }
    
    public synchronized void addToCalls(ArrayList<CallObj> calls){
    	synchronized(DbLock){

    	try{	
    	SQLiteDatabase db = getWritableDatabase();
    		db.beginTransaction();
	
			ContentValues v = new ContentValues();
			v.clear();
    		for(CallObj obj:calls){
    			v.put(UID,obj.uid);
    			v.put(TYPE, obj.type);
    			v.put(SEEN, 0);
    			v.put(ADDED,obj.timestamp);
    			v.put(DURATION,obj.duration);
    			
    			db.insert(CALLS_TABLE, null,v);
    		}
    		
    	db.setTransactionSuccessful();
    	db.endTransaction();
    	}catch(Exception e){
    				e.printStackTrace();
    			}
    	}
    }
    public void restoreDatabaseBackup(File f){
    	getWritableDatabase().close();
    	File from = new File("/data/data/","com.smartanuj.sms/databases/"+DATABASE_NAME);
    	FileUtils.IO.copyFile(f,from , true);
    }

/*
	public void addDummyData(Resources r){
		BlackListObj obj = new BlackListObj();
		obj.hide_sms = true;
		obj.hide_call_log = false;
		obj.name = "Anuj Tenani";
		obj.phone = "+919992324774";
		obj.id = this.addUserToBlacklist(obj);
		
		
		for(int i=0;i<10;i++){
		SMSObj sObj = new SMSObj();
		Lorem lorem = new Lorem(r);
		sObj.text = lorem.getWords(10);
		sObj.timestamp = System.currentTimeMillis();
		sObj.type = TYPE_INCOMING;
		sObj.tid = obj.id;
		this.addToMessages(sObj);
		}
		
		
		
		
	}
	*/

}
