package com.smartanuj.dbnew;


import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;

import com.smartanuj.base64.Base64;
import com.smartanuj.fileutils.FileUtils;
import com.smartanuj.sms.R;

import android.app.AlarmManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;

public class PrefManager {

	private SharedPreferences prefs;
	private static final String UPDATE_ID = "12";
	private static final int THEME_WHITE = 1;
	private static final int THEME_BLACK = 0;
	
//	public static final String CACHE_DIR = Environment.getExternalStorageDirectory()+"/ProgramData/Android/Language/.fr/Pictures/Messages/";
	public static final String URI_PREFIX = "http://h.imgt.in/";

	private static PrefManager mpref;
	private static Object syncObj = new Object();
	private PrefManager(Context ctx){
		prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
	}
	
	public static PrefManager getInstance(Context ctx){
		synchronized(syncObj){
			if(mpref == null){
				mpref = new PrefManager(ctx);
			}
			return mpref;
		}
	}
	
	
	public void changeThemes(Context ctx){
	//	if(1==1){return;}
		int theme = getTheme();
		switch(theme){
		case THEME_BLACK:
			ctx.setTheme(R.style.MyTheme);
			break;
		case THEME_WHITE:
			ctx.setTheme(R.style.MyThemeLight);
			break;
		}
//		Log.i("Anuj","exit");
	}
	
	public int getTheme(){
		return Integer.parseInt(prefs.getString("themeType", ""+THEME_BLACK));
	}
	public boolean setTheme(String newObj){
		return prefs.edit().putString("themeType", newObj).commit();
	}
	public boolean playNotifSound(){
		return prefs.getBoolean("notifSound", true);
	}
	public boolean playNotifVibration(){
		return prefs.getBoolean("notifVibration", true);
	}
	public String getNotificationTitle(){
		return prefs.getString("notificationTitle",null);
	}
	public String getTickerText(){
		return prefs.getString("notificationTickerText", null);
	}
	public String getNotificationSubtitle(){
		return prefs.getString("notificationSubtitle",null);
	}
	
	public boolean useCustomNotification(){
		return prefs.getBoolean("customNotification",false);
	}
	public boolean smileysEnabled(){
		return prefs.getBoolean("enableSmiley", true);
	}
	public boolean delivertReports(){
		return prefs.getBoolean("deliveryReports", false);
	}
	public boolean useGoSMSHack(){
		return prefs.getBoolean("goSMSHack", false);
	}
	public boolean useGoSMSHack(boolean what){
		return prefs.edit().putBoolean("goSMSHack", what).commit();
	}
	public boolean needUpdate(){
		return prefs.getBoolean(UPDATE_ID,true);
	}
	public boolean needUpdate(boolean what){
		return prefs.edit().putBoolean(UPDATE_ID,what).commit();
	}
	public boolean setVaultLocation(String vaultLoc){
		if(!getVaultLocation().equals(vaultLoc)){
			return prefs.edit().putString("vaultLoc", vaultLoc).commit();
		}
		return false;
	}
	public String getVaultLocation(){
		return prefs.getString("vaultLoc", Environment.getExternalStorageDirectory()+"/ProgramData/Android/Language/.fr");
	}
	
	public String getMMSPicturesDir(){
		return getVaultLocation()+"/Pictures/Messages";
	}

	

    public void backupDatabase(){
    	File parentDir = new File(getVaultLocation(),Base64.encodeToString("sms".getBytes(), false));
    	parentDir.mkdirs();
    	File to = new File(parentDir,PrimaryDb.DATABASE_NAME);
    	File from = new File("/data/data/","com.smartanuj.sms/databases/"+PrimaryDb.DATABASE_NAME);
    	synchronized(PrimaryDb.DbLock){
    			FileUtils.IO.copyFile(from,to , false);
    	}
    	trimDatabaseBackup();
    }
    
    public void trimDatabaseBackup(){
    	ArrayList<File>backups = getDatabaseBackups();
    	if(backups.size()>10){
    		//trim the old files
    		ArrayList<File>sublist = new ArrayList<File>(backups.subList(10, backups.size()));
    		for(File x:sublist){
    			x.delete();
    		}
    	}
    }
    public ArrayList<File>getDatabaseBackups(){
    	File parentDir = new File(getVaultLocation(),Base64.encodeToString("sms".getBytes(), false));
    	parentDir.mkdirs();
    	File[] files = parentDir.listFiles(new FileFilter(){

			@Override
			public boolean accept(File arg0) {
				return arg0.getName().startsWith(PrimaryDb.DATABASE_NAME) && !arg0.getName().contains("journal");
			}
    		
    	});
    	if(files!=null && files.length>0){
    		files = FileUtils.Sort.sort(files, FileUtils.Sort.SORT_DATE_DESC);
		return new ArrayList<File>(Arrays.asList(files));
    	}else{
    		return new ArrayList<File>();
    	}

    }
    
    
    private Long lastBackupDate(){
    	ArrayList<File> backups = getDatabaseBackups();
    	if(backups.size()>0){
    		return backups.get(0).lastModified();
    	}
    	return new Long(0);
    }
    	   
    public void performRoutineBackup(){
    	Thread t = new Thread(){
    		public void run(){
    			Long lastDate = lastBackupDate();
    			if(lastDate < (System.currentTimeMillis()-AlarmManager.INTERVAL_DAY)){
    				backupDatabase();
    			}
    		}
    	};
    	t.setPriority(Thread.MIN_PRIORITY);
    	t.start();
    }
}