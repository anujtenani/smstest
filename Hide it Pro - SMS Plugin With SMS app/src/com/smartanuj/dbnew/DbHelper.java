package com.smartanuj.dbnew;

import com.smartanuj.sms.util.FroyoHelper;

import android.content.Context;
import android.os.Build;

public class DbHelper {
	
	private static PrimaryDb primaryDb;
	private static String path;
	public static PrimaryDb getPrimaryDb(Context ctx) {
		String dbPath = PrefManager.getInstance(ctx).getVaultLocation();
		if(primaryDb == null || !path.equals(dbPath)){
			path = dbPath;
			primaryDb = new PrimaryDb(ctx,dbPath);
			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO){
				FroyoHelper.doBackup(ctx);
			}
		}
		return primaryDb;
	}
	
	public static void closePrimaryDb(){
		if(primaryDb != null){
			primaryDb.close();
		}
	}
	
	@Override
	public void finalize(){
		if(primaryDb != null){
			primaryDb.close();
		}
	}
	
}
