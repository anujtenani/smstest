package com.contactpicker.compatibility;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

public class MyContactPicker extends Activity{

    boolean newApi = true;
    private static final int  CONTACT_PICKER = 11;

    private ContactPickerBase cpicker;
    @Override
    public void onCreate(Bundle icicle){
	super.onCreate(icicle);
    
	if(Build.VERSION.SDK_INT > Build.VERSION_CODES.DONUT){
	    //use the new api
	    newApi = true;
	}else{
	    //use the old api
	    newApi = false;
	}
	try{
	if(newApi){
		cpicker = new ContactPickerNew();
		Intent intentContact = cpicker.getPickerIntent();
//	    Intent intentContact = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI); 
	    startActivityForResult(intentContact, CONTACT_PICKER);
	}else{
		cpicker = new ContactPickerOld();
//	    Intent intentContact = new Intent(Intent.ACTION_PICK, People.CONTENT_URI); 
//	    intentContact.setType(People.CONTENT_TYPE);
		Intent intentContact = cpicker.getPickerIntent();
		startActivityForResult(intentContact, CONTACT_PICKER);
	}
	}catch(Exception e){
		finish();
	}
    
    }
    @Override
    public void onActivityResult(int requestCode,int resultCode,Intent data){
	if(resultCode==Activity.RESULT_OK){
	    
	   Uri uri =  data.getData();
	   if(uri!=null){
/*	       if(newApi){
		   setContactNew(uri);
	       }else{
		   setContactOld(uri);
	       }
 */
		   MContact cont = cpicker.getContact(this, uri);
		   setResultAndFinish(cont);
	   }
	
	}else{
	    setResult(Activity.RESULT_CANCELED);
	    finish(); 
	}
    }
    
    private void setResultAndFinish(MContact cont){
	
	Intent i = new Intent();
	i.putStringArrayListExtra("phone",cont.phones);
	i.putExtra("name",cont.name);
//	i.putExtra("pic", cont.image);
	setResult(Activity.RESULT_OK,i);
	finish();
    }
    
  /*  private void setContactNew(Uri uri)    {
	ArrayList<String> phoneNumbers = new ArrayList<String>();
	String name = null;
	Cursor cursor =  managedQuery(uri, null, null, null, null);      
       while (cursor.moveToNext()) 
       {           
           String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
           name = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME)); 
           Log.i("Anuj",name);
        String hasPhone = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
           if (hasPhone.equalsIgnoreCase("1")) {
            Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ contactId,null, null);
            while (phones.moveToNext()) 
            {
             String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
             phoneNumbers.add(phoneNumber);
             Log.i("Anuj",phoneNumber);
            }
            phones.close();
           }
       }
       setResultAndFinish(phoneNumbers,name);
    }
    */
/*    private void setContactOld(Uri uri){
	ArrayList<String> phoneNumbers = new ArrayList<String>();
	String name = null;
	
	Cursor c =  managedQuery(uri, null, null, null, null);
        if (c.moveToFirst()) {
          name = c.getString(c.getColumnIndexOrThrow(People.NAME));
          String ph = c.getString(c.getColumnIndexOrThrow(People.NUMBER_KEY));
          phoneNumbers.add(ph);
          Log.i("Anuj",name+":"+ph);
        }
        c.close();
        setResultAndFinish(phoneNumbers,name);
 
    }
  */  
}
