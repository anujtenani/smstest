package com.contactpicker.compatibility;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;

public abstract class ContactPickerBase {

	
	public abstract Intent getPickerIntent();		
	

	public abstract MContact getContact(Activity ctx, Uri uri);
	
	public abstract  Long getPersonId(ContentResolver cr,String phone);
		
	
	
	
}
