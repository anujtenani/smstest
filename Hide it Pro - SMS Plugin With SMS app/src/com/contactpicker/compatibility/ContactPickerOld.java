package com.contactpicker.compatibility;

import java.util.ArrayList;

import com.smartanuj.sms.R;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.Contacts.People;
import android.provider.Contacts.PeopleColumns;
import android.provider.Contacts.PhonesColumns;

public class ContactPickerOld extends ContactPickerBase {

	
	@Override
	public Intent getPickerIntent(){
		  	Intent intentContact = new Intent(Intent.ACTION_PICK, People.CONTENT_URI); 
		    intentContact.setType(People.CONTENT_TYPE);
		    return intentContact;
	}
	
	@Override
	public MContact getContact(Activity ctx,Uri uri)    {

			ArrayList<String> phoneNumbers = new ArrayList<String>();
			String name = null;
			
			Cursor c =  ctx.managedQuery(uri, null, null, null, null);
		        if (c.moveToFirst()) {
		          name = c.getString(c.getColumnIndexOrThrow(PeopleColumns.NAME));
		          String ph = c.getString(c.getColumnIndexOrThrow(PhonesColumns.NUMBER));
		          phoneNumbers.add(ph);
		        }
		        c.close();	

		        Bitmap bit = People.loadContactPhoto(ctx, uri, R.drawable.emo_im_cool,null);
		        
		        
		        		
		      MContact cont = new MContact();
		      cont.name = name;
		      cont.phones = phoneNumbers;
	//	      cont.image = bit;

		      return cont;
	}

	@Override
	public Long getPersonId(ContentResolver cr, String phone) {
		Uri curi = Uri.withAppendedPath(People.CONTENT_FILTER_URI, Uri.encode(phone));
		Long personId = null;
		Cursor c = cr.query(curi, new String[]{People._ID},null ,null, null);
		if(c!=null&&c.getCount()>0){
			c.moveToFirst();
			personId = c.getLong(c.getColumnIndexOrThrow(People._ID));
		c.close();
		}
		
		
		
		return personId;
	}
}
