package com.contactpicker.compatibility;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;

public class ContactPickerNew extends ContactPickerBase{

	
	@Override
	public Intent getPickerIntent(){
		  Intent intentContact = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI); 
		  return intentContact;
	}
	
	@Override
	public MContact getContact(Activity ctx,Uri uri)    {
			ArrayList<String> phoneNumbers = new ArrayList<String>();
			String name = null;
		//	Bitmap bMap = null;
			Cursor cursor =  ctx.managedQuery(uri, null, null, null, null);      
		       while (cursor.moveToNext()) 
		       {           
		           String contactId = cursor.getString(cursor.getColumnIndex(BaseColumns._ID));
		           name = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME)); 
		  //         Log.i("Anuj",name);
		           
		              
		        String hasPhone = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
		           if (hasPhone.equalsIgnoreCase("1")) {
		            Cursor phones = ctx.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ contactId,null, null);
		            while (phones.moveToNext()) 
		            {
		             String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
		             phoneNumbers.add(phoneNumber);
		   //          Log.i("Anuj",phoneNumber);
		            }
		            phones.close();
		           }
		      
		       }
		       
	/*	       
		      InputStream is = openPhoto(ctx,uri);
		      if(is!=null){
		    			  bMap = BitmapFactory.decodeStream(is);
		      }
		      if(bMap==null){
		    	  Log.i("Anuj","bmap is null");
		      }
*/		      
		      MContact cont = new MContact();
		      cont.name = name;
		      cont.phones = phoneNumbers;
//		      cont.image = bMap;
		      return cont;
	}
	
	 private InputStream openPhoto(Activity ctx,Uri contactUri) {
//	     Uri contactUri = ContentUris.withAppendedId(Contacts.CONTENT_URI, contactId);
	     Uri photoUri = Uri.withAppendedPath(contactUri, Contacts.Photo.CONTENT_DIRECTORY);
	     Cursor cursor = ctx.getContentResolver().query(photoUri,
	          new String[] {Contacts.Photo.PHOTO}, null, null, null);
	     if (cursor == null) {
	         return null;
	     }
	     try {
	         if (cursor.moveToFirst()) {
	             byte[] data = cursor.getBlob(0);
	             if (data != null) {
	                 return new ByteArrayInputStream(data);
	             }
	         }
	     } finally {
	         cursor.close();
	     }
	     return null;
	 }

	@Override
	public Long getPersonId(ContentResolver cr,String phone) {
		Long personId = null;
		Uri curi = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phone));
		String[] projection = new String[] {ContactsContract.PhoneLookup._ID};
		Cursor cur = cr.query(curi, projection, null, null, null);
		if(cur!=null) {
			if(cur.getCount()>0){
				cur.moveToFirst();
				personId = cur.getLong(cur.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
			}
		  }
		  cur.close();
		return personId;
	}
		    
}
