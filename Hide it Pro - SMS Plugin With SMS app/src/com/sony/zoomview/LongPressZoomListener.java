package com.sony.zoomview;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;


public class LongPressZoomListener implements View.OnTouchListener , OnGestureListener , OnDoubleTapListener{

	    /**
	     * Enum defining listener modes. Before the view is touched the listener is
	     * in the UNDEFINED mode. Once touch starts it can enter either one of the
	     * other two modes: If the user scrolls over the view the listener will
	     * enter PAN mode, if the user lets his finger rest and makes a longpress
	     * the listener will enter ZOOM mode.
	     */
	    enum Mode {
	        UNDEFINED, PAN, ZOOM
	    }

	    /** Time of tactile feedback vibration when entering zoom mode */
//	    private static final long VIBRATE_TIME = 50;

	    /** Current listener mode */
	    private Mode mMode = Mode.UNDEFINED;

	    /** Zoom control to manipulate */
	    private DynamicZoomControl mZoomControl;

	    /** X-coordinate of previously handled touch event */
	    private float mX;

	    /** Y-coordinate of previously handled touch event */
	    private float mY;

	    /** X-coordinate of latest down event */
	    private float mDownX;

	    /** Y-coordinate of latest down event */
	    private float mDownY;

	    /** Velocity tracker for touch events */
	    private VelocityTracker mVelocityTracker;

	    /** Distance touch can wander before we think it's scrolling */
	    private final int mScaledTouchSlop;

	    /** Duration in ms before a press turns into a long press */
	    private final int mLongPressTimeout;


	    /** Maximum velocity for fling */
	    private final int mScaledMaximumFlingVelocity;

	    /** set bitmap full zoomfactor */
	    
	    /**
	     * Creates a new instance
	     * 
	     * @param context Application context
	     */
	    private GestureDetector gestures;
	    private final boolean supportsPinchZoom;
	    private MyPinchZoom pz;
	    Boolean isPZing;
	    Float oldDist;
	    
	    public LongPressZoomListener(Context context) {
	        mLongPressTimeout = ViewConfiguration.getLongPressTimeout();
	        mScaledTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
	        mScaledMaximumFlingVelocity = ViewConfiguration.get(context)        .getScaledMaximumFlingVelocity();
	        gestures = new GestureDetector(context, this);
	        supportsPinchZoom = true;
	        isPZing = false;
            if(supportsPinchZoom){
            	pz = new MyPinchZoom();
            }	else{
            	}
	    }

	    /**
	     * Sets the zoom control to manipulate
	     * 
	     * @param control Zoom control
	     */
	    public void setZoomControl(DynamicZoomControl control) {
	        mZoomControl = control;
	
	    }

	    /**
	     * Runnable that enters zoom mode
	     */
	    private final Runnable mLongPressRunnable = new Runnable() {
	        public void run() {
	            mMode = Mode.ZOOM;
	        }
	    };
	    
	    public interface gestureListener{
	    	public void onFlickForward();
			public void onFlickBackward();
			public void onToggle();
	    }
	    
	    gestureListener gesture;
	    public void setOnGestureListener(gestureListener listener){
		gesture = listener;
	    }

	    // implements View.OnTouchListener
	    public boolean onTouch(View v, MotionEvent event) {

		gestures.onTouchEvent(event);
	    	
	        final int action = event.getAction();
	        final float x = event.getX();
	        final float y = event.getY();

	        if (mVelocityTracker == null) {
	            mVelocityTracker = VelocityTracker.obtain();
	        }
	        mVelocityTracker.addMovement(event);

	        // pinch zoom code
	        	if(supportsPinchZoom && pz.getPointerCount(event)>1){
	        			isPZing = true;
	        			//block accidental flicking by setting this parameter to true
	        			flicking = true;
	        			if(oldDist == null){ oldDist = pz.spacing(event);	     }
	        	}else{
        			isPZing = false;	        		
        			flicking = false;
        			oldDist = null;
	        	}
	        	switch (action) {
	        
	        	case MotionEvent.ACTION_DOWN:
        	                mZoomControl.stopFling();
        	                v.postDelayed(mLongPressRunnable, mLongPressTimeout);
        	                mDownX = x;
        	                mDownY = y;
        	                mX = x;
        	                mY = y;        
        	        	flicking = false;
        	        break;
	            case MotionEvent.ACTION_MOVE: {
	                final float dx = (x - mX) / v.getWidth();
	                final float dy = (y - mY) / v.getHeight();
	             
	                if(isPZing){
	                    Float[] coords = pz.midPoint(event);
	                    float newDist = pz.spacing(event);
	                    if(newDist>10f){
	                	final float scale = newDist / oldDist;
	                        final float xx = coords[0] / v.getWidth();
	                        final float yy = coords[1] / v.getHeight();
	                        mZoomControl.zoom(scale, xx,yy);           	
	                        oldDist = newDist;
	                    }
	                } else if (mMode == Mode.ZOOM) {
	                    	mZoomControl.zoom((float)Math.pow(20, -dy), mDownX / v.getWidth(), mDownY/ v.getHeight());
	                } else if (mMode == Mode.PAN) {
	                	if(mZoomControl.getZoomState().getZoom()>1.0f){ mZoomControl.pan(-dx, -dy);	                	}
	                } else {
	                    final float scrollX = mDownX - x;
	                    final float scrollY = mDownY - y;
	                    final float dist = (float)Math.sqrt(scrollX * scrollX + scrollY * scrollY);
	                    if (dist >= mScaledTouchSlop) {
	                        v.removeCallbacks(mLongPressRunnable);
	                        mMode = Mode.PAN;
	                    }
	                }

	                mX = x;
	                mY = y;
	                break;
	            }

	            case MotionEvent.ACTION_UP:
	                if (mMode == Mode.PAN) {
	                	if(mZoomControl.getZoomState().getZoom()>1.0f){
	                    mVelocityTracker.computeCurrentVelocity(1000, mScaledMaximumFlingVelocity);
	                    mZoomControl.startFling(-mVelocityTracker.getXVelocity() / v.getWidth(),
	                           -mVelocityTracker.getYVelocity() / v.getHeight());
	                	}
	                } else {
	                    mZoomControl.startFling(0, 0);
	                }
	                mVelocityTracker.recycle();
	                mVelocityTracker = null;
	                v.removeCallbacks(mLongPressRunnable);
	                mMode = Mode.UNDEFINED;
	            	if(flicking){
	            	    mZoomControl.stopFling();	 
	            	    mZoomControl.pan(0.5f, 0.5f);
	                    mZoomControl.getZoomState().setPanX(0.5f);
	                    mZoomControl.getZoomState().setPanY(0.5f);
	                    mZoomControl.getZoomState().setZoom(1f);
            		}
	                break;

	            default:
	                mVelocityTracker.recycle();
	                mVelocityTracker = null;
	                v.removeCallbacks(mLongPressRunnable);
	                mMode = Mode.UNDEFINED;
	                break;

	        }

	        return true;
	    }


		@Override
		public boolean onDown(MotionEvent e) {
			return false;
		}

		boolean flicking = false;

		
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,	float velocityY) {
			float zoom = mZoomControl.getZoomState().getZoom() ;
			if(zoom <= 1.0f && flicking == false){
        		flicking = true;
			if(velocityX<0){
				if(gesture!=null){gesture.onFlickForward();}
			}else{
				if(gesture!=null){gesture.onFlickBackward();}
			}
			}
			return true;
		}

		@Override
		public void onLongPress(MotionEvent e) {
		}

		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,float distanceY) {
		    return false;
		}

		@Override
		public void onShowPress(MotionEvent e) {
			
		}

		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			return false;
		}

		@Override
		public boolean onDoubleTap(MotionEvent e) {
			float zoom = mZoomControl.getZoomState().getZoom() ;
			if(zoom == 1.0f){
			    fullZoomIn();
			}else{
			     fullZoomOut();
			 }
			return false;
		}

		@Override
		public boolean onDoubleTapEvent(MotionEvent e) {
			return false;
		}

		@Override
		public boolean onSingleTapConfirmed(MotionEvent e) {
			if(gesture!=null){	    gesture.onToggle();}
    //			flick.setToggle(true);
//			flick.notifyObservers();
			return false;
		}

		 public void fullZoomIn(){
			AspectQuotient aspectQuotient= mZoomControl.getAspectQuotient();
			float zoomFactor = aspectQuotient.fullZoom; 
	//		float zoomFactor = 2.0f;
			mZoomControl.getZoomState().setPanX(0.5f);
			mZoomControl.getZoomState().setPanY(0.5f);
			mZoomControl.getZoomState().setZoom(zoomFactor);
			mZoomControl.updatePanLimits();
			mZoomControl.getZoomState().notifyObservers();
		    }
		    public void fullZoomOut(){
			 mZoomControl.getZoomState().setPanX(0.5f);
			 mZoomControl.getZoomState().setPanY(0.5f);
			 mZoomControl.getZoomState().setZoom(1.0f);
			 mZoomControl.updatePanLimits();
			 mZoomControl.getZoomState().notifyObservers();
		    }	
}


	