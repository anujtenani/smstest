package com.sony.zoomview;
import android.graphics.PointF;
import android.util.FloatMath;
import android.view.MotionEvent;

public class MyPinchZoom {

	// comes from MotionEvent.ACTION_POINTER_UP / DOWN
	public static final int POINTER_UP = 6;
	public static final int POINTER_DOWN = 5;


	public int getPointerCount(MotionEvent event){
		return event.getPointerCount();
	}
	public float spacing(MotionEvent event) {
		   float x = event.getX(0) - event.getX(1);
		   float y = event.getY(0) - event.getY(1);
		   return FloatMath.sqrt(x * x + y * y);
	}
	public Float[] midPoint(MotionEvent event) {
		   float x = event.getX(0) + event.getX(1);
		   float y = event.getY(0) + event.getY(1);
		   Float[] coords  = new Float[]{x/2,y/2};
		   return coords;
	}
	    /** Calculate the mid point of the first two fingers */
	public void midPoint(PointF point, MotionEvent event) {
	        float x = event.getX(0) + event.getX(1);
	        float y = event.getY(0) + event.getY(1);
	        point.set(x / 2, y / 2);
	}
}
